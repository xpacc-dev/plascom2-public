#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/sh

ScriptResults_UpdateResult()
{

TESTNAME=${1}
RESULT=${2}
OUTPUTFILE=${3}
printf "${TESTNAME}=${RESULT}\n" >> ${OUTPUTFILE}

}