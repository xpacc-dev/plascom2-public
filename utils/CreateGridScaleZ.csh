#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/tcsh

set numPointsX=$1
set numPointsY=$2
set zfile=$3

foreach numPointsZ (`cat ${zfile}`)
  set outfile=gridscale_config_${numPointsX}_${numPointsY}_${numPointsZ}
  rm -f ${outfile}
  cat gridscale_base.config > ${outfile}
  printf "advect1d:NumPoints = ${numPointsX} ${numPointsY} ${numPointsZ}\n" >> ${outfile}
  printf "advect1d:ProfileName = gridTiming_${numPointsX}_${numPointsY}_${numPointsZ}\n" >> ${outfile}
end
