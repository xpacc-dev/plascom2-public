#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/bash

SOURCEPATH=${1}
OUTFILE=${2}
ORIGPATH=`pwd`

cd ${SOURCEPATH}
REPONAME=`git ls-remote 2>&1 | grep From | grep git | cut -d " " -f 2`
BRANCHNAME=`git branch | grep "*" | cut -d " " -f 2-`
LOCALREV=`git rev-parse HEAD`
BRANCHREFNAME=`git symbolic-ref -q HEAD`
if [ "${BRANCHREFNAME}" != "" ]; then
  UPSTREAM=`git for-each-ref --format='%(upstream:short)' ${BRANCHREFNAME}`
  REMOTEREV=`git rev-parse ${UPSTREAM}`
else
  # Detached HEAD
  REMOTEREV=""
fi
BUILDHOST=`hostname -f`
BUILDDATE=`date`
CHANGED=`git diff --name-only | tr '\n' ' '`
REVISION=""
if [ "$LOCALREV" == "$REMOTEREV" ]; then
 REVISION=${LOCALREV}
fi
if [ "$OUTFILE" != "" ]; then
 if [ -e ${OUTFILE} ]; then
    rm ${OUTFILE}
 fi
 printf "#define REPONAME         \"${REPONAME}\"\n"         > ${OUTFILE}
 printf "#define BRANCHNAME       \"${BRANCHNAME}\"\n"      >> ${OUTFILE}
 if [ "$REVISION" != "" ]; then
  printf "#define REVISION        \"${REVISION}\"\n"        >> ${OUTFILE}
 else
  printf "#define LOCALREV         \"${LOCALREV}\"\n"        >> ${OUTFILE}
  printf "#define REMOTEREV        \"${REMOTEREV}\"\n"       >> ${OUTFILE}
 fi
 printf "#define BUILDHOST        \"${BUILDHOST}\"\n"       >> ${OUTFILE}
 printf "#define BUILDDATE        \"${BUILDDATE}\"\n"       >> ${OUTFILE}
 if [ "$CHANGED" != "" ]; then
  printf "#define CHANGED         \"${CHANGED}\"\n"         >> ${OUTFILE}
 fi
else
 printf "REPO=${REPONAME}\nBRANCH=${BRANCHNAME}\n"
 printf "LOCALREV=${LOCALREV}\nREMOTEREV=${REMOTEREV}\n"
 printf "BUILDHOST=${BUILDHOST}\nBUILDDATE=${BUILDDATE}\n"
 if [ "$CHANGED" != "" ]; then
  printf "CHANGED FILES=${CHANGED}\n"
 fi
fi
cd ${ORIGPATH}
