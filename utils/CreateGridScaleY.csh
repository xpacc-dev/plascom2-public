#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/tcsh

set numPointsX=$1
set yfile=$2
set numPointsZ=$3

set xZeros = ""
if( ${numPointsX} < 10 ) then
  set xZeros = "000000000"
else if ( ${numPointsX} < 100 ) then
  set xZeros = "00000000"
else if ( ${numPointsX} < 1000 ) then
  set xZeros = "0000000"
else if ( ${numPointsX} < 10000 ) then
  set xZeros = "000000"
else if ( ${numPointsX} < 100000 ) then
  set xZeros = "00000"
else if ( ${numPointsX} < 1000000 ) then
  set xZeros = "0000"
else if ( ${numPointsX} < 10000000 ) then
  set xZeros = "000"
else if ( ${numPointsX} < 100000000 ) then
  set xZeros = "00"
else if ( ${numPointsX} < 1000000000 ) then
  set xZeros = "0"
endif 

set zZeros = ""
if( ${numPointsZ} < 10 ) then
  set zZeros = "000000000"
else if ( ${numPointsZ} < 100 ) then
  set zZeros = "00000000"
else if ( ${numPointsZ} < 1000 ) then
  set zZeros = "0000000"
else if ( ${numPointsZ} < 10000 ) then
  set zZeros = "000000"
else if ( ${numPointsZ} < 100000 ) then
  set zZeros = "00000"
else if ( ${numPointsZ} < 1000000 ) then
  set zZeros = "0000"
else if ( ${numPointsZ} < 10000000 ) then
  set zZeros = "000"
else if ( ${numPointsZ} < 100000000 ) then
  set zZeros = "00"
else if ( ${numPointsZ} < 1000000000 ) then
  set zZeros = "0"
endif 

foreach numPointsY (`cat ${yfile}`)

    set yZeros = ""
    if( ${numPointsY} < 10 ) then
    set yZeros = "000000000"
    else if ( ${numPointsY} < 100 ) then
    set yZeros = "00000000"
    else if ( ${numPointsY} < 1000 ) then
    set yZeros = "0000000"
    else if ( ${numPointsY} < 10000 ) then
    set yZeros = "000000"
    else if ( ${numPointsY} < 100000 ) then
    set yZeros = "00000"
    else if ( ${numPointsY} < 1000000 ) then
    set yZeros = "0000"
    else if ( ${numPointsY} < 10000000 ) then
    set yZeros = "000"
    else if ( ${numPointsY} < 100000000 ) then
    set yZeros = "00"
    else if ( ${numPointsY} < 1000000000 ) then
    set yZeros = "0"
    endif

  set outfile=gridscale_config_${xZeros}${numPointsX}_${yZeros}${numPointsY}_${zZeros}${numPointsZ}
  rm -f ${outfile}
  cat gridscale_base.config > ${outfile}
  printf "advect1d:NumPoints = ${numPointsX} ${numPointsY} ${numPointsZ}\n" >> ${outfile}
  printf "advect1d:ProfileName = gridTiming_${xZeros}${numPointsX}_${yZeros}${numPointsY}_${zZeros}${numPointsZ}\n" >> ${outfile}

end
