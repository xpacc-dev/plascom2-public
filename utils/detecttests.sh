#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/sh

WORKDIR=${1}
ODIR=`pwd`


cd ${WORKDIR}

# Discover serial unit tests
rm serial_unit_tests.cmake
grep "serialUnitResults\.UpdateResult" -r * | grep -v "//" | cut -d "(" -f 2 | cut -d "," -f 1 | sed 's/"//g' | sort | uniq > tmptestnms.txt
for TESTNAME in `cat tmptestnms.txt`; do
  printf "ADD_TEST(${TESTNAME} \${TESTRESULT} ${TESTNAME} plascom2_testresults.txt)\n" >> serial_unit_tests.cmake
done
rm tmptestnms.txt

# Discover parallel unit tests
rm parallel_unit_tests.cmake
grep "parallelUnitResults\.UpdateResult" -r * | grep -v "//" | cut -d "(" -f 2 | cut -d "," -f 1 | sed 's/"//g' | sort | uniq > tmptestnms.txt
for TESTNAME in `cat tmptestnms.txt`; do
  printf "ADD_TEST(${TESTNAME} \${TESTRESULT} ${TESTNAME} plascom2_testresults.txt)\n" >> parallel_unit_tests.cmake
done
rm tmptestnms.txt

cd ${ODIR}


