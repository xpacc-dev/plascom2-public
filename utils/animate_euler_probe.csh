#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/tcsh

set FILENAME=${1}
set NFRAMES=${2}
set STEP=${3}

if("$NFRAMES" == "") then
  set NFRAMES=100
endif

if("$STEP" == "") then
  set STEP=10
endif

@ iStep = 0

while ( $iStep < $NFRAMES )
    rm -f step.gp
    set framezeros = ""
    if( $iStep < 10 ) then
      set framezeros = "000"
    else if ( $iStep < 100 ) then
      set framezeros = "00"
    else if ( $iStep < 1000 ) then
     set  framezeros = "0"
    endif 
    printf "set yrange [1:3]\n" > step.gp
    printf "set key left\n" >> step.gp
    printf "set term png\n" >> step.gp
    printf "set grid ytics\n" >> step.gp
    printf "set output 'frame_${framezeros}${iStep}'\n" >> step.gp
#    printf "plot '${FILENAME}' index ${iStep} using 1 every 5 w lp t 'Rho','' index ${iStep} using 2 every 5 w lp t 'RhoVx'\n" >> step.gp
#    printf "plot '${FILENAME}' index ${iStep} using 1 every 10 w lp t 'Rho','' index ${iStep} using 4 every 10 w lp t 'RhoVz'\n" >> step.gp
#    printf "plot '${FILENAME}' index ${iStep} using 5 every 10 w lp t 'RhoE'\n" >> step.gp
    printf "plot '${FILENAME}' index ${iStep} using 1 every 10 w lp t 'Rho'\n" >> step.gp
#    printf "plot '${FILENAME}' index ${iStep} using 1 w lp t 'Rho','' index ${iStep} using 2 w lp t 'RhoVx'\n" >> step.gp
    printf "quit" >> step.gp
    gnuplot < step.gp
    @ iStep += ${STEP}
end
convert frame_* probe.gif
rm -f frame_*
