#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/tcsh

set FILENAME=${1}
set FILENAME2=${2}
set NFRAMES=${3}
set STEP=${4}

if("$NFRAMES" == "") then
  set NFRAMES=100
endif

if("$STEP" == "") then
  set STEP=10
endif

@ iStep = 0

while ( $iStep < $NFRAMES )
    rm -f step.gp
    set framezeros = ""
    if( $iStep < 10 ) then
      set framezeros = "000"
    else if ( $iStep < 100 ) then
      set framezeros = "00"
    else if ( $iStep < 1000 ) then
     set  framezeros = "0"
    endif 
#    printf "set yrange [-0.01:.06]\n" > step.gp
    printf "set key left\n" >> step.gp
    printf "set term png\n" >> step.gp
    printf "set grid ytics\n" >> step.gp
    printf "set output 'frame_${framezeros}${iStep}'\n" >> step.gp
    printf "plot '${FILENAME}' index ${iStep} every 5 using 0:1 w p t 'U','${FILENAME2}' index ${iStep} every 5 using 0:1 w l t 'Analytic'\n" >> step.gp
    printf "quit" >> step.gp
    gnuplot < step.gp
    @ iStep += ${STEP}
end
convert frame_* probe.gif
rm -f frame_*
