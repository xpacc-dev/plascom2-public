!
! Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!
!
! @file Kernels for barebones mode
!
SUBROUTINE BAREBONES(runCode)

  INTEGER :: runCode
  
  runCode = 0
  WRITE(*,*) 'Hello Codelet4 World.'

END SUBROUTINE BAREBONES
