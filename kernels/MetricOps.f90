!
! Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!
MODULE MetricOps

  USE OPERATORS
  USE GRID
  
  IMPLICIT NONE
  
CONTAINS
  
  
  SUBROUTINE ALPHAWEIGHT(                          &
    numDim,numPointsBuffer,bufferSizes,opInterval, &
         gridType,gridMetric,alphaDir,alphaW)
    
    IMPLICIT NONE 

    INTEGER(KIND=4), INTENT(IN)         :: numDim, gridType, alphaDir
    INTEGER(KIND=8), INTENT(IN)         :: bufferSizes(numDim),numPointsBuffer
    INTEGER(KIND=8), INTENT(IN)         :: opInterval(2*numDim)
    REAL(KIND=8),    INTENT(IN), TARGET :: gridMetric(numDim*numDim*numPointsBuffer)
    REAL(KIND=8),    INTENT(OUT)        :: alphaW(numPointsBuffer)

    REAL(KIND=8)    :: gridScale
    INTEGER(KIND=8) :: metricOffset

    REAL(KIND=8),    DIMENSION(:), POINTER :: metricPtr

    IF(gridType < RECTILINEAR) THEN

       gridScale = ABS(gridMetric(alphaDir))

       CALL ASSIGNMENTXA(numDim,numPointsBuffer,bufferSizes,opInterval, &
            gridScale,alphaW)

    ELSE IF(gridType == RECTILINEAR) THEN


       metricOffset =  (alphaDir-1)*numPointsBuffer
       metricPtr    => gridMetric(metricOffset+1:metricOffset+numPointsBuffer)

       CALL ASSIGNMENTYABSX(numDim,numPointsBuffer,bufferSizes,opInterval, &
            metricPtr,alphaW);
       
    ELSE ! Curvilinear

       metricOffset =  (alphaDir-1)*numDim*numPointsBuffer
       metricPtr    => gridMetric(metricOffset+1:metricOffset+(numDim*numPointsBuffer))

       CALL VECLEN(numDim,numPointsBuffer,bufferSizes,opInterval, &
            numDim,metricPtr,alphaW)
       
    END IF
    
  END SUBROUTINE ALPHAWEIGHT

  SUBROUTINE ALPHAWEIGHT2(                          &
    numDim,numPointsBuffer,bufferSizes,opInterval, &
         gridType,gridMetric,gridJacobian,alphaDir,alphaW)
    
    IMPLICIT NONE 

    INTEGER(KIND=4), INTENT(IN)         :: numDim, gridType, alphaDir
    INTEGER(KIND=8), INTENT(IN)         :: bufferSizes(numDim),numPointsBuffer
    INTEGER(KIND=8), INTENT(IN)         :: opInterval(2*numDim)
    REAL(KIND=8),    INTENT(IN), TARGET :: gridMetric(numDim*numDim*numPointsBuffer)
    REAL(KIND=8),    INTENT(IN), TARGET :: gridJacobian(numPointsBuffer)
    REAL(KIND=8),    INTENT(OUT)        :: alphaW(numPointsBuffer)

    REAL(KIND=8)    :: gridScale
    INTEGER(KIND=8) :: metricOffset

    REAL(KIND=8),    DIMENSION(:), POINTER :: metricPtr
    REAL(KIND=8),    DIMENSION(:), POINTER :: jacobianM1Ptr
    
    IF(gridType < RECTILINEAR) THEN

       gridScale = gridJacobian(1)*ABS(gridMetric(alphaDir))

       CALL ASSIGNMENTXA(numDim,numPointsBuffer,bufferSizes,opInterval, &
            gridScale,alphaW)

    ELSE IF(gridType == RECTILINEAR) THEN


       metricOffset  =  (alphaDir-1)*numPointsBuffer
       metricPtr     => gridMetric(metricOffset+1:metricOffset+numPointsBuffer)
       
       CALL ASSIGNMENTYABSX(numDim,numPointsBuffer,bufferSizes,opInterval, &
            metricPtr,alphaW);
       CALL YXY(numDim,numPointsBuffer,bufferSizes,opInterval, &
            gridJacobian,alphaW);
       
    ELSE ! Curvilinear

       metricOffset  =  (alphaDir-1)*numDim*numPointsBuffer
       metricPtr     => gridMetric(metricOffset+1:metricOffset+(numDim*numPointsBuffer))


       CALL VECLEN(numDim,numPointsBuffer,bufferSizes,opInterval, &
            numDim,metricPtr,alphaW)       
       CALL YXY(numDim,numPointsBuffer,bufferSizes,opInterval, &
            gridJacobian,alphaW);
       
    END IF
    
  END SUBROUTINE ALPHAWEIGHT2

  SUBROUTINE VHATCOMPONENT(                          &
       numDim,numPointsBuffer,bufferSizes,opInterval, &
       gridType,gridMetric,velDir,velocity,velHatComponent)
    
    IMPLICIT NONE 

    INTEGER(KIND=4), INTENT(IN)         :: numDim, gridType,velDir
    INTEGER(KIND=8), INTENT(IN)         :: bufferSizes(numDim),numPointsBuffer
    INTEGER(KIND=8), INTENT(IN)         :: opInterval(2*numDim)
    REAL(KIND=8),    INTENT(IN), TARGET :: gridMetric(numDim*numDim*numPointsBuffer)
    REAL(KIND=8),    INTENT(IN), TARGET :: velocity(numDim*numPointsBuffer)
    REAL(KIND=8),    INTENT(OUT)        :: velHatComponent(numPointsBuffer)


    INTEGER         :: iDim, jDim
    INTEGER(KIND=8) :: metricOffset, velOffset
    REAL(KIND=8)    :: gridScale

    REAL(KIND=8),    DIMENSION(:), POINTER :: velPtr
    REAL(KIND=8),    DIMENSION(:), POINTER :: metricPtr

    ! Get vHat component for given grid type and direction
    !
    ! If grid < rectilinear,   vhat   = constant*V(idim)
    !                          metric = (constant_x, constant_y, constant_z)
    ! If grid == rectilinear,  vhat   = metric(idim)*V(idim)
    !                          metric = <M_x,M_y,M_z> [i.e. diagonal per point]
    ! If grid == curvilinear,  vhat   = Metric(idim)*V(idim)
    !                          metric = <Mx_i Mx_j Mx_k My_i ... Mz_k> [tensor per point]
    !
!    DO iDim = 1,numDim 
!       velPtr => velocity((iDim-1)*numPointsBuffer+1:(iDim-1)*numPointsBuffer+numPointsBuffer)
!       WRITE(*,*) 'Velocity(',iDim,') = ',velPtr
!    END DO
    
    IF(gridType < RECTILINEAR) THEN

       gridScale = gridMetric(velDir)
       velOffset = (velDir-1)*numPointsBuffer
       velPtr    => velocity(velOffset+1:velOffset+numPointsBuffer)

       CALL YAX(numDim,numPointsBuffer,bufferSizes,opInterval, &
            gridScale,velPtr,velHatComponent)

    ELSE IF(gridType == RECTILINEAR) THEN


       metricOffset =  (velDir-1)*numPointsBuffer
       metricPtr    => gridMetric(metricOffset+1:metricOffset+numPointsBuffer)
       velPtr       => velocity(metricOffset+1:metricOffset+numPointsBuffer)

       CALL ZXY(numDim,numPointsBuffer,bufferSizes,opInterval, &
            metricPtr,velPtr,velHatComponent);

    ELSE ! Curvilinear

       metricOffset =  (velDir-1)*numDim*numPointsBuffer
       metricPtr    => gridMetric(metricOffset+1:metricOffset+(numDim*numPointsBuffer))

       CALL ZXDOTY(numDim,numPointsBuffer,bufferSizes,opInterval, &
            numDim,metricPtr,velocity,velHatComponent);

    END IF

!    WRITE(*,*) 'VelHat(',velDir,') = ',velHatComponent

  END SUBROUTINE VHATCOMPONENT

 
  SUBROUTINE IJKGRADTOXYZDIV(                         &
       numDim, numPoints, gridSizes, opInterval,      & 
       gridType, gridJacobian,gridMetric,gradVBuffer, & 
       divBuffer)
    
    IMPLICIT NONE

    INTEGER(KIND=4), INTENT(IN)         :: numDim, gridType
    INTEGER(KIND=8), INTENT(IN)         :: gridSizes(numDim),  numPoints
    INTEGER(KIND=8), INTENT(IN)         :: opInterval(2*numDim)
    REAL(KIND=8),    INTENT(IN)         :: gridJacobian(numPoints)
    REAL(KIND=8),    INTENT(IN), TARGET :: gridMetric(numDim*numDim*numPoints)
    REAL(KIND=8),    INTENT(IN), TARGET :: gradVBuffer(numDim*numDim*numPoints)
    REAL(KIND=8),    INTENT(OUT)        :: divBuffer(numPoints)

    INTEGER         :: iDim, jDim
    INTEGER(KIND=8) :: metricOffset, gradOffset
    REAL(KIND=8)    :: gridScale

    REAL(KIND=8),    DIMENSION(:), POINTER :: gradPtr
    REAL(KIND=8),    DIMENSION(:), POINTER :: metricPtr

    IF(gridType > RECTILINEAR) THEN ! full curvilinear
       DO iDim = 1,numDim
          DO jDim = 1,numDim
             gradOffset   =  ((iDim-1)*numDim+(jDim-1))*numPoints
             metricOffset =  ((iDim-1)+(jDim-1)*numDim)*numPoints
             metricPtr    => gridMetric(metricOffset+1:metricOffset+numPoints)
             gradPtr      => gradVBuffer(gradOffset+1:gradOffset+numPoints)
             CALL YWXPY(numDim,numPoints,gridSizes,opInterval, &
                  metricPtr,gradPtr,divBuffer)
          END DO
       END DO
       CALL YXY(numDim,numPoints,gridSizes,opInterval,gridJacobian,divBuffer)
    ELSE IF(gridType < RECTILINEAR) THEN ! uniform rectangular
       DO iDim = 1,numDim
          gridScale  = gridMetric(iDim)
          gradOffset = ((iDim-1)*(numDim+1))*numPoints
          gradPtr    => gradVBuffer(gradOffset+1:gradOffset+numPoints)
          CALL YAXPY(numDim,numPoints,gridSizes,opInterval, &
               gridScale,gradPtr, divBuffer)
       END DO
       CALL XAX(numDim,numPoints,gridSizes,opInterval, &
            gridJacobian(1),divBuffer)
    ELSE ! rectilinear (weirdo)
       DO iDim = 1,numDim
          metricOffset =  (iDim-1)*numPoints
          metricPtr    => gridMetric(metricOffset+1:metricOffset+numPoints)
          gradOffset   = ((iDim-1)*(numDim+1))*numPoints
          gradPtr      => gradVBuffer(gradOffset+1:gradOffset+numPoints)
          CALL YWXPY(numDim,numPoints,gridSizes,opInterval, &
               metricPtr,gradPtr,divBuffer)          
       END DO
       CALL YXY(numDim,numPoints,gridSizes,opInterval,gridJacobian,divBuffer)
    END IF

  END SUBROUTINE IJKGRADTOXYZDIV

  !> @brief Converts Cartesian (computational) gradient to physical coordinates
  SUBROUTINE GRADIJKTOGRADXYZ(                             &
       numDim, numPoints, gridSizes, opInterval, gridType, &
       gridJacobian,gridMetric,gradIJK, gradXYZ)
    
    IMPLICIT NONE

    INTEGER(KIND=4), INTENT(IN)         :: numDim, gridType
    INTEGER(KIND=8), INTENT(IN)         :: gridSizes(numDim),  numPoints
    INTEGER(KIND=8), INTENT(IN)         :: opInterval(2*numDim)
    REAL(KIND=8),    INTENT(IN)         :: gridJacobian(numPoints)
    REAL(KIND=8),    INTENT(IN), TARGET :: gridMetric(numDim*numDim*numPoints)
    REAL(KIND=8),    INTENT(IN), TARGET :: gradIJK(numDim*numPoints)
    REAL(KIND=8),    INTENT(OUT),TARGET :: gradXYZ(numDim*numPoints)

    INTEGER         :: iDim, jDim
    INTEGER(KIND=8) :: metricOffset, gradIJKOffset, gradXYZOffset
    REAL(KIND=8)    :: gridScale
    REAL(KIND=8)    :: zero

    REAL(KIND=8),    DIMENSION(:), POINTER :: gradIJKPtr
    REAL(KIND=8),    DIMENSION(:), POINTER :: gradXYZPtr
    REAL(KIND=8),    DIMENSION(:), POINTER :: metricPtr

    zero = 0.0_8

    IF(gridType > RECTILINEAR) THEN ! full curvilinear
       
       ! Note: only need to zero for curvilinear, other metric types
       ! utilize assignment-type kernels
       DO iDim = 1,numDim
          gradXYZOffset =  (iDim-1)*numPoints
          gradXYZPtr    => gradXYZ(gradXYZOffset+1:gradXYZOffset+numPoints)
          CALL ASSIGNMENTXA(numDim,numPoints,gridSizes,opInterval,zero,gradXYZPtr)
       END DO

       DO iDim = 1,numDim
          gradXYZOffset   =  (iDim-1)*numPoints
          gradXYZPtr      => gradXYZ(gradXYZOffset+1:gradXYZOffset+numPoints)
          DO jDim = 1,numDim
             gradIJKOffset   =  (jDim-1)*numPoints
             gradIJKPtr      => gradIJK(gradIJKOffset+1:gradIJKOffset+numPoints)
             metricOffset    =  ((jDim-1)*numDim+(iDim-1))*numPoints
             metricPtr       => gridMetric(metricOffset+1:metricOffset+numPoints)
             CALL YWXPY(numDim,numPoints,gridSizes,opInterval, &
                  metricPtr,gradIJKPtr,gradXYZPtr)
          END DO
          CALL YXY(numDim,numPoints,gridSizes,opInterval,gridJacobian,gradXYZPtr)
       END DO

    ELSE IF(gridType < RECTILINEAR) THEN ! uniform rectangular
       DO iDim = 1,numDim
          gridScale     = gridMetric(iDim)*gridJacobian(1)
          gradIJKOffset = (iDim-1)*numPoints
          gradIJKPtr    => gradIJK(gradIJKOffset+1:gradIJKOffset+numPoints)
          gradXYZPtr    => gradXYZ(gradIJKOffset+1:gradIJKOffset+numPoints)
          CALL YAX(numDim,numPoints,gridSizes,opInterval, &
               gridScale,gradIJKPtr, gradXYZPtr)
       END DO
    ELSE ! rectilinear (weirdo)
       DO iDim = 1,numDim
          metricOffset =  (iDim-1)*numPoints
          metricPtr    => gridMetric(metricOffset+1:metricOffset+numPoints)
          gradIJKPtr   => gradIJK(metricOffset+1:metricOffset+numPoints)
          gradXYZPtr   => gradXYZ(metricOffset+1:metricOffset+numPoints)
          CALL ZXY(numDim,numPoints,gridSizes,opInterval, &
               metricPtr,gradIJKPtr,gradXYZPtr)          
          CALL YXY(numDim,numPoints,gridSizes,opInterval,gridJacobian,gradXYZPtr)
       END DO
    END IF

  END SUBROUTINE GRADIJKTOGRADXYZ


END MODULE MetricOps
