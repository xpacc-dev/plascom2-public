!
! Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!
MODULE SATUtil

  USE GRID

  IMPLICIT NONE

CONTAINS
  
  SUBROUTINE FARFIELD(                                                   &
       numDim,bufferSizes,numPointsBuffer,patchNormalDir,patchSizes,     &
       numPointsPatch,numPatchPointsOp,patchPointsOp,gridType,gridMetric,&
       jacobianDeterminant,bcParams,gasParams,rhoBuffer,rhoVBuffer,      &
       rhoEBuffer,viscousFluxBuffer,numscalar,scalarBuffer,        &
       rhoRHS,rhoVRHS,rhoERHS,scalarRHS,                              &
       rhoTarget,rhoVTarget,rhoETarget,scalarTarget)
    
    IMPLICIT NONE

    INTEGER(KIND=4) :: numDim,numscalar,patchNormalDir,gridType
    INTEGER(KIND=8) :: numPointsBuffer,bufferSizes(numDim)
    INTEGER(KIND=8) :: patchSizes(numDim),numPointsPatch
    INTEGER(KIND=8) :: numPatchPointsOp
    INTEGER(KIND=8) :: patchPointsOp(numPatchPointsOp)
    REAL(KIND=8)    :: gridMetric(numDim*numDim*numPointsBuffer)
    REAL(KIND=8)    :: jacobianDeterminant(numPointsBuffer)
    REAL(KIND=8)    :: bcParams(3),gasParams(5)
    REAL(KIND=8)    :: rhoBuffer(numPointsBuffer)
    REAL(KIND=8)    :: rhoVBuffer(numDim*numPointsBuffer)
    REAL(KIND=8)    :: rhoEBuffer(numPointsBuffer)
    REAL(KIND=8)    :: viscousFluxBuffer((numDim+2)*numPointsBuffer)
    REAL(KIND=8)    :: scalarBuffer(numscalar*numPointsBuffer)
    REAL(KIND=8)    :: rhoRHS(numPointsBuffer)
    REAL(KIND=8)    :: rhoVRHS(numDim*numPointsBuffer)
    REAL(KIND=8)    :: rhoERHS(numPointsBuffer)
    REAL(KIND=8)    :: scalarRHS(numscalar*numPointsBuffer)
    REAL(KIND=8)    :: rhoTarget(numPointsBuffer)
    REAL(KIND=8)    :: rhoVTarget(numDim*numPointsBuffer)
    REAL(KIND=8)    :: rhoETarget(numPointsBuffer)
    REAL(KIND=8)    :: scalarTarget(numScalar*numPointsBuffer)

    ! gasParams(:) = Cref Gamma Cp Re
    ! bcParams(:)  = sigma1 sigma2

    ! ... Local variables
    INTEGER(4) :: ND, nAuxVars, jj, iDim, iDir
    INTEGER(8) :: Nc, l0, iPoint, pointIndex, metricOffset
    INTEGER(4) :: normDir, sgn ! , gas_dv_model
    REAL(8) :: dsgn, sndspdref2, invtempref, tempref, gamref, spcGasConst
    REAL(8) :: XI_X, XI_Y, XI_Z, XI_T, bndry_h,sbpBoundaryWeight
    REAL(8) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, gridJacobian
    REAL(8) :: rhoFac, penaltyFac, scalarPenaltyFac, scalarDiff

    REAL(8), POINTER :: XIX(:)
    !    REAL(KIND=RFREAL), Pointer :: XI_TAU(:,:), cv(:,:), gv(:,:)
    REAL(8), POINTER :: pnorm_vec(:) ! , rhs(:,:)
    REAL(8), POINTER :: Lambda(:,:), gI1(:), uB(:), Tinv(:,:), Tmat(:,:)
    REAL(8), POINTER :: Aprime(:,:), gI2(:), matX(:,:), penalty(:) ,correction(:)
    REAL(8) :: norm_vec(3), gamma, ucon, ubAux,xh
    REAL(8) :: sigmaI1, sigmaI2, sigmaAux, Sfactor, ibfac_local
    REAL(8) :: spd_snd, RE, REInv, SAT_sigmaI2_FF, SAT_sigmaI1_FF

    LOGICAL :: TRIGGERED
    TRIGGERED = .FALSE.


    ! ... Problem size
    ND = numDim
    Nc = numPointsBuffer
    !    N(1:MAX_ND) = 1; Np(1:MAX_ND) = 1;
    !    Do j = 1, ND
    !      N(j)  =  grid_ie(j) -  grid_is(j) + 1
    !      Np(j) = patch_ie(j) - patch_is(j) + 1
    !    End Do
    nAuxVars = numscalar

    ! ... Useful quantities about this boundary
    normDir = abs(patchNormalDir)
    sgn = normDir / patchNormalDir
    dsgn = dble(sgn)


    ! ... Reference quantities
    ! gasParams = (sndspdref,gamma,Cp,1/Re)
    ! MJA changed for new nonDimensionalization
    !sndspdref2 = gasParams(1)*gasParams(1) ! input%sndspdref * input%sndspdref
    !invtempref = gasParams(3)/sndspdref2  ! input%Cpref / sndspdref2
    !tempref    = 1.0_8 / invtempref 
    !gamref     = gasParams(2)
    !REinv      = gasParams(4)
    gamref     = gasParams(2)
    spcGasConst= gasParams(3)
    sndspdref2 = gasParams(5)*gasParams(5)
    tempref    = gasParams(4)
    invtempref = tempref
    RE         = gasParams(1)
    REInv      = 0.0_8
    IF(RE .gt. 0) REInv = 1.0_8/RE


    ! ... BC Constants
    ! bcParams = (sigma1,sigma2,sbpBoundaryStencilWeight)
    SAT_sigmaI1_FF              = bcParams(1) ! input%SAT_sigmaI1_FF
    SAT_sigmaI2_FF              = bcParams(2) ! input%SAT_sigmaI2_FF
    sbpBoundaryWeight           = bcParams(3)

    !    gas_dv_model                = input%gas_dv_model

!    WRITE(*,*) 'NORMAL DIR: ',patchNormalDir
!    WRITE(*,*) 'SNDSPDREF2 = ',sndspdref2 
!    WRITE(*,*) 'tempref = ',tempref
!    WRITE(*,*) 'gamref = ',gamref
!    WRITE(*,*) 'sigma1 = ',SAT_sigmaI1_FF
!    WRITE(*,*) 'sigma2 = ',SAT_sigmaI2_FF
!    WRITE(*,*) 'REInv = ',REinv
!    WRITE(*,*) 'Bweight = ',sbpBoundaryWeight
!    WRITE(*,*) 'JAC = ',jacobianDeterminant
    
    ! ... memory allocation
    ALLOCATE(XIX(4))
    ALLOCATE(gI1(ND+2), uB(ND+2), Tmat(ND+2,ND+2), Tinv(ND+2,ND+2), penalty(ND+2),correction(ND+2))
    ALLOCATE(gI2(ND+2), Aprime(ND+2,ND+2), Lambda(ND+2,ND+2), matX(ND+2,ND+2), pnorm_vec(ND))

!    WRITE(*,*) 'Farfield ',sgn,normDir

    XIX(:) = 0.0_8
    IF(gridType <= UNIRECT) THEN
       XIX(normDir) = gridMetric(normDir)
       metricOffset = 0
       gridJacobian = jacobianDeterminant(1)
    ELSE IF(gridType == RECTILINEAR) THEN
       metricOffset = (normDir-1)*numPointsBuffer
    ELSE
       metricOffset = (normDir-1)*numDim*numPointsBuffer
    ENDIF

!    WRITE(*,*) 'gridtype,metricoffset = ',gridType,metricOffset

    ! ... loop over all of the points
    DO iPoint = 1, numPatchPointsOp
       l0 = patchPointsOp(iPoint) + 1 ! assume the points coming in are from C (0-based)
       ! ... iblank factor (either 1 or zero)
       ibfac_local = 1.0_8 ! ibfac(l0)

       ! ... construct the normalized metrics
       !       XI_X = MT1(l0,t2Map(normDir,1)); XI_Y = 0.0_8; XI_Z = 0.0_8
       !       if (ND >= 2) XI_Y = MT1(l0,t2Map(normDir,2))
       !       if (ND == 3) XI_Z = MT1(l0,t2Map(normDir,3))
       !       XI_T = XI_TAU(l0,normDir)
       IF(gridType == CURVILINEAR ) THEN
          DO iDir = 1,numDim
             XIX(iDir) = gridMetric(metricOffset + (iDir-1)*numPointsBuffer +l0)
          END DO
          gridJacobian = jacobianDeterminant(l0)
       ELSE IF(gridType == RECTILINEAR) THEN          
          XIX(normDir) = gridMetric(metricOffset + l0)
          gridJacobian = jacobianDeterminant(l0)
       ENDIF

       XI_X = XIX(1) ! gridMetric(1)
       XI_Y = XIX(2)
       XI_Z = XIX(3)
       XI_T = XIX(4)

 !      IF(TRIGGERED.eqv..FALSE.) THEN
 !      WRITE(*,*) 'XI_X: ',XI_X
 !      WRITE(*,*) 'XI_Y: ',XI_Y
 !         WRITE(*,*) 'XI_Z: ',XI_Z
 !      ENDIF

       ! ... multiply by the Jacobian
       XI_X = XI_X * gridJacobian  ! JAC(l0)
       XI_Y = XI_Y * gridJacobian  ! JAC(l0)
       XI_Z = XI_Z * gridJacobian  ! JAC(l0)
       XI_T = XI_T * gridJacobian  ! JAC(l0)

       ! ... normalized metrics
       xh = sqrt((XI_X*XI_X)+(XI_Y*XI_Y)+(XI_Z*XI_Z))
       bndry_h = 1.0_8 / xh
       XI_X_TILDE = XI_X * bndry_h
       XI_Y_TILDE = XI_Y * bndry_h
       XI_Z_TILDE = XI_Z * bndry_h

  
     ! ... inviscid penalty parameter
       sigmaI1 = -SAT_sigmaI1_FF * dsgn

       ! ... pull off the boundary data 
       !       uB(:) = cv(l0,:)
       uB(1) = rhoBuffer(l0)
       DO iDim = 1, numDim
          uB(1+iDim) = rhoVBuffer(l0+(iDim-1)*numPointsBuffer)
       END DO
       uB(numDim+2) = rhoEBuffer(l0)

       ! ... target data
       !       gI1(:) = cvTarget(l0,:)
       gI1(1) = rhoTarget(l0)
       DO iDim = 1, numDim
          gI1(1+iDim) = rhoVTarget(l0+(iDim-1)*numPointsBuffer)
       END DO
       gI1(numDim+2) = rhoETarget(l0)

       ! ... follow Magnus's algorithm (norm_vec has unit length)
       norm_vec(1:3) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)
       pnorm_vec(:) = norm_vec(1:ND) * xh

       ! ... use target data to make implicit work better?
       GAMMA = gamref ! gv(l0,1)
       !       If (gas_dv_model == DV_MODEL_IDEALGAS .or. &
       !            gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
  !     IF(TRIGGERED.eqv..FALSE.) THEN
  !     WRITE(*,*) 'XI_X_TILDE: ',XI_X_TILDE
  !     WRITE(*,*) 'XI_Y_TILDE: ',XI_Y_TILDE
  !     WRITE(*,*) 'XI_Z_TILDE: ',XI_Z_TILDE
  !     WRITE(*,*) 'XI_X: ',XI_X
  !     WRITE(*,*) 'XI_Y: ',XI_Y
  !     WRITE(*,*) 'XI_Z: ',XI_Z
  !     WRITE(*,*) 'BOUNDARYH:  ',bndry_h
  !     WRITE(*,*) 'GridJacobian: ',gridJacobian
  !     WRITE(*,*) 'STATE: ',uB(:)
  !     WRITE(*,*) 'TARGET:',gI1(:)
       
  !     ENDIF

       CALL SAT_Form_Roe_Matrices(ND, uB, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)

       !          Else If (gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then
       !
       !            Call SAT_Form_Roe_Matrices_TP(uB, gI1, ND, gamma, gamref, pnorm_vec, Tmat, Tinv, Lambda)
       !          End If
       
       ! ... save ucon, speed_sound
       ucon    = Lambda(1,1)
       spd_snd = Lambda(ND+1,ND+1) - ucon

       ! ... only pick those Lambda that are incoming
       If ( sgn == 1 ) Then
          Do jj = 1, ND+2
             Lambda(jj,jj) = max(Lambda(jj,jj),0.0_8)
          End Do
       Else 
          Do jj = 1, ND+2
             Lambda(jj,jj) = min(Lambda(jj,jj),0.0_8)
          End Do
       End If
       
       ! ... modify Lambda if subsonic outflow
       ! ... left boundary
       if (sgn == 1 .and. &
            ucon < 0.0_8 .and. ucon + spd_snd > 0.0_8) then
          Lambda(ND+2,ND+1) = Lambda(ND+2,ND+1) - ucon - spd_snd
          ! right boundary
       else if (sgn == -1 .and. &
            ucon > 0.0_8 .and. ucon - spd_snd < 0.0_8) then
          Lambda(ND+1,ND+2) = Lambda(ND+1,ND+2) - ucon + spd_snd
       end if

       ! ... multiply Lambda' into X
       matX = MATMUL(Lambda,Tinv)
       
       ! ... compute the characteristic matrix
       Aprime = MATMUL(Tmat,matX)

       ! ... subtract off the target
       uB(1:ND+2) = uB(1:ND+2) - gI1(1:ND+2)

       ! ... compute the characteristic penalty vector
       gI2(1:ND+2) = MATMUL(Aprime,uB)

       penaltyFac = sbpBoundaryWeight*sigmaI1
       correction(:) = penaltyFac*gI2(:)
       ! ... compute penalty (Bndry_h is included in Lambda already)
       !       Do jj = 1, ND+2
       !          rhs(l0,jj) = rhs(l0,jj) + penaltyFac * gI2(jj)
       !       End Do
       rhoRHS(l0) = rhoRHS(l0) + correction(1)
       DO iDim = 1,numDim
          pointIndex = l0 + (iDim-1)*numPointsBuffer
          rhoVRHS(pointIndex) = rhoVRHS(pointIndex) + correction(1+iDim)
       END DO
       rhoERHS(l0) = rhoERHS(l0) + correction(numDim+2)

!       IF(TRIGGERED.eqv..FALSE.) THEN
!       WRITE(*,*) 'RHS: ',rhoRHS(l0),rhoVRHS(l0),rhoVRHS(l0+numPointsBuffer),rhoERHS(l0)
!       WRITE(*,*) 'CORRECTION: ',correction(:)
!          TRIGGERED = .TRUE.
!       ENDIF

       ! ... SAT FARFIELD (species treatment)
       IF(numscalar > 0) THEN
          If (dsgn * ucon > 0) Then
             Sfactor =  ucon * ibfac_local * penaltyFac
             rhoFac = rhoBuffer(l0)/rhoTarget(l0)
             scalarPenaltyFac = Sfactor*rhoFac
             Do jj = 1, numscalar
                pointIndex = (jj-1)*numPointsBuffer + l0
                scalarDiff = (scalarBuffer(pointIndex) - scalarTarget(pointIndex))
                scalarRHS(pointIndex) = scalarRHS(pointIndex) + scalarPenaltyFac * scalarDiff
                !             rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + Sfactor * &
                !                  (auxVars(l0,jj) - auxVarsTarget(l0,jj)/cvTarget(l0,1)*cv(l0,1))
             End do
          End If
       END IF

       ! VISCOUS PART 
       IF(REInv > 0.0) THEN
         ! ... factor
!         sigmaI2 = dsgn * SAT_sigmaI2_FF * SBP_invPdiag_block1(1) / bndry_h !* JAC(l0)
         sigmaI2 = dsgn * SAT_sigmaI2_FF*sbpBoundaryWeight/bndry_h !* JAC(l0)
          
         ! ... target state
         do jj = 1, ND+2
           gI2(jj) = 0.0_8
         end do

         ! ... boundary data (already includes 1/Re factor)
         uB(:) = ViscousFluxBuffer(l0) * XI_X_TILDE
         if (ND >= 2) &
              uB(:) = uB(:) + ViscousFluxBuffer(l0) * XI_Y_TILDE
         if (ND == 3) &
              uB(:) = uB(:) + ViscousFluxBuffer(l0) * XI_Z_TILDE
         uB(:) = 0
         ! ... penalty term
         penalty(:) = sigmaI2 * (uB(:) - gI2(:))

         ! ... add to the rhs
         rhoRHS(l0) = rhoRHS(l0) + ibfac_local * penalty(1)
         DO iDim = 1,numDim
           pointIndex = l0 + (iDim-1)*numPointsBuffer
           rhoVRHS(pointIndex) = rhoVRHS(pointIndex) + ibfac_local*penalty(1+iDim)
         END DO
         rhoERHS(l0) = rhoERHS(l0) + ibfac_local * penalty(numDim+2)
          
         ! species transport
!          sigmaAux = ibfac_local * sigmaI2
!          do jj = 1, nAuxVars
!             ubAux = patch_ViscousFluxAux(jj,lp,normDir) 
!             rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + sigmaAux* ubAux
!          end do
          
       END IF
    END DO

    DEALLOCATE(XIX)
    DEALLOCATE(correction)
    DEALLOCATE(gI1, uB, Tmat, Tinv, penalty)
    DEALLOCATE(gI2, Aprime, Lambda, matX, pnorm_vec)

    RETURN

  END SUBROUTINE FARFIELD

  SUBROUTINE SLIP_ADIABATIC(                                           &
       numDim,bufferSizes,numPointsBuffer,patchNormalDir,patchSizes,   &
       numPointsPatch,numPatchPointsOp,patchPointsOp,gridType,         &
       gridMetric,jacobianDeterminant,bcParams,gasParams,              &
       rhoBuffer,rhoVBuffer,rhoEBuffer,numscalar,scalarBuffer,   &
       rhoRHS,rhoVRHS,rhoERHS, scalarRHS,rhoTarget,rhoVTarget,      &
       rhoETarget,scalarTarget)
    
    IMPLICIT NONE

    INTEGER(KIND=4) :: numDim,numscalar,patchNormalDir,gridType
    INTEGER(KIND=8) :: numPointsBuffer,bufferSizes(numDim)
    INTEGER(KIND=8) :: patchSizes(numDim),numPointsPatch
    INTEGER(KIND=8) :: numPatchPointsOp
    INTEGER(KIND=8) :: patchPointsOp(numPatchPointsOp)
    REAL(KIND=8)    :: gridMetric(numDim*numDim*numPointsBuffer)
    REAL(KIND=8)    :: jacobianDeterminant(numPointsBuffer)
    REAL(KIND=8)    :: bcParams(3),gasParams(5)
    REAL(KIND=8)    :: rhoBuffer(numPointsBuffer)
    REAL(KIND=8)    :: rhoVBuffer(numDim*numPointsBuffer)
    REAL(KIND=8)    :: rhoEBuffer(numPointsBuffer)
    REAL(KIND=8)    :: scalarBuffer(numscalar*numPointsBuffer)
    REAL(KIND=8)    :: rhoRHS(numPointsBuffer)
    REAL(KIND=8)    :: rhoVRHS(numDim*numPointsBuffer)
    REAL(KIND=8)    :: rhoERHS(numPointsBuffer)
    REAL(KIND=8)    :: scalarRHS(numscalar*numPointsBuffer)
    REAL(KIND=8)    :: rhoTarget(numPointsBuffer)
    REAL(KIND=8)    :: rhoVTarget(numDim*numPointsBuffer)
    REAL(KIND=8)    :: rhoETarget(numPointsBuffer)
    REAL(KIND=8)    :: scalarTarget(numScalar*numPointsBuffer)


    ! ... Local variables
    INTEGER(4) :: ND, nAuxVars, jj, iDim, iDir
    INTEGER(8) :: Nc, l0, iPoint, pointIndex,metricOffset
    INTEGER(4) :: normDir, sgn ! , gas_dv_model
    !    INTEGER, Dimension(MAX_ND) :: N, Np
    !    INTEGER, Pointer :: iblank(:), t2Map(:,:)
    !    INTEGER, Pointer :: grid_is(:), grid_ie(:), patch_is(:), patch_ie(:)
    !    TYPE (t_grid), Pointer :: grid
    !    TYPE (t_mixt), Pointer :: state
    !    TYPE (t_mixt_input), Pointer :: input
    REAL(8) :: dsgn, sndspdref2, invtempref, tempref, gamref, spcGasConst
    REAL(8) :: XI_X, XI_Y, XI_Z, XI_T, bndry_h,sbpBoundaryWeight
    REAL(8) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, gridJacobian
    REAL(8) :: rhoFac, penaltyFac, scalarPenaltyFac, scalarDiff

    REAL(8), POINTER :: XIX(:), bVelocity(:), vWall(:), vWallTarget(:), vTarget(:)
    !    REAL(KIND=RFREAL), Pointer :: XI_TAU(:,:), cv(:,:), gv(:,:)
    REAL(8), POINTER :: pnorm_vec(:) ! , rhs(:,:)
    !    REAL(KIND=RFREAL), Pointer :: patch_ViscousFlux(:,:,:)
    !    REAL(KIND=RFREAL), Pointer :: patch_ViscousFluxAux(:,:,:)
    !    REAL(KIND=RFREAL), Pointer :: patch_ViscousRHSAux(:,:,:), rhs_auxVars(:,:)
    !    REAL(KIND=RFREAL), Pointer :: auxVars(:,:), Lambda(:,:), ibfac(:)
    REAL(8), POINTER :: Lambda(:,:), gI1(:), uB(:), Tinv(:,:), Tmat(:,:)
    !    REAL(KIND=RFREAL), Pointer :: gI1(:), ub(:), Tinv(:,:), Tmat(:,:)
    REAL(8), POINTER :: Aprime(:,:), gI2(:), matX(:,:), penalty(:) ,correction(:)
    !    REAL(KIND=RFREAL), Pointer :: auxVarsTarget(:,:), cvTarget(:,:)
    !    REAL(KIND=RFREAL), Pointer :: SBP_invPdiag_block1(:), JAC(:), MT1(:,:)
    REAL(8) :: norm_vec(3), gamma, uCon, ubAux, specRad, vDotXi, xh
    REAL(8) :: sigmaI1, sigmaI2, sigmaAux, Sfactor, ibfac_local
    REAL(8) :: spd_snd, RE, REInv, SAT_sigmaI1,pressureTarget

    LOGICAL :: TRIGGERED
    TRIGGERED = .FALSE.

    ! ... Main data containers
    !    patch_gridID = patch%gridID
    !    grid  => region%grid(patch_gridID)
    !    state => region%state(patch_gridID)
    !    input => grid%input

    ! ... Pointer dereferences
    !    grid_is                    => grid%is
    !    grid_ie                    => grid%ie
    !    patch_is                   => patch%is
    !    patch_ie                   => patch%ie
    !    cv                         => state%cv
    !    gv                         => state%gv
    !    cvTarget                   => state%cvTarget
    !    rhs                        => state%rhs
    !    iblank                     => grid%iblank
    !    JAC                        => grid%JAC
    !    MT1                        => grid%MT1
    !    ibfac                      => grid%ibfac
    !    t2Map                      => region%global%t2Map
    !    XI_TAU                     => grid%XI_TAU
    !    rhs_auxVars                => state%rhs_auxVars
    !    patch_ViscousFlux          => patch%ViscousFlux
    !    patch_ViscousFluxAux       => patch%ViscousFluxAux
    !    SBP_invPdiag_block1        => grid%SBP_invPdiag_block1
    !    auxVars                    => state%auxVars
    !    auxVarsTarget              => state%auxVarsTarget

    ! ... Problem size
    ND = numDim
    Nc = numPointsBuffer

    nAuxVars = numscalar

    ! ... Useful quantities about this boundary
    normDir = abs(patchNormalDir)
    sgn = normDir / patchNormalDir
    dsgn = dble(sgn)


    ! ... Reference quantities
    ! gasParams = (sndspdref,gamma,Cp,1/Re)
    ! bcParams(:)  = sigma1 sigma2
    ! MJA changed for new nonDimensionalization
    !sndspdref2 = gasParams(1)*gasParams(1) ! input%sndspdref * input%sndspdref
    !invtempref = gasParams(3)/sndspdref2  ! input%Cpref / sndspdref2
    !tempref    = 1.0_8 / invtempref 
    !gamref     = gasParams(2)
    !REinv      = gasParams(4)
    gamref     = gasParams(2)
    spcGasConst= gasParams(3)
    sndspdref2 = gasParams(5)*gasParams(5)
    tempref    = gasParams(4)
    invtempref = tempref
    RE         = gasParams(1)
    REinv      = 1.0_8/RE

    ! ... BC Constants
    ! bcParams = (sigma1,sbpBoundaryStencilWeight)
    SAT_sigmaI1                 = bcParams(1) ! input%SAT_sigmaI1_FF
    sbpBoundaryWeight           = bcParams(2)

    !    gas_dv_model                = input%gas_dv_model

    ! ... memory allocation
    ALLOCATE(XIX(4),vWall(ND),bVelocity(ND),vWallTarget(ND),vTarget(ND))
    ALLOCATE(gI1(ND+2), uB(ND+2), Tmat(ND+2,ND+2), Tinv(ND+2,ND+2), penalty(ND+2),correction(ND+2))
    ALLOCATE(gI2(ND+2), Aprime(ND+2,ND+2), Lambda(ND+2,ND+2), matX(ND+2,ND+2), pnorm_vec(ND))
    
    XIX(:) = 0.0_8
    IF(gridType <= UNIRECT) THEN
       XIX(normDir) = gridMetric(normDir)
       metricOffset = 0
       gridJacobian = jacobianDeterminant(1)
    ELSE IF(gridType == RECTILINEAR) THEN
       metricOffset = (normDir-1)*numPointsBuffer
    ELSE
       metricOffset = (normDir-1)*numDim*numPointsBuffer
    ENDIF

!    XIX(:) = 0.0_8
!    XIX(normDir) = gridMetric(normDir) ! only diagonal terms are non-zero

!    WRITE(*,*) 'XIX = ',XIX
!    WRITE(*,*) 'NORMAL DIR: ',patchNormalDir
!    WRITE(*,*) 'SNDSPDREF2 = ',sndspdref2 
!    WRITE(*,*) 'tempref = ',tempref
!    WRITE(*,*) 'gamref = ',gamref
!    WRITE(*,*) 'sigma1 = ',SAT_sigmaI1
!    WRITE(*,*) 'REInv = ',REinv
!    WRITE(*,*) 'Bweight = ',sbpBoundaryWeight
!    WRITE(*,*) 'JAC = ',jacobianDeterminant

!    TRIGGERED = .TRUE.

    ! ... loop over all of the points
    DO iPoint = 1, numPatchPointsOp

       l0 = patchPointsOp(iPoint) + 1 ! assume the points coming in are from C (0-based)
       !          lp = (k-patch_is(3))*Np(1)*Np(2) + (j-patch_is(2))*Np(1) + (i-patch_is(1)+1)

       !       IF(iPoint == (numPatchPointsOp/2)) TRIGGERED = .FALSE.

       ! ... iblank factor (either 1 or zero)
       ibfac_local = 1.0_8 ! ibfac(l0)

       ! ... construct the normalized metrics
       !       XI_X = MT1(l0,t2Map(normDir,1)); XI_Y = 0.0_8; XI_Z = 0.0_8
       !       if (ND >= 2) XI_Y = MT1(l0,t2Map(normDir,2))
       !       if (ND == 3) XI_Z = MT1(l0,t2Map(normDir,3))
       !       XI_T = XI_TAU(l0,normDir)
       !       IF(ND > 1)  XI_Y = gridMetric(2)
       !      IF(ND > 2)  XI_Z = gridMetric(3)
       
       
       IF(gridType == CURVILINEAR ) THEN
          DO iDir = 1,numDim
             XIX(iDir) = gridMetric(metricOffset + (iDir-1)*numPointsBuffer +l0)
          END DO
          gridJacobian = jacobianDeterminant(l0)
       ELSE IF(gridType == RECTILINEAR) THEN          
          XIX(normDir) = gridMetric(metricOffset + l0)
          gridJacobian = jacobianDeterminant(l0)
       ENDIF

       ! ... multiply by the Jacobian
       XI_X = XIX(1) * gridJacobian  ! JAC(l0)
       XI_Y = XIX(2) * gridJacobian  ! JAC(l0)
       XI_Z = XIX(3) * gridJacobian  ! JAC(l0)
       XI_T = XIX(4) * gridJacobian  ! JAC(l0)

       ! ... normalized metrics
       xh = sqrt( (XI_X * XI_X) + (XI_Y * XI_Y) + (XI_Z * XI_Z) )
       bndry_h = 1.0_8 / xh
       XI_X_TILDE = XI_X * bndry_h
       XI_Y_TILDE = XI_Y * bndry_h
       XI_Z_TILDE = XI_Z * bndry_h
       

       ! ... follow Magnus's algorithm (norm_vec has unit length)
       norm_vec(1:3) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)
       pnorm_vec(:) = norm_vec(1:ND) * xh


       ! ... penalty parameter
       sigmaI1 = -SAT_sigmaI1 * dsgn
       
       ! ... inviscid penalty parameter
       !       sigmaI1 = -SAT_sigmaI1_FF * dsgn

       ! ... pull off the boundary data 
       !       uB(:) = cv(l0,:)
       uB(1) = rhoBuffer(l0)
       vDotXi = 0
       DO iDim = 1, numDim
          uB(1+iDim) = rhoVBuffer(l0+(iDim-1)*numPointsBuffer)
          bVelocity(iDim) = rhoVBuffer(l0+(iDim-1)*numPointsBuffer)/rhoBuffer(l0)
          vDotXi = vDotXi + bVelocity(iDim)*XIX(iDim)*gridJacobian*bndry_h
       END DO
       uB(numDim+2) = rhoEBuffer(l0)

       vWall(1:numDim) = vDotXi*norm_vec(1:numDim)
       ! NON MOVING GRIDS!!
       vWallTarget(1:numDim) = 0.0_8
       !       = dot_product(XYZ_TAU(l0,1:ND),norm_vec(1:ND)) * norm_vec(1:ND) 


       DO iDim = 1,numDim
          vTarget(iDim) = bVelocity(iDim) - ( vWall(iDim) - vWallTarget(iDim) )
       END DO
!       pressureTarget = pressureBuffer(l0)

       ! ... target data
       !       gI1(:) = cvTarget(l0,:)
       gI1(1) = uB(1)
       gI1(numDim+2) = uB(numDim+2)
       DO iDim = 1, numDim
          gI1(1+iDim)   = gI1(1) * vTarget(iDim)
          gI1(numDim+2) = gI1(numDim+2) + 0.5_8*gI1(1)*(vTarget(iDim)**2 - bVelocity(iDim)**2)
       END DO


       ! ... use target data to make implicit work better?
       GAMMA = gamref ! gv(l0,1)
       !       If (gas_dv_model == DV_MODEL_IDEALGAS .or. &
       !            gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
       

!       IF(TRIGGERED .EQV. .FALSE.) THEN
!          WRITE(*,*) 'L0 = ',l0 
!          WRITE(*,*) 'XIX = ',XIX
!          WRITE(*,*) '1/xh = ',1.0_8/xh
!          WRITE(*,*) 'VdotXi = ',vdotxi
!          WRITE(*,*) 'bVelocity = ',bVelocity
!          WRITE(*,*) 'pnorm:  ',pnorm_vec
!          WRITE(*,*) 'State:  ',uB
!          WRITE(*,*) 'Target: ',gI1
!          TRIGGERED = .TRUE.
!       ENDIF

       CALL SAT_Form_Roe_Matrices(ND, uB, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)

       specRad = 0.0_8
       uCon    = vDotXi*xh

       ! ... only pick those Lambda that are incoming
       If ( sgn == 1 ) Then
          Do jj = 1, ND+2
             Lambda(jj,jj) = max(Lambda(jj,jj),0.0_8)
             specRad = max(abs(Lambda(jj,jj)),abs(specRad))
          End Do
       Else 
          Do jj = 1, ND+2
             Lambda(jj,jj) = min(Lambda(jj,jj),0.0_8)
             specRad = max(abs(Lambda(jj,jj)),abs(specRad))
          End Do
       End If

!       IF(TRIGGERED.eqv..FALSE.) THEN
!          WRITE(*,*) 'XI_X_TILDE: ',XI_X_TILDE
!          WRITE(*,*) 'XI_Y_TILDE: ',XI_Y_TILDE
!          WRITE(*,*) 'XI_Z_TILDE: ',XI_Z_TILDE
!          WRITE(*,*) 'XI_X: ',XI_X
!          WRITE(*,*) 'XI_Y: ',XI_Y
!          WRITE(*,*) 'XI_Z: ',XI_Z
!          WRITE(*,*) 'BOUNDARYH:  ',bndry_h
!          WRITE(*,*) 'STATE: ',uB(:)
!          WRITE(*,*) 'TARGET:',gI1(:)
!          WRITE(*,*) 'UCON: ',uCon
!          WRITE(*,*) 'SpecRad: ',specRad
!          WRITE(*,*) 'vDotXi: ',vDotXi
!       ENDIF

       matX = MATMUL(Lambda,Tinv)
       
       ! ... compute the characteristic matrix
       Aprime = MATMUL(Tmat,matX)

       ! ... subtract off the target
       uB(1:ND+2) = uB(1:ND+2) - gI1(1:ND+2)

       ! ... compute the characteristic penalty vector
       gI2(1:ND+2) = MATMUL(Aprime,uB)

       penaltyFac = sbpBoundaryWeight*sigmaI1
       correction(:) = penaltyFac*gI2(:)

       ! IBFAC would be considered here
       rhoRHS(l0) = rhoRHS(l0) + ibfac_local * correction(1)
       DO iDim = 1,numDim
          pointIndex = l0 + (iDim-1)*numPointsBuffer
          rhoVRHS(pointIndex) = rhoVRHS(pointIndex) + ibfac_local*correction(1+iDim)
       END DO
       rhoERHS(l0) = rhoERHS(l0) + ibfac_local * correction(numDim+2)
       
!       IF(TRIGGERED.eqv..FALSE.) THEN
!          WRITE(*,*) 'CORRECTION(',l0,'): ',correction(:)
!          TRIGGERED = .TRUE.
!       ENDIF

       ! ... SAT_SLIP_ADIABATIC (species treatment)
       IF(numscalar > 0) THEN
          If (dsgn * uCon > 0) Then
             Sfactor =  -uCon * dsgn * ibfac_local * sbpBoundaryWeight
             Do jj = 1, numscalar
                pointIndex = (jj-1)*numPointsBuffer + l0
                scalarRHS(pointIndex) = scalarRHS(pointIndex) + Sfactor*scalarBuffer(pointIndex)
             End do
          End If
       END IF

       !> @todo add viscous contribution for SAT_SLIP_ADIABATIC
       ! VISCOUS PART OMITTED (for now)


!                  if ( REinv > 0.0_rfreal ) then
!                    sigmaI2 = ibfac_local * dsgn * 1_8 * SBP_invPdiag_block1(1)  * JAC(l0)
!                    gI2 = 0_8
!                    uB(:) = patch_ViscousFlux(:,lp,normDir)
!                    penalty(:) = sigmaI2 * (uB(:) - gI2(:))
!                    penalty(normDir+1) = 0_8   !normal momentum
!                    do jj = 1, ND+2
!                      rhs(l0,jj) = rhs(l0,jj) + penalty(jj)
!                    end do
!                    do jj = 1, nAuxVars
!                      ubAux = patch_ViscousFluxAux(jj,lp,normDir) 
!                      rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + sigmaI2* ubAux
!                    end do
!                  endif !REinv > 0.0_rfreal


    END DO

    DEALLOCATE(XIX,vWall,bVelocity,vWallTarget,vTarget)
    DEALLOCATE(gI1, uB, Tmat, Tinv, penalty, correction)
    DEALLOCATE(gI2, Aprime, Lambda, matX, pnorm_vec)

    RETURN

  END SUBROUTINE SLIP_ADIABATIC

  SUBROUTINE NOSLIP_ISOTHERMAL(                                        &
       numDim,bufferSizes,numPointsBuffer,patchNormalDir,patchSizes,   &
       numPointsPatch,numPatchPointsOp,patchPointsOp,gridType,         &
       gridMetric,jacobianDeterminant,bcParams,gasParams,              &
       rhoBuffer,rhoVBuffer,rhoEBuffer,numscalar,scalarBuffer,   &
       rhoRHS,rhoVRHS,rhoERHS, scalarRHS,rhoTarget,rhoVTarget,      &
       rhoETarget,scalarTarget,muBuffer,lambdaBuffer)
    
    IMPLICIT NONE

    INTEGER(KIND=4) :: numDim,numscalar,patchNormalDir,gridType
    INTEGER(KIND=8) :: numPointsBuffer,bufferSizes(numDim)
    INTEGER(KIND=8) :: patchSizes(numDim),numPointsPatch
    INTEGER(KIND=8) :: numPatchPointsOp
    INTEGER(KIND=8) :: patchPointsOp(numPatchPointsOp)
    REAL(KIND=8)    :: gridMetric(numDim*numDim*numPointsBuffer)
    REAL(KIND=8)    :: jacobianDeterminant(numPointsBuffer)
    REAL(KIND=8)    :: bcParams(4),gasParams(6)
    REAL(KIND=8)    :: rhoBuffer(numPointsBuffer)
    REAL(KIND=8)    :: rhoVBuffer(numDim*numPointsBuffer)
    REAL(KIND=8)    :: rhoEBuffer(numPointsBuffer)
    REAL(KIND=8)    :: scalarBuffer(numscalar*numPointsBuffer)
    REAL(KIND=8)    :: rhoRHS(numPointsBuffer)
    REAL(KIND=8)    :: rhoVRHS(numDim*numPointsBuffer)
    REAL(KIND=8)    :: rhoERHS(numPointsBuffer)
    REAL(KIND=8)    :: scalarRHS(numscalar*numPointsBuffer)
    REAL(KIND=8)    :: rhoTarget(numPointsBuffer)
    REAL(KIND=8)    :: rhoVTarget(numDim*numPointsBuffer)
    REAL(KIND=8)    :: rhoETarget(numPointsBuffer)
    REAL(KIND=8)    :: scalarTarget(numScalar*numPointsBuffer)
    REAL(KIND=8)    :: muBuffer(numPointsBuffer)
    REAL(KIND=8)    :: lambdaBuffer(numPointsBuffer)


    ! ... Local variables
    INTEGER(4) :: ND, nAuxVars, jj, iDim, iDir
    INTEGER(8) :: Nc, l0, iPoint, pointIndex, metricOffset
    INTEGER(4) :: normDir, sgn ! , gas_dv_model
    INTEGER(4) :: numDebugPoints, iDebugPoint,debugPoints(6)
    !    INTEGER, Dimension(MAX_ND) :: N, Np
    !    INTEGER, Pointer :: iblank(:), t2Map(:,:)
    !    INTEGER, Pointer :: grid_is(:), grid_ie(:), patch_is(:), patch_ie(:)
    !    TYPE (t_grid), Pointer :: grid
    !    TYPE (t_mixt), Pointer :: state
    !    TYPE (t_mixt_input), Pointer :: input
    REAL(8) :: dsgn, sndspdref2, invtempref, tempref, gamref, spcGasConst
    REAL(8) :: XI_X, XI_Y, XI_Z, XI_T, bndry_h,sbpBoundaryWeight, bcWallTemp
    REAL(8) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, gridJacobian
    REAL(8) :: rhoFac, penaltyFac, scalarPenaltyFac, scalarDiff

    REAL(8), POINTER :: XIX(:), bVelocity(:), vWall(:), vWallTarget(:), vTarget(:)
    !    REAL(KIND=RFREAL), Pointer :: XI_TAU(:,:), cv(:,:), gv(:,:)
    REAL(8), POINTER :: pnorm_vec(:) ! , rhs(:,:)
    !    REAL(KIND=RFREAL), Pointer :: patch_ViscousFlux(:,:,:)
    !    REAL(KIND=RFREAL), Pointer :: patch_ViscousFluxAux(:,:,:)
    !    REAL(KIND=RFREAL), Pointer :: patch_ViscousRHSAux(:,:,:), rhs_auxVars(:,:)
    !    REAL(KIND=RFREAL), Pointer :: auxVars(:,:), Lambda(:,:), ibfac(:)
    REAL(8), POINTER :: Lambda(:,:), gI1(:), uB(:), Tinv(:,:), Tmat(:,:)
    !    REAL(KIND=RFREAL), Pointer :: gI1(:), ub(:), Tinv(:,:), Tmat(:,:)
    REAL(8), POINTER :: Aprime(:,:), gI2(:), matX(:,:), penalty(:) ,correction(:)
    !    REAL(KIND=RFREAL), Pointer :: auxVarsTarget(:,:), cvTarget(:,:)
    !    REAL(KIND=RFREAL), Pointer :: SBP_invPdiag_block1(:), JAC(:), MT1(:,:)
    REAL(8) :: norm_vec(3), gamma, uCon, ubAux, specRad, vDotXi, xh
    REAL(8) :: sigmaI1, sigmaI2, sigmaAux, Sfactor, ibfac_local, T_wall
    REAL(8) :: spd_snd, RE, REInv, SAT_sigmaI1, SAT_sigmaI2, pressureTarget
    REAL(8) :: viscousPenaltyScale

    LOGICAL :: TRIGGERED
    TRIGGERED = .FALSE.

    ! ... Main data containers
    !    patch_gridID = patch%gridID
    !    grid  => region%grid(patch_gridID)
    !    state => region%state(patch_gridID)
    !    input => grid%input

    ! ... Pointer dereferences
    !    grid_is                    => grid%is
    !    grid_ie                    => grid%ie
    !    patch_is                   => patch%is
    !    patch_ie                   => patch%ie
    !    cv                         => state%cv
    !    gv                         => state%gv
    !    cvTarget                   => state%cvTarget
    !    rhs                        => state%rhs
    !    iblank                     => grid%iblank
    !    JAC                        => grid%JAC
    !    MT1                        => grid%MT1
    !    ibfac                      => grid%ibfac
    !    t2Map                      => region%global%t2Map
    !    XI_TAU                     => grid%XI_TAU
    !    rhs_auxVars                => state%rhs_auxVars
    !    patch_ViscousFlux          => patch%ViscousFlux
    !    patch_ViscousFluxAux       => patch%ViscousFluxAux
    !    SBP_invPdiag_block1        => grid%SBP_invPdiag_block1
    !    auxVars                    => state%auxVars
    !    auxVarsTarget              => state%auxVarsTarget

    ! ... Problem size
    ND = numDim
    Nc = numPointsBuffer

    nAuxVars = numscalar

    ! ... Useful quantities about this boundary
    normDir = abs(patchNormalDir)
    sgn = normDir / patchNormalDir
    dsgn = dble(sgn)
    numDebugPoints = 0

    ! ... Reference quantities
    ! gasParams = (sndspdref,gamma,Cp,1/Re)
    ! bcParams(:)  = sigma1 sigma2
    ! MJA changed for new nonDimensionalization
    !sndspdref2 = gasParams(1)*gasParams(1) ! input%sndspdref * input%sndspdref
    !invtempref = gasParams(3)/sndspdref2  ! input%Cpref / sndspdref2
    !tempref    = 1.0_8 / invtempref 
    !gamref     = gasParams(2)
    !REinv      = gasParams(4)
    gamref     = gasParams(2)
    spcGasConst= gasParams(3)
    sndspdref2 = gasParams(5)*gasParams(5)
    tempref    = gasParams(4)
    invtempref = tempref
    RE         = gasParams(1)
    REinv      = 1.0_8/RE
    viscousPenaltyScale = gasParams(6)

    ! ... BC Constants
    ! bcParams = (sigma1,sbpBoundaryStencilWeight)
    SAT_sigmaI1                 = bcParams(1) ! input%SAT_sigmaI1_FF
    SAT_sigmaI2                 = bcParams(2) ! /input%SAT_sigmaI2
    bcWallTemp                  = bcParams(3) ! input%bcic_wallTemp
    sbpBoundaryWeight           = bcParams(4) ! 

    !write(*,*) "SAT_sigmaI1 SAT_sigmaI2 bcWallTemp sbpBoundaryWeight tempRef"
    !write(*,*) SAT_sigmaI1, SAT_sigmaI2, bcWallTemp, sbpBoundaryWeight, tempRef

    !    gas_dv_model                = input%gas_dv_model

    ! ... memory allocation
    ALLOCATE(XIX(4),vWall(ND),bVelocity(ND),vWallTarget(ND),vTarget(ND))
    ALLOCATE(gI1(ND+2), uB(ND+2), Tmat(ND+2,ND+2), Tinv(ND+2,ND+2), penalty(ND+2),correction(ND+2))
    ALLOCATE(gI2(ND+2), Aprime(ND+2,ND+2), Lambda(ND+2,ND+2), matX(ND+2,ND+2), pnorm_vec(ND))
    
    XIX(:) = 0.0_8
    IF(gridType <= UNIRECT) THEN
       XIX(normDir) = gridMetric(normDir)
       metricOffset = 0
       gridJacobian = jacobianDeterminant(1)
    ELSE IF(gridType == RECTILINEAR) THEN
       metricOffset = (normDir-1)*numPointsBuffer
    ELSE
       metricOffset = (normDir-1)*numDim*numPointsBuffer
    ENDIF

!    WRITE(*,*) 'XIX = ',XIX
!    WRITE(*,*) 'NORMAL DIR: ',patchNormalDir
!    WRITE(*,*) 'SNDSPDREF2 = ',sndspdref2 
!    WRITE(*,*) 'tempref = ',tempref
!    WRITE(*,*) 'gamref = ',gamref
!    WRITE(*,*) 'sigma1 = ',SAT_sigmaI1
!    WRITE(*,*) 'REInv = ',REinv
!    WRITE(*,*) 'Bweight = ',sbpBoundaryWeight
!    WRITE(*,*) 'JAC = ',jacobianDeterminant

!    TRIGGERED = .TRUE.
    debugPoints(:) = 0
    numDebugPoints = 6
    debugPoints(1) = 91427
    debugPoints(2) = 91428
    debugPoints(3) = 91678
    debugPoints(4) = 114268
    debugPoints(5) = 114269
    debugPoints(6) = 114018

    ! ... loop over all of the points
    DO iPoint = 1, numPatchPointsOp

       l0 = patchPointsOp(iPoint) + 1 ! assume the points coming in are from C (0-based)
       !          lp = (k-patch_is(3))*Np(1)*Np(2) + (j-patch_is(2))*Np(1) + (i-patch_is(1)+1)

       !       IF(iPoint == (numPatchPointsOp/2)) TRIGGERED = .FALSE.

       ! ... iblank factor (either 1 or zero)
       ibfac_local = 1.0_8 ! ibfac(l0)
       
       
       ! ... construct the normalized metrics
       !       XI_X = MT1(l0,t2Map(normDir,1)); XI_Y = 0.0_8; XI_Z = 0.0_8
       !       if (ND >= 2) XI_Y = MT1(l0,t2Map(normDir,2))
       !       if (ND == 3) XI_Z = MT1(l0,t2Map(normDir,3))
       !       XI_T = XI_TAU(l0,normDir)
       !       IF(ND > 1)  XI_Y = gridMetric(2)
       !      IF(ND > 2)  XI_Z = gridMetric(3)
       IF(gridType == CURVILINEAR ) THEN
          DO iDir = 1,numDim
             XIX(iDir) = gridMetric(metricOffset + (iDir-1)*numPointsBuffer +l0)
          END DO
          gridJacobian = jacobianDeterminant(l0)
       ELSE IF(gridType == RECTILINEAR) THEN          
          XIX(normDir) = gridMetric(metricOffset + l0)
          gridJacobian = jacobianDeterminant(l0)
       ENDIF
       
       
       ! ... multiply by the Jacobian
       XI_X = XIX(1) * gridJacobian  ! JAC(l0)
       XI_Y = XIX(2) * gridJacobian  ! JAC(l0)
       XI_Z = XIX(3) * gridJacobian  ! JAC(l0)
       XI_T = XIX(4) * gridJacobian  ! JAC(l0)

       ! ... normalized metrics
       xh = sqrt( (XI_X * XI_X) + (XI_Y * XI_Y) + (XI_Z * XI_Z) )
       bndry_h = 1.0_8 / xh
       XI_X_TILDE = XI_X * bndry_h
       XI_Y_TILDE = XI_Y * bndry_h
       XI_Z_TILDE = XI_Z * bndry_h
       

       ! ... follow Magnus's algorithm (norm_vec has unit length)
       norm_vec(1:3) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)
       pnorm_vec(:) = norm_vec(1:ND) * xh

       ! ... penalty parameter
       sigmaI1 = -SAT_sigmaI1 * dsgn
       
       ! ... inviscid penalty parameter
       !       sigmaI1 = -SAT_sigmaI1_FF * dsgn

       ! ... pull off the boundary data 
       !       uB(:) = cv(l0,:)
       uB(1) = rhoBuffer(l0)
       vDotXi = 0
       DO iDim = 1, numDim
          uB(1+iDim) = rhoVBuffer(l0+(iDim-1)*numPointsBuffer)
          bVelocity(iDim) = rhoVBuffer(l0+(iDim-1)*numPointsBuffer)/rhoBuffer(l0)
!          vDotXi = vDotXi + bVelocity(iDim)*XIX(iDim)*gridJacobian*bndry_h
          vDotXi = vDotXi + bVelocity(iDim)*norm_vec(iDim)
       END DO
       uB(numDim+2) = rhoEBuffer(l0)

       vWall(1:numDim) = vDotXi*norm_vec(1:numDim)
       ! NON MOVING GRIDS!!
       vWallTarget(1:numDim) = 0.0_8
       !       = dot_product(XYZ_TAU(l0,1:ND),norm_vec(1:ND)) * norm_vec(1:ND) 


       DO iDim = 1,numDim
          vTarget(iDim) = bVelocity(iDim) - ( vWall(iDim) - vWallTarget(iDim) )
       END DO

!       pressureTarget = pressureBuffer(l0)

       ! ... target data
       !       gI1(:) = cvTarget(l0,:)
       gI1(1) = uB(1)
       gI1(numDim+2) = uB(numDim+2)
       DO iDim = 1, numDim
          gI1(1+iDim)   = gI1(1) * vTarget(iDim)
          gI1(numDim+2) = gI1(numDim+2) + 0.5_8*gI1(1)*(vTarget(iDim)**2 - bVelocity(iDim)**2)
       END DO


       ! ... use target data to make implicit work better?
       GAMMA = gamref ! gv(l0,1)
       !       If (gas_dv_model == DV_MODEL_IDEALGAS .or. &
       !            gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
       

!       IF(TRIGGERED .EQV. .FALSE.) THEN
!          WRITE(*,*) 'L0 = ',l0 
!          WRITE(*,*) 'XIX = ',XIX
!          WRITE(*,*) '1/xh = ',1.0_8/xh
!          WRITE(*,*) 'VdotXi = ',vdotxi
!          WRITE(*,*) 'bVelocity = ',bVelocity
!          WRITE(*,*) 'pnorm:  ',pnorm_vec
!          WRITE(*,*) 'State:  ',uB
!          WRITE(*,*) 'Target: ',gI1
!          TRIGGERED = .TRUE.
!       ENDIF

!       DO iDebugPoint = 1, numDebugPoints
!          IF(l0 == debugPoints(iDebugPoint)) THEN
!             WRITE(*,*) 'Before Inviscid point ',l0 
!             WRITE(*,*) 'rho  = ',ub(1), ' rhoT  = ',gI1(1)
!             WRITE(*,*) 'rhoU = ',ub(2),' rhoUT = ',gI1(2)
!             WRITE(*,*) 'rhoV = ',ub(3),' rhoVT = ',gI1(3)
!             WRITE(*,*) 'rhoE = ',ub(4),' rhoET = ',gI1(4)
!             WRITE(*,*) 'U    = ',bVelocity(1), ' uT = ',vTarget(1)
!             WRITE(*,*) 'V    = ',bVelocity(2), ' uT = ',vTarget(2)
!             WRITE(*,*) 'uWall = ',vWall(1),' uWallTarget = ',vWallTarget(1)
!             WRITE(*,*) 'vWall = ',vWall(2),' vWallTarget = ',vWallTarget(2)
!             WRITE(*,*) 'PNORM = (',pnorm_vec,') vDotXi = ',vDotXi
!             WRITE(*,*) 'normvec = ',norm_vec
!          ENDIF
!       ENDDO

       CALL SAT_Form_Roe_Matrices(ND, uB, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)

       specRad = 0.0_8
       uCon    = vDotXi*xh

       ! ... only pick those Lambda that are incoming
       If ( sgn == 1 ) Then
          Do jj = 1, ND+2
             Lambda(jj,jj) = max(Lambda(jj,jj),0.0_8)
             specRad = max(abs(Lambda(jj,jj)),abs(specRad))
          End Do
       Else 
          Do jj = 1, ND+2
             Lambda(jj,jj) = min(Lambda(jj,jj),0.0_8)
             specRad = max(abs(Lambda(jj,jj)),abs(specRad))
          End Do
       End If

!       IF(TRIGGERED.eqv..FALSE.) THEN
!          WRITE(*,*) 'XI_X_TILDE: ',XI_X_TILDE
!          WRITE(*,*) 'XI_Y_TILDE: ',XI_Y_TILDE
!          WRITE(*,*) 'XI_Z_TILDE: ',XI_Z_TILDE
!          WRITE(*,*) 'XI_X: ',XI_X
!          WRITE(*,*) 'XI_Y: ',XI_Y
!          WRITE(*,*) 'XI_Z: ',XI_Z
!          WRITE(*,*) 'BOUNDARYH:  ',bndry_h
!          WRITE(*,*) 'STATE: ',uB(:)
!          WRITE(*,*) 'TARGET:',gI1(:)
!          WRITE(*,*) 'UCON: ',uCon
!          WRITE(*,*) 'SpecRad: ',specRad
!          WRITE(*,*) 'vDotXi: ',vDotXi
!       ENDIF
!       debugPoint = 114273

       matX = MATMUL(Lambda,Tinv)
       
       ! ... compute the characteristic matrix
       Aprime = MATMUL(Tmat,matX)

       ! ... subtract off the target
       uB(1:ND+2) = uB(1:ND+2) - gI1(1:ND+2)

       ! ... compute the characteristic penalty vector
       gI2(1:ND+2) = MATMUL(Aprime,uB)

       penaltyFac = sbpBoundaryWeight*sigmaI1
       correction(:) = penaltyFac*gI2(:)

!       DO iDebugPoint = 1, numDebugPoints
!          IF(l0 == debugPoints(iDebugPoint)) THEN
!             WRITE(*,*) 'Before Inviscid point ',l0 
!             WRITE(*,*) 'SIGMAI1 = ',sigmaI1,'bWeight = ',sbpBoundaryWeight
!             WRITE(*,*) 'rhoRHS  = ',rhoRHS(l0), ' dRho  = ',correction(1)
!             WRITE(*,*) 'rhoURHS = ',rhoVRHS(l0),' dRhoU = ',correction(2)
!             WRITE(*,*) 'rhoVRHS = ',rhoVRHS(l0+numPointsBuffer),' dRhoV = ',correction(3)
!             WRITE(*,*) 'rhoERHS = ',rhoERHS(l0),' dRhoE = ',correction(4)
!             WRITE(*,*) 'cpRho  = ',gI2(1)
!             WRITE(*,*) 'cpRhoU = ',gI2(2)
!             WRITE(*,*) 'cpRhoV = ',gI2(3)
!             WRITE(*,*) 'cpRhoE = ',gI2(4)
!          ENDIF
!       ENDDO

       ! IBFAC would be considered here
       ! Inviscid correction
       rhoRHS(l0) = rhoRHS(l0) + ibfac_local * correction(1)
       DO iDim = 1,numDim
          pointIndex = l0 + (iDim-1)*numPointsBuffer
          rhoVRHS(pointIndex) = rhoVRHS(pointIndex) + ibfac_local*correction(1+iDim)
       END DO
       rhoERHS(l0) = rhoERHS(l0) + ibfac_local * correction(numDim+2)
       

!       IF(TRIGGERED.eqv..FALSE.) THEN
!          WRITE(*,*) 'CORRECTION(',l0,'): ',correction(:)
!          TRIGGERED = .TRUE.
!       ENDIF
       ! ... Species treatment (inviscid part)
       IF(numscalar > 0) THEN
          If (dsgn * uCon > 0) Then
             Sfactor =  -uCon * dsgn * ibfac_local * sbpBoundaryWeight
             Do jj = 1, numscalar
                pointIndex = (jj-1)*numPointsBuffer + l0
                scalarRHS(pointIndex) = scalarRHS(pointIndex) + Sfactor*scalarBuffer(pointIndex)
             End do
          End If
       END IF


       ! VISCOUS PART

!       uB(:) = cv(l0,:)
       ! MJA, repeated from above
       uB(1) = rhoBuffer(l0)
       DO iDim = 1, numDim
          uB(1+iDim) = rhoVBuffer(l0+(iDim-1)*numPointsBuffer)
       END DO
       uB(numDim+2) = rhoEBuffer(l0)

       ! ... wall temperature
       ! T_wall = bcic_WallTemp / ((gamref - 1.0_8)*tempref)
       ! new nondimensionalization...can be moved outside this loop altogether
       T_wall = bcWallTemp / tempref

       ! ... target state
       ! gI2(1) = RHO_TARGET
       gI2(1) = rhoBuffer(l0)
               
       ! ... wall velocity
       ! gI2(2:ND+1) = XYZ_TAU(l0,1:ND) * RHO_TARGET
       ! gI2(2:ND+1) = 0.0_8
       ! MJA, non-moving meshes
       gI2(2:iDim+1) = 0.0_8

       ! ... thermally perfect or not                 
       ! if (gas_dv_model == DV_MODEL_IDEALGAS) then
       !   gI2(ND+2)   = RHO_TARGET * T_wall / GAMMA + 0.5_rfreal * SUM(gI2(2:ND+1)**2) / RHO_TARGET

       ! MJA, only implemented gas model right now
       ! old nondimen
       !gI2(numDim+2)   = rhoTarget(l0) * T_wall / gamref + 0.5_8 * SUM(gI2(2:iDim+1)**2) / gI2(1)
       ! new dimen, roll spcGasConst and (gamma-1) into one const outside of loop

       ! use a temperature from the input file, if that temperature is <0, 
       !use the target energy (temperature)
       IF (T_wall < 0) THEN
         gI2(numDim+2)   = rhoETarget(l0)
       ELSE
         gI2(numDim+2)   = gI2(1) * T_wall * spcGasConst / (gamref-1) &
                           + 0.5_8 * SUM(gI2(2:iDim+1)**2) / gI2(1)
       ENDIF

       ! sigmaI2 = -(SAT_sigmaI2 / 4.0_rfreal) * (SBP_invPdiag_block1(1)/bndry_h)**2 * &
       !     maxval(tv(l0,1:)) / RHO_TARGET * MAX(GAMMA / Pr, 5.0_rfreal / 3.0_rfreal)
       ! sigmaI2 = sigmaI2 * REinv
       sigmaI2 = -SAT_sigmaI2
!       penaltyFac = (sbpBoundaryWeight/bndry_h)**2*sigmaI2/4.0_8 * &
!                    REInv*5.0_8/3.0_8*max(muBuffer(l0),lambdaBuffer(l0))/rhoTarget(l0)
! TESTING MTC
       penaltyFac = (sbpBoundaryWeight/bndry_h)**2*sigmaI2/4.0_8 * &
                    REInv*viscousPenaltyScale*max(muBuffer(l0),lambdaBuffer(l0))/gI2(1)
       
       
       ! ... compute penalty
       ! ... compute penalty
       ! penalty(:) = sigmaI2 * (uB(1:ND+2) - gI2(1:ND+2))
       penalty(:) = penaltyFac * (uB(1:ND+2) - gI2(1:ND+2))

       ! ... add to the rhs
       ! do jj = 1, ND+2
       !    rhs(l0,jj) = rhs(l0,jj) + ibfac_local * penalty(jj)
       ! end do
       rhoRHS(l0) = rhoRHS(l0) + ibfac_local * penalty(1)
       DO iDim = 1,numDim
          pointIndex = l0 + (iDim-1)*numPointsBuffer
          rhoVRHS(pointIndex) = rhoVRHS(pointIndex) + ibfac_local*penalty(1+iDim)
       END DO
       rhoERHS(l0) = rhoERHS(l0) + ibfac_local * penalty(numDim+2)

!       DO iDebugPoint = 1, numDebugPoints
!          IF(l0 == debugPoints(iDebugPoint)) THEN
!             WRITE(*,*) 'After Viscous point ',l0 
!             WRITE(*,*) 'rhoRHS  = ',rhoRHS(l0), ' dRho  = ',penalty(1)
!             WRITE(*,*) 'rhoURHS = ',rhoVRHS(l0),' dRhoU = ',penalty(2)
!             WRITE(*,*) 'rhoVRHS = ',rhoVRHS(l0+numPointsBuffer),' dRhoV = ',penalty(3)
!             WRITE(*,*) 'rhoERHS = ',rhoERHS(l0),' dRhoE = ',penalty(4)
!             WRITE(*,*) 'gI2(1)  = ',gI2(1)
!             WRITE(*,*) 'gI2(2)  = ',gI2(2)
!             WRITE(*,*) 'gI2(3)  = ',gI2(3)
!             WRITE(*,*) 'gI2(4)  = ',gI2(4)
!          ENDIF
!       ENDDO

        !zero grad for species
       ! sigmaAux = ibfac_local * dsgn *1_8* SBP_invPdiag_block1(1) * JAC(l0)
       ! do jj = 1, nAuxVars
       !    ubAux = patch_ViscousFluxAux(jj,lp,normDir) 
       !    rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + sigmaAux* ubAux
       ! end do

       ! MJA
       ! species transport not fully implemented yet for viscous terms

    END DO

    DEALLOCATE(XIX,vWall,bVelocity,vWallTarget,vTarget)
    DEALLOCATE(gI1, uB, Tmat, Tinv, penalty, correction)
    DEALLOCATE(gI2, Aprime, Lambda, matX, pnorm_vec)

    RETURN

  END SUBROUTINE NOSLIP_ISOTHERMAL

  SUBROUTINE ROE_MAT(numDim,inCV,outCV, &
       gamma,normDir,tMat,tInv,lambda,status)

    IMPLICIT NONE

    INTEGER(4), INTENT(IN)  :: numDim
    REAL(8),    INTENT(IN)  :: inCV(numDim+2), outCV(numDim+2)
    REAL(8),    INTENT(IN)  :: gamma, normDir(numDim)
    REAL(8),    INTENT(OUT) :: tMat((numDim+2)*(numDim+2)),tInv((numDim+2)*(numDim+2)),lambda(numDim+2)
    INTEGER(4), INTENT(OUT) :: status

    REAL(8)    :: gammaM1,uCon,alpha,beta,phi2,theta,denomRoe,xiX,xiY,xiZ
    REAL(8)    :: xiXTilde,xiYTilde,xiZTilde
    REAL(8)    :: rhoIn,rhoV1In,rhoV2In,rhoV3In,rhoM1In,rhoEIn
    REAL(8)    :: rhoOut,rhoV1Out,rhoV2Out,rhoV3Out,rhoM1Out,rhoEOut
    REAL(8)    :: inV1,inV2,inV3,outV1,outV2,outV3
    REAL(8)    :: inKE,inIE,inP,inH
    REAL(8)    :: outKE,outIE,outP,outH
    REAL(8)    :: ccRoe,bbRoe,v1Roe,v2Roe,v3Roe,hRoe,keRoe
    REAL(8)    :: soundSpeedRoe
    INTEGER(4) :: iDim

    status = 0

    IF(numDim == 1 .OR. numDim > 3) THEN
       status = 1
       RETURN
    ENDIF

    ! ... constants
    gammaM1 = gamma - 1.0_8

    ! form primitives from "in"
    rhoIn   = inCV(1)
    rhoV1In = inCV(2)
    rhoV2In = inCV(3)
    rhoV3In = 0.0_8
    rhoM1In = 1.0/rhoIn

    inV1  = rhoM1In*rhoV1In
    inV2  = rhoM1In*rhoV2In

    IF(numDim == 2) THEN
       rhoEIn  = inCV(4)
    ELSE
       rhoV3In = inCV(4)
       rhoEIn  = inCV(5)
    ENDIF
    inV3  = rhoM1In*rhoV3In

    inKE  = 0.5_8 * (inV1**2 + inV2**2 + inV3**2)
    inIE  = rhoM1In*rhoEIn - inKE
    inP   = gammaM1*rhoIn*inIE
    inH   = (gamma/gammaM1)*inP/rhoIn + inKE


    ! form primitives from "out"
    rhoOut   = outCV(1)
    rhoV1Out = outCV(2)
    rhoV2Out = outCV(3)
    rhoM1Out = 1.0/rhoOut
    outV1  = rhoM1Out*rhoV1Out
    outV2  = rhoM1Out*rhoV2Out
    IF(numDim == 2) THEN
       rhoEOut = outCV(4)
    ELSE
       rhoEOut  = outCV(5)
       rhoV3Out = outCV(4)
    ENDIF
    outV3   = rhoM1Out*rhoV3Out

    outKE  = 0.5_8 * (outV1**2 + outV2**2 + outV3**2)
    outIE  = rhoM1Out*rhoEOut - outKE
    outP   = gammaM1*rhoOut*outIE
    outH   = (gamma/gammaM1)*outP/rhoOut + outKE

    ! Roe vars
    ccRoe  = SQRT(rhoOut/rhoIn)
    bbRoe  = 1.0_8 / (1.0_8 + ccRoe)
    v1Roe = (inV1 + outV1 * ccRoe) * bbRoe
    v2Roe = (inV2 + outV2 * ccRoe) * bbRoe
    v3Roe = (inV3 + outV3 * ccRoe) * bbRoe
    hRoe  = (inH + outH*ccRoe)*bbRoe
    keRoe = 0.5_8 * (v1Roe**2 + v2Roe**2 + v3Roe**2)

    IF((hRoe - keRoe) <= 0.0_8) THEN
       write (*,'(5(a,E20.8,1X))') "gamma = ", gamma, ", inKE = ", inKE, ", inIE = ", inIE, &
            ", inP = ", inP, ", inH = ", inH
       status = 1
       RETURN
    ENDIF

    soundSpeedRoe = SQRT(gammaM1*(hRoe - keRoe))

    ! ... variable renaming
    xiX = normDir(1)
    xiY = normDir(2)
    xiZ = 0.0_8
    IF(numDim == 3) xiZ = normDir(3)
    denomRoe = 1.0_8 / SQRT(xiX**2 + xiY**2 + xiZ**2)
    xiXTilde = xiX * denomRoe
    xiYTilde = xiY * denomRoe
    xiZTilde = xiZ * denomRoe

    uCon = v1Roe*xiX + v2Roe*xiY + v3Roe*xiZ

    ! ... directly from Pulliam & Chaussee
    alpha = 0.5_8*rhoIn/soundSpeedRoe
    beta = 1.0_8/(rhoIn*soundSpeedRoe)
    theta = xiXTilde*v1Roe + xiYTilde*v2Roe + xiZTilde*v3Roe
    phi2 = 0.5_8*(gammaM1)*(v1Roe**2 + v2Roe**2 + v3Roe**2)

    ! ... compute the diagonal matrix lambda
    lambda(:) = 0.0_8
    DO iDim = 1, numDim
       Lambda(iDim) = ucon
    END DO
    lambda(numDim+1) = ucon + soundSpeedRoe * SQRT(xiX**2 + xiY**2 + xiZ**2)
    lambda(numDim+2) = ucon - soundSpeedRoe * SQRT(xiX**2 + xiY**2 + xiZ**2)

    if (numDim == 2) then

       tMat(1)  = 1.0_8
       tMat(2)  = v1Roe
       tMat(3)  = v2Roe
       tMat(4)  = phi2/gammaM1

       tMat(5)  = 0.0_8
       tMat(6)  = xiYTilde*rhoIn
       tMat(7)  = -xiXTilde*rhoIn
       tMat(8)  = rhoIn*(xiYTilde*v1Roe - xiXTilde*v2Roe)

       tMat(9)  = alpha
       tMat(10) = alpha*(v1Roe + xiXTilde*soundSpeedRoe)
       tMat(11) = alpha*(v2Roe + xiYTilde*soundSpeedRoe)
       tMat(12) = alpha*((phi2 + soundSpeedRoe**2)/gammaM1 + soundSpeedRoe*theta)

       tMat(13) = alpha
       tMat(14) = alpha*(v1Roe - xiXTilde*soundSpeedRoe)
       tMat(15) = alpha*(v2Roe - xiYTilde*soundSpeedRoe)
       tMat(16) = alpha*((phi2 + soundSpeedRoe**2)/gammaM1 - soundSpeedRoe*theta)

       Tinv(1)  = 1.0_8 - phi2 / soundSpeedRoe**2
       Tinv(2)  = -(xiYTilde * v1Roe - xiXTilde * v2Roe) / rhoIn
       Tinv(3)  = beta * (phi2 - soundSpeedRoe * theta)
       Tinv(4)  = beta * (phi2 + soundSpeedRoe * theta)

       Tinv(5)  = gammaM1 * v1Roe / soundSpeedRoe**2
       Tinv(6)  =  xiYTilde / rhoIn
       Tinv(7)  = beta * (xiXTilde * soundSpeedRoe - gammaM1 * v1Roe)
       Tinv(8)  = -beta * (xiXTilde * soundSpeedRoe + gammaM1 * v1Roe)

       Tinv(9)  = gammaM1 * v2Roe / soundSpeedRoe**2
       Tinv(10) = -xiXTilde / rhoIn
       Tinv(11) = beta * (xiYTilde * soundSpeedRoe - gammaM1 * v2Roe)
       Tinv(12) = -beta * (xiYTilde * soundSpeedRoe + gammaM1 * v2Roe)

       Tinv(13) = -gammaM1 / soundSpeedRoe**2
       Tinv(14) = 0.0_8
       Tinv(15) = beta * gammaM1
       Tinv(16) = beta * gammaM1

    ELSE IF (numDim == 3) THEN


       tMat(1) = xiXTilde
       tMat(2) = xiXTilde * v1Roe
       tMat(3) = xiXTilde * v2Roe + xiZTilde * rhoIn
       tMat(4) = xiXTilde * v3Roe - xiYTilde * rhoIn
       tMat(5) = xiXTilde * phi2 / gammaM1 + rhoIn * (xiZTilde * v2Roe - xiYTilde * v3Roe)

       tMat(6) = xiYTilde
       tMat(7) = xiYTilde * v1Roe - xiZTilde * rhoIn
       tMat(8) = xiYTilde * v2Roe
       tMat(9) = xiYTilde * v3Roe + xiXTilde * rhoIn
       tMat(10) = xiYTilde * phi2 / gammaM1 + rhoIn * (xiXTilde * v3Roe - xiZTilde * v1Roe)

       tMat(11) = xiZTilde
       tMat(12) = xiZTilde * v1Roe + xiYTilde * rhoIn
       tMat(13) = xiZTilde * v2Roe - xiXTilde * rhoIn
       tMat(14) = xiZTilde * v3Roe
       tMat(15) = xiZTilde * phi2 / gammaM1 + rhoIn * (xiYTilde * v1Roe - xiXTilde * v2Roe)

       tMat(16) = alpha
       tMat(17) = alpha * (v1Roe + xiXTilde * soundSpeedRoe)
       tMat(18) = alpha * (v2Roe + xiYTilde * soundSpeedRoe)
       tMat(19) = alpha * (v3Roe + xiZTilde * soundSpeedRoe)
       tMat(20) = alpha * ((phi2 + soundSpeedRoe**2)/gammaM1 + soundSpeedRoe * THETA)

       tMat(21) = alpha
       tMat(22) = alpha * (v1Roe - xiXTilde * soundSpeedRoe)
       tMat(23) = alpha * (v2Roe - xiYTilde * soundSpeedRoe)
       tMat(24) = alpha * (v3Roe - xiZTilde * soundSpeedRoe)
       tMat(25) = alpha * ((phi2 + soundSpeedRoe**2)/gammaM1 - soundSpeedRoe * THETA)

       Tinv(1) =  xiXTilde * (1.0_8 - phi2 / soundSpeedRoe**2) - (xiZTilde * v2Roe - xiYTilde * v3Roe) / rhoIn
       Tinv(2) =  xiYTilde * (1.0_8 - phi2 / soundSpeedRoe**2) - (xiXTilde * v3Roe - xiZTilde * v1Roe) / rhoIn
       Tinv(3) =  xiZTilde * (1.0_8 - phi2 / soundSpeedRoe**2) - (xiYTilde * v1Roe - xiXTilde * v2Roe) / rhoIn
       Tinv(4) =  beta * (phi2 - soundSpeedRoe * theta)
       Tinv(5) =  beta * (phi2 + soundSpeedRoe * theta)

       Tinv(6) =  xiXTilde * gammaM1 * v1Roe / soundSpeedRoe**2
       Tinv(7) = -xiZTilde / rhoIn + xiYTilde * gammaM1 * v1Roe / soundSpeedRoe**2
       Tinv(8) =  xiYTilde / rhoIn + xiZTilde * gammaM1 * v1Roe / soundSpeedRoe**2
       Tinv(9) =  beta * (xiXTilde * soundSpeedRoe - gammaM1 * v1Roe)
       Tinv(10) = -beta * (xiXTilde * soundSpeedRoe + gammaM1 * v1Roe)

       Tinv(11) =  xiZTilde / rhoIn + xiXTilde * gammaM1 * v2Roe / soundSpeedRoe**2
       Tinv(12) =  xiYTilde * gammaM1 * v2Roe / soundSpeedRoe**2
       Tinv(13) = -xiXTilde / rhoIn + xiZTilde * gammaM1 * v2Roe / soundSpeedRoe**2
       Tinv(14) =  beta * (xiYTilde * soundSpeedRoe - gammaM1 * v2Roe)
       Tinv(15) = -beta * (xiYTilde * soundSpeedRoe + gammaM1 * v2Roe)

       Tinv(16) = -xiYTilde / rhoIn + xiXTilde * gammaM1 * v3Roe / soundSpeedRoe**2
       Tinv(17) =  xiXTilde / rhoIn + xiYTilde * gammaM1 * v3Roe / soundSpeedRoe**2
       Tinv(18) =  xiZTilde * gammaM1 * v3Roe / soundSpeedRoe**2
       Tinv(19) =  beta * (xiZTilde * soundSpeedRoe - gammaM1 * v3Roe)
       Tinv(20) = -beta * (xiZTilde * soundSpeedRoe + gammaM1 * v3Roe)

       Tinv(21) = -xiXTilde * gammaM1 / soundSpeedRoe**2
       Tinv(22) = -xiYTilde * gammaM1 / soundSpeedRoe**2
       Tinv(23) = -xiZTilde * gammaM1 / soundSpeedRoe**2
       Tinv(24) =  beta * gammaM1
       Tinv(25) =  beta * gammaM1


    END IF
  
  END SUBROUTINE ROE_MAT




  subroutine SAT_Form_Roe_Matrices_TP(u_in, u_out, ND, gamma, gamref, norm, tmat, tinv, lambda)

!    USE ModGlobal
!    USE ModMPI
    IMPLICIT NONE

    Real(8), Pointer :: u_in(:), u_out(:), tmat(:,:), tinv(:,:), norm(:), lambda(:,:)
    Real(8) :: gamma, rho_in, ux_in, uy_in, uz_in, ke_in, p_in, en_in, h_in
    Real(8) :: gm1, rho_out, ux_out, uy_out, uz_out, ke_out, p_out, en_out, h_out
    Real(8) :: cc, bb, ux_Roe, uy_Roe, uz_Roe, h_Roe, ke_Roe, spd_snd_Roe, T_Roe, en_Roe
    Real(8) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, ucon, rho, alpha, theta, beta, phi2
    Real(8) :: XI_X, XI_Y, XI_Z, XI_T, denom, GAMREF, Up, Um

    Integer :: N, ND, jj, i, j, counter,ii
    Logical, dimension(ND+2) :: BWORK
    Integer, dimension(ND+2) :: order

    ! ... Lapack arguments
    CHARACTER(len=1) :: JOBVL, JOBVR
    INTEGER :: INFO, LDA, LDVL, LDVR, LWORK,M
    Real(8), dimension(ND+2,ND+2) :: A, VL, VR,B,LAM, Pmat
    Real(8), dimension(ND+2) :: WI, WR
    Real(8), dimension(300) :: WORK
    ! ... for right vector inversion
    integer, dimension(ND+2) :: IPIV

    gamma = gamref
    
    ! ... temporary
    XI_T = 0.0_8

    JOBVL = 'N'
    JOBVR = 'V'
    LDA = ND + 2
    LDVL = ND + 2
    LDVR = ND + 2
    M = ND+2

    LWORK = 300

    LAM(:,:) = 0.0_8
    A(:,:) = 0.0_8
    VL(:,:) = 0.0_8
    VR(:,:) = 0.0_8
    WR(:) = 0.0_8
    WI(:) = 0.0_8
    INFO = 0
    
    
    ! ... constants
    gm1 = gamma - 1.0_8

    ! ... form primative variables from "in"
    rho_in = u_in(1)
    ux_in  = u_in(2) / rho_in; uy_in = 0.0_8; uz_in = 0.0_8
    if (ND >= 2) uy_in  = u_in(3) / rho_in
    if (ND == 3) uz_in  = u_in(4) / rho_in
    ke_in  = 0.5_8 * (ux_in**2 + uy_in**2 + uz_in**2)
    en_in  = u_in(ND+2)/rho_in - ke_in
    p_in   = gm1 * rho_in * en_in
    h_in   = (gamma / gm1) * p_in / rho_in + ke_in 

    ! ... form primative variables from "out"
    rho_out = u_out(1)
    ux_out  = u_out(2) / rho_out; uy_out = 0.0_8; uz_out = 0.0_8
    if (ND >= 2) uy_out  = u_out(3) / rho_out
    if (ND == 3) uz_out  = u_out(4) / rho_out
    ke_out  = 0.5_8 * (ux_out**2 + uy_out**2 + uz_out**2)
    en_out  = u_out(ND+2)/rho_out - ke_out
    p_out   = gm1 * rho_out * en_out
    h_out   = (gamma / gm1) * p_out / rho_out + ke_out 

    ! ... form Roe variables
    cc = sqrt(rho_out / rho_in)
    bb = 1.0_8 / (1.0_8 + cc)
    ux_Roe = (Ux_in + Ux_out * cc) * bb
    uy_Roe = (Uy_in + Uy_out * cc) * bb
    uz_Roe = (Uz_in + Uz_out * cc) * bb
    h_Roe  = ( h_in +  h_out * cc) * bb
    ke_Roe = 0.5_8 * (ux_Roe**2 + uy_Roe**2 + uz_Roe**2)
    spd_snd_Roe = sqrt(gamma/GAMREF * gm1*(h_Roe - ke_Roe))

    ! ... temporarily
    T_Roe = gamma*(en_in + en_out * cc) * bb
    en_Roe = T_Roe/gamma
    
    ! ... variable renaming
    XI_X = norm(1); XI_Y = 0.0_8; XI_Z = 0.0_8
    if (ND >= 2) XI_Y = norm(2)
    if (ND == 3) XI_Z = norm(3)
    denom = 1.0_8 / sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
    XI_X_TILDE = XI_X * denom
    XI_Y_TILDE = XI_Y * denom
    XI_Z_TILDE = XI_Z * denom
    RHO = rho_in

    ! ... directly from Pulliam & Chaussee
    theta = XI_X * UX_ROE + XI_Y * UY_ROE + XI_Z * UZ_ROE
    phi2 = 0.5_8 * (GAMMA - 1.0_8) * (UX_ROE**2 + UY_ROE**2 + UZ_ROE**2)
    
    if (ND == 2) then

      A(1,1) = XI_T
      A(1,2) = XI_X
      A(1,3) = XI_Y
      A(1,4) = 0.0_8

      A(2,1) = XI_X * ( T_Roe / GAMREF * (GAMREF - 1.0_8) - (gm1 * en_Roe - phi2)) - theta * ux_Roe
      A(2,2) = XI_T + theta - XI_X * (GAMMA - 2.0_8) * ux_Roe
      A(2,3) = XI_Y * ux_Roe - gm1 * XI_X * uy_Roe
      A(2,4) = gm1 * XI_X

      A(3,1) = XI_Y * ( T_Roe / GAMREF * (GAMREF - 1.0_8) - (gm1 * en_Roe - phi2)) - theta * uy_Roe
      A(3,2) = XI_X * uy_Roe - gm1 * XI_Y * ux_Roe
      A(3,3) = XI_T + theta - XI_Y * (GAMMA - 2.0_8) * uy_Roe
      A(3,4) = gm1 * XI_Y

      A(4,1) = theta * (2.0_8 * phi2 - gamma * (en_Roe + ke_Roe)) 
      A(4,2) = XI_X * (T_Roe / GAMREF * (GAMREF - 1.0_8) + en_Roe + phi2 / gm1 ) - gm1 * theta * ux_Roe
      A(4,3) = XI_Y * (T_Roe / GAMREF * (GAMREF - 1.0_8) + en_Roe + phi2 / gm1 ) - gm1 * theta * uy_Roe
      A(4,4) = XI_T + gamma * theta

    else if (ND == 3) then

      A(1,1) = XI_T
      A(1,2) = XI_X
      A(1,3) = XI_Y
      A(1,4) = XI_Z
      A(1,5) = 0.0_8

      A(2,1) = XI_X * ( T_Roe / GAMREF * (GAMREF - 1.0_8) - (gm1 * en_Roe - phi2)) - theta * ux_Roe
      A(2,2) = XI_T + theta - XI_X * (GAMMA - 2.0_8) * ux_Roe
      A(2,3) = XI_Y * ux_Roe - gm1 * XI_X * uy_Roe
      A(2,4) = XI_Z * ux_Roe - gm1 * XI_X * uz_Roe
      A(2,5) = gm1 * XI_X

      A(3,1) = XI_Y * ( T_Roe / GAMREF * (GAMREF - 1.0_8) - (gm1 * en_Roe - phi2)) - theta * uy_Roe
      A(3,2) = XI_X * uy_Roe - gm1 * XI_Y * ux_Roe
      A(3,3) = XI_T + theta - XI_Y * (GAMMA - 2.0_8) * uy_Roe
      A(3,4) = XI_Z * uy_Roe - gm1 * XI_Y * uz_Roe
      A(3,5) = gm1 * XI_Y

      A(4,1) = XI_Z * ( T_Roe / GAMREF * (GAMREF - 1.0_8) - (gm1 * en_Roe - phi2)) - theta * uz_Roe
      A(4,2) = XI_X * uz_Roe - gm1 * XI_Z * ux_Roe
      A(4,3) = XI_Y * uz_Roe - gm1 * XI_Z * uy_Roe
      A(4,4) = XI_T + theta - XI_Z * (GAMMA - 2.0_8) * uz_Roe
      A(4,5) = gm1 * XI_Z

      A(5,1) = theta * (2.0_8 * phi2 - gamma * (en_Roe + ke_Roe)) 
      A(5,2) = XI_X * (T_Roe / GAMREF * (GAMREF - 1.0_8) + en_Roe + phi2 / gm1 ) - gm1 * theta * ux_Roe
      A(5,3) = XI_Y * (T_Roe / GAMREF * (GAMREF - 1.0_8) + en_Roe + phi2 / gm1 ) - gm1 * theta * uy_Roe
      A(5,4) = XI_Z * (T_Roe / GAMREF * (GAMREF - 1.0_8) + en_Roe + phi2 / gm1 ) - gm1 * theta * uz_Roe
      A(5,5) = XI_T + gamma * theta

    end if

    ! ... find the Eigenvalues and Eigenvectors
    ! ... Lapack driver DGEEV
!#ifdef HAVE_BLAS    
!    CALL DGEEV( JOBVL, JOBVR, M, A, LDA, WR, WI, VL, LDVL, VR, &
!         LDVR, WORK, LWORK, INFO )
!#endif

    lambda(:,:) = 0.0_8
    Pmat(:,:) = 0.0_8

    ! ... form transformation matrices
    ucon = ux_Roe * XI_X + uy_Roe * XI_Y + uz_Roe * XI_Z
    Up = ucon + 0.1_8 * SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
    Um = ucon - 0.1_8 * SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
    counter = 0

    ! ... Sort eigenvalues: {U,U,U,U+c,U-c}
    do jj = 1, ND+2

      if(WR(jj) .lt. Up .and. WR(jj) .gt. Um) then

        counter = counter + 1
        lambda(counter,counter) = WR(jj)
        order(jj) = counter
        Pmat(jj,counter) = 1.0_8
      elseif(WR(jj) .gt. ucon) then
        lambda(ND+1,ND+1) = WR(jj)
        order(jj) = ND+1
        Pmat(jj,ND+1) = 1.0_8
      elseif(WR(jj) .lt. ucon) then
        lambda(ND+2,ND+2) = WR(jj)
        order(jj) = ND+2
        Pmat(jj,ND+2) = 1.0_8
      end if
    end do

    Tmat = matmul(VR,Pmat)
    VR = Tmat

    ! ... invert VR
    ! ... using a pseudoinvert using singular value decomposition
    ! ... VR = U*D*V^T

    ! ... Args: A,A, size VR, size VR, VR, size VR, array of singular values WI, U, size U, V^T, size V, work, lwork, INFO
    JOBVL = 'A'
    WI(:) = 0.0_8
    VL(:,:) = 0.0_8
    A(:,:) = 0.0_8

    ! ... VR = VL*diag(WI)*A^T
!#ifdef HAVE_BLAS
!    call DGESVD(JOBVL,JOBVL,LDVR,LDVR,VR,LDVR,WI,VL,LDVR,A,LDVR,WORK,LWORK,INFO)
!#endif

    Tinv(:,:) = 0.0_8
    ! ... pseudo inverse is inv(VR) = inv(VL*diag(WI)*A^T) = A*inv(diag(WI))*VL^T where small (~0) values of WI -> 1/(WI) = 0
    do i = 1,ND+2
      if(WI(i) .gt. TINY(WI(i))) then
        Tinv(i,i) = 1.0_8/WI(i)
      else
        Tinv(i,i) = 0.0_8
      end if
    end do


    Tinv = MATMUL(TRANSPOSE(A),Tinv)
    Tinv = MATMUL(Tinv,TRANSPOSE(VL))

  end subroutine SAT_Form_Roe_Matrices_TP



  subroutine SAT_Form_Roe_Matrices(ND, u_in, u_out, gamma, norm, tmat, tinv, lambda)

!    USE ModGlobal

    IMPLICIT NONE

    Real(8) :: u_in(ND+2), u_out(ND+2), tmat(ND+2,ND+2), tinv(ND+2,ND+2), norm(ND), lambda(ND+2,ND+2)
    Real(8) :: gamma, rho_in, ux_in, uy_in, uz_in, ke_in, p_in, en_in, h_in
    Real(8) :: gm1, rho_out, ux_out, uy_out, uz_out, ke_out, p_out, en_out, h_out
    Real(8) :: cc, bb, ux_Roe, uy_Roe, uz_Roe, h_Roe, ke_Roe, spd_snd_Roe
    Real(8) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, ucon, rho, alpha, theta, beta, phi2
    Real(8) :: XI_X, XI_Y, XI_Z, denom
    Integer :: N, ND, jj

    ! ... size of this problem
    !ND = size(u_in) - 2

    ! ... constants
    gm1 = gamma - 1.0_8

    ! ... form primative variables from "in"
    rho_in = u_in(1)
    ux_in  = u_in(2) / rho_in; uy_in = 0.0_8; uz_in = 0.0_8
    if (ND >= 2) uy_in  = u_in(3) / rho_in
    if (ND == 3) uz_in  = u_in(4) / rho_in
    ke_in  = 0.5_8 * (ux_in**2 + uy_in**2 + uz_in**2)
    en_in  = u_in(ND+2)/rho_in - ke_in
    p_in   = gm1 * rho_in * en_in
    h_in   = (gamma / gm1) * p_in / rho_in + ke_in 

    ! ... form primative variables from "out"
    rho_out = u_out(1)
    ux_out  = u_out(2) / rho_out; uy_out = 0.0_8; uz_out = 0.0_8
    if (ND >= 2) uy_out  = u_out(3) / rho_out
    if (ND == 3) uz_out  = u_out(4) / rho_out
    ke_out  = 0.5_8 * (ux_out**2 + uy_out**2 + uz_out**2)
    en_out  = u_out(ND+2)/rho_out - ke_out
    p_out   = gm1 * rho_out * en_out
    h_out   = (gamma / gm1) * p_out / rho_out + ke_out 

    ! ... form Roe variables
    cc = sqrt(rho_out / rho_in)
    bb = 1.0_8 / (1.0_8 + cc)
    ux_Roe = (Ux_in + Ux_out * cc) * bb
    uy_Roe = (Uy_in + Uy_out * cc) * bb
    uz_Roe = (Uz_in + Uz_out * cc) * bb
    h_Roe  = ( h_in +  h_out * cc) * bb
    ke_Roe = 0.5_8 * (ux_Roe**2 + uy_Roe**2 + uz_Roe**2)
    if (h_Roe - ke_Roe <= 0.0_8) then
       !      write (*,'(6(E20.8,1X))') gamma, h_Roe, ke_Roe, ux_roe, uy_roe, uz_roe
       write (*,'(5(a,E20.8,1X))') "SATUTIL:FORM_ROE_MATRICES: ERRONEOUS CONDITIONS: gamma = ", gamma, ", ke_in = ", &
            ke_in, ", en_in = ", en_in, ", p_in = ", p_in, ", h_in = ", h_in
       write (*,*) 'u_in = ',u_in
       stop
    end if
    spd_snd_Roe = sqrt(gm1*(h_Roe - ke_Roe))

    ! ... variable renaming
    XI_X = norm(1); XI_Y = 0.0_8; XI_Z = 0.0_8
    if (ND >= 2) XI_Y = norm(2)
    if (ND == 3) XI_Z = norm(3)
    denom = 1.0_8 / sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
    XI_X_TILDE = XI_X * denom
    XI_Y_TILDE = XI_Y * denom
    XI_Z_TILDE = XI_Z * denom
    RHO = rho_in

!!$    ! ... debugging
!!$    RHO = rho_in
!!$    UX_ROE = UX_IN
!!$    UY_ROE = UY_IN
!!$    UZ_ROE = UZ_IN
!!$    SPD_SND_ROE = sqrt(gm1*(H_IN - KE_IN))

    ! ... form transformation matrices
    ucon = ux_Roe * XI_X + uy_Roe * XI_Y + uz_Roe * XI_Z

    if (ND == 2) then

      ! ... directly from Pulliam & Chaussee
      alpha = 0.5_8 * RHO / SPD_SND_ROE
      beta = 1.0_8 / (RHO * SPD_SND_ROE)
      theta = XI_X_TILDE * UX_ROE + XI_Y_TILDE * UY_ROE
      phi2 = 0.5_8 * (GAMMA - 1.0_8) * (UX_ROE**2 + UY_ROE**2)
      Tmat(1,1) = 1.0_8
      Tmat(1,2) = 0.0_8
      Tmat(1,3) = alpha
      Tmat(1,4) = alpha
      Tmat(2,1) = UX_ROE
      Tmat(2,2) = XI_Y_TILDE * RHO
      Tmat(2,3) = alpha * (UX_ROE + XI_X_TILDE * SPD_SND_ROE)
      Tmat(2,4) = alpha * (UX_ROE - XI_X_TILDE * SPD_SND_ROE)
      Tmat(3,1) = UY_ROE
      Tmat(3,2) = -XI_X_TILDE * RHO
      Tmat(3,3) = alpha * (UY_ROE + XI_Y_TILDE * SPD_SND_ROE)
      Tmat(3,4) = alpha * (UY_ROE - XI_Y_TILDE * SPD_SND_ROE)
      Tmat(4,1) = PHI2 / (GAMMA - 1.0_8)
      Tmat(4,2) = RHO * (XI_Y_TILDE * UX_ROE - XI_X_TILDE * UY_ROE)
      Tmat(4,3) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_8) + SPD_SND_ROE * THETA)
      Tmat(4,4) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_8) - SPD_SND_ROE * THETA)

      Tinv(1,1) = 1.0_8 - PHI2 / SPD_SND_ROE**2
      Tinv(1,2) = (GAMMA - 1.0_8) * UX_ROE / SPD_SND_ROE**2
      Tinv(1,3) = (GAMMA - 1.0_8) * UY_ROE / SPD_SND_ROE**2
      Tinv(1,4) = -(GAMMA - 1.0_8) / SPD_SND_ROE**2
      Tinv(2,1) = -(XI_Y_TILDE * UX_ROE - XI_X_TILDE * UY_ROE) / RHO
      Tinv(2,2) =  XI_Y_TILDE / RHO
      Tinv(2,3) = -XI_X_TILDE / RHO
      Tinv(2,4) = 0.0_8
      Tinv(3,1) = beta * (PHI2 - SPD_SND_ROE * theta)
      Tinv(3,2) = beta * (XI_X_TILDE * SPD_SND_ROE - (GAMMA - 1.0_8) * UX_ROE)
      Tinv(3,3) = beta * (XI_Y_TILDE * SPD_SND_ROE - (GAMMA - 1.0_8) * UY_ROE)
      Tinv(3,4) = beta * (GAMMA - 1.0_8)
      Tinv(4,1) = beta * (PHI2 + SPD_SND_ROE * theta)
      Tinv(4,2) = -beta * (XI_X_TILDE * SPD_SND_ROE + (GAMMA - 1.0_8) * UX_ROE)
      Tinv(4,3) = -beta * (XI_Y_TILDE * SPD_SND_ROE + (GAMMA - 1.0_8) * UY_ROE)
      Tinv(4,4) = beta * (GAMMA - 1.0_8)

      ! ... compute the diagonal matrix lambda
      Lambda(:,:) = 0.0_8
      do jj = 1, ND
        Lambda(jj,jj) = ucon
      end do
      Lambda(ND+1,ND+1) = ucon + SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2)
      Lambda(ND+2,ND+2) = ucon - SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2)

    else if (ND == 3) then

      ! ... directly from Pulliam & Chaussee
      alpha = 0.5_8 * RHO / SPD_SND_ROE
      beta = 1.0_8 / (RHO * SPD_SND_ROE)
      theta = XI_X_TILDE * UX_ROE + XI_Y_TILDE * UY_ROE + XI_Z_TILDE * UZ_ROE
      phi2 = 0.5_8 * (GAMMA - 1.0_8) * (UX_ROE**2 + UY_ROE**2 + UZ_ROE**2)

      Tmat(1,1) = XI_X_TILDE
      Tmat(1,2) = XI_Y_TILDE
      Tmat(1,3) = XI_Z_TILDE
      Tmat(1,4) = alpha
      Tmat(1,5) = alpha
      Tmat(2,1) = XI_X_TILDE * UX_ROE
      Tmat(2,2) = XI_Y_TILDE * UX_ROE - XI_Z_TILDE * RHO
      Tmat(2,3) = XI_Z_TILDE * UX_ROE + XI_Y_TILDE * RHO
      Tmat(2,4) = alpha * (UX_ROE + XI_X_TILDE * SPD_SND_ROE)
      Tmat(2,5) = alpha * (UX_ROE - XI_X_TILDE * SPD_SND_ROE)
      Tmat(3,1) = XI_X_TILDE * UY_ROE + XI_Z_TILDE * RHO
      Tmat(3,2) = XI_Y_TILDE * UY_ROE
      Tmat(3,3) = XI_Z_TILDE * UY_ROE - XI_X_TILDE * RHO
      Tmat(3,4) = alpha * (UY_ROE + XI_Y_TILDE * SPD_SND_ROE)
      Tmat(3,5) = alpha * (UY_ROE - XI_Y_TILDE * SPD_SND_ROE)
      Tmat(4,1) = XI_X_TILDE * UZ_ROE - XI_Y_TILDE * RHO
      Tmat(4,2) = XI_Y_TILDE * UZ_ROE + XI_X_TILDE * RHO
      Tmat(4,3) = XI_Z_TILDE * UZ_ROE
      Tmat(4,4) = alpha * (UZ_ROE + XI_Z_TILDE * SPD_SND_ROE)
      Tmat(4,5) = alpha * (UZ_ROE - XI_Z_TILDE * SPD_SND_ROE)
      Tmat(5,1) = XI_X_TILDE * PHI2 / (GAMMA - 1.0_8) + RHO * (XI_Z_TILDE * UY_ROE - XI_Y_TILDE * UZ_ROE)
      Tmat(5,2) = XI_Y_TILDE * PHI2 / (GAMMA - 1.0_8) + RHO * (XI_X_TILDE * UZ_ROE - XI_Z_TILDE * UX_ROE)
      Tmat(5,3) = XI_Z_TILDE * PHI2 / (GAMMA - 1.0_8) + RHO * (XI_Y_TILDE * UX_ROE - XI_X_TILDE * UY_ROE)
      Tmat(5,4) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_8) + SPD_SND_ROE * THETA)
      Tmat(5,5) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_8) - SPD_SND_ROE * THETA)

      Tinv(1,1) =  XI_X_TILDE * (1.0_8 - PHI2 / SPD_SND_ROE**2) - (XI_Z_TILDE * UY_ROE - XI_Y_TILDE * UZ_ROE) / RHO
      Tinv(1,2) =  XI_X_TILDE * (GAMMA - 1.0_8) * UX_ROE / SPD_SND_ROE**2
      Tinv(1,3) =  XI_Z_TILDE / RHO + XI_X_TILDE * (GAMMA - 1.0_8) * UY_ROE / SPD_SND_ROE**2
      Tinv(1,4) = -XI_Y_TILDE / RHO + XI_X_TILDE * (GAMMA - 1.0_8) * UZ_ROE / SPD_SND_ROE**2
      Tinv(1,5) = -XI_X_TILDE * (GAMMA - 1.0_8) / SPD_SND_ROE**2
      Tinv(2,1) =  XI_Y_TILDE * (1.0_8 - PHI2 / SPD_SND_ROE**2) - (XI_X_TILDE * UZ_ROE - XI_Z_TILDE * UX_ROE) / RHO
      Tinv(2,2) = -XI_Z_TILDE / RHO + XI_Y_TILDE * (GAMMA - 1.0_8) * UX_ROE / SPD_SND_ROE**2
      Tinv(2,3) =  XI_Y_TILDE * (GAMMA - 1.0_8) * UY_ROE / SPD_SND_ROE**2
      Tinv(2,4) =  XI_X_TILDE / RHO + XI_Y_TILDE * (GAMMA - 1.0_8) * UZ_ROE / SPD_SND_ROE**2
      Tinv(2,5) = -XI_Y_TILDE * (GAMMA - 1.0_8) / SPD_SND_ROE**2
      Tinv(3,1) =  XI_Z_TILDE * (1.0_8 - PHI2 / SPD_SND_ROE**2) - (XI_Y_TILDE * UX_ROE - XI_X_TILDE * UY_ROE) / RHO
      Tinv(3,2) =  XI_Y_TILDE / RHO + XI_Z_TILDE * (GAMMA - 1.0_8) * UX_ROE / SPD_SND_ROE**2
      Tinv(3,3) = -XI_X_TILDE / RHO + XI_Z_TILDE * (GAMMA - 1.0_8) * UY_ROE / SPD_SND_ROE**2
      Tinv(3,4) =  XI_Z_TILDE * (GAMMA - 1.0_8) * UZ_ROE / SPD_SND_ROE**2
      Tinv(3,5) = -XI_Z_TILDE * (GAMMA - 1.0_8) / SPD_SND_ROE**2
      Tinv(4,1) =  beta * (PHI2 - SPD_SND_ROE * theta)
      Tinv(4,2) =  beta * (XI_X_TILDE * SPD_SND_ROE - (GAMMA - 1.0_8) * UX_ROE)
      Tinv(4,3) =  beta * (XI_Y_TILDE * SPD_SND_ROE - (GAMMA - 1.0_8) * UY_ROE)
      Tinv(4,4) =  beta * (XI_Z_TILDE * SPD_SND_ROE - (GAMMA - 1.0_8) * UZ_ROE)
      Tinv(4,5) =  beta * (GAMMA - 1.0_8)
      Tinv(5,1) =  beta * (PHI2 + SPD_SND_ROE * theta)
      Tinv(5,2) = -beta * (XI_X_TILDE * SPD_SND_ROE + (GAMMA - 1.0_8) * UX_ROE)
      Tinv(5,3) = -beta * (XI_Y_TILDE * SPD_SND_ROE + (GAMMA - 1.0_8) * UY_ROE)
      Tinv(5,4) = -beta * (XI_Z_TILDE * SPD_SND_ROE + (GAMMA - 1.0_8) * UZ_ROE)
      Tinv(5,5) =  beta * (GAMMA - 1.0_8)

      ! ... compute the diagonal matrix lambda
      Lambda(:,:) = 0.0_8
      do jj = 1, ND
        Lambda(jj,jj) = ucon
      end do
      Lambda(ND+1,ND+1) = ucon + SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
      Lambda(ND+2,ND+2) = ucon - SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)

    end if

  end subroutine SAT_Form_Roe_Matrices

  SUBROUTINE DISSIPATIONWEIGHT(numDim,bufferSize,numPoints,bufferInterval,   &
       sigmaDissipation,sigmaDilatation,dilatationRamp,dilatationCutoff,     &
       divV,sigmaDiss)
    
    IMPLICIT NONE

    INTEGER(KIND=4), INTENT(IN)  :: numDim
    INTEGER(KIND=8), INTENT(IN)  :: bufferSize(numDim),  numPoints
    INTEGER(KIND=8), INTENT(IN)  :: bufferInterval(2*numDim) 
    REAL(KIND=8),    INTENT(IN)  :: sigmaDissipation,sigmaDilatation
    REAL(KIND=8),    INTENT(IN)  :: dilatationRamp,dilatationCutoff
    REAL(KIND=8),    INTENT(IN)  :: divV(numPoints)
    REAL(KIND=8),    INTENT(OUT) :: sigmaDiss(numPoints)
    
    INTEGER(KIND=8) :: I, J, K
    INTEGER(KIND=8) :: nPlane, zIndex, yIndex, yzIndex, bufferIndex, xSize
    INTEGER(KIND=8) :: iStart,iEnd,jStart,jEnd,kStart,kEnd

    iStart = bufferInterval(1)
    iEnd   = bufferInterval(2)
    xSize  = bufferSize(1)

    IF(numDim == 1) THEN
       DO I = iStart, iEnd
          sigmaDiss(I) = (sigmaDissipation + &
               sigmaDilatation*0.5_8*(1.0_8 + TANH(dilatationRamp*(dilatationCutoff-divV(I)))))  
       END DO
    ELSE IF(numDim == 2) THEN
       jStart = bufferInterval(3)
       jEnd   = bufferInterval(4)
       DO J = jStart, jEnd
          yIndex = (J-1)*xSize
          DO I = iStart, iEnd
             bufferIndex = yIndex + I
             sigmaDiss(bufferIndex) = (sigmaDissipation + &
                  sigmaDilatation*0.5_8*(1.0_8 + TANH(dilatationRamp*(dilatationCutoff-divV(bufferIndex)))))  
          END DO
       END DO
    ELSE IF(numDim == 3) THEN
       jStart = bufferInterval(3)
       jEnd   = bufferInterval(4)
       kStart = bufferInterval(5)
       kEnd   = bufferInterval(6)
       nPlane = xSize * bufferSize(2)
       DO K = kStart, kEnd
          zIndex = (K-1)*nPlane
          DO J = jStart, jEnd
             yzIndex = zIndex + (J-1)*xSize
             DO I = iStart, iEnd
                bufferIndex = yzIndex + I
                sigmaDiss(bufferIndex) = (sigmaDissipation + &
                     sigmaDilatation*0.5_8*(1.0_8 + TANH(dilatationRamp*(dilatationCutoff-divV(bufferIndex)))))
             END DO
          END DO
       END DO
    ENDIF
       
       
  END SUBROUTINE DISSIPATIONWEIGHT

END MODULE SATUtil
