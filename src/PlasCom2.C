//
// Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
// License: MIT, http://opensource.org/licenses/MIT
//
#include "PlasCom2.H"
#include "BareBonesKernels.H"


namespace plascom2 {

  int application::RunApplication()
  {
    FunctionEntry("RunApplication");
    int runCode = 0;
    std::string runMode(appConfig.GetValue<std::string>("PlasCom2:RunMode"));
    globalCommunicator.Barrier();
    std::ostringstream localMessages;
    localMessages << "RunMode = '" << runMode << "'" << std::endl;
    StdOut(localMessages.str());
    pcpp::io::RenewStream(localMessages);
    if(runMode.empty())
      runMode = "BareBones";
    if(runMode == "BareBones"){
      runCode = 1;
      StdOut("Doing barebones.\n");
      FC_GLOBAL(barebones,BAREBONES)(&runCode);
    } else if (runMode == "PlasCom2") {
      runCode = Run();
      //     } else if(runMode == "EulerPBSPeriodic"){
      //       StdOut("Doing Euler.\n");
      //       runCode = RunEulerPBSPeriodic();
    } else if(runMode == "PC2"){
      StdOut("Doing PC2.\n");
      runCode = RunPlasCom2();
    } else if (runMode == "MaxwellSolver") {
      StdOut("Doing Maxwell Solver.\n");
      //      runCode = RunMaxwellSolver();
    } else {
      StdOut("Unknown run option.\n");
      runCode = 1;
    }
    FunctionExit("RunApplication");
    return(runCode);
  }

}
