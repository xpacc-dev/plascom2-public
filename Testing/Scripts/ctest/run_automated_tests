#!/bin/tcsh
# Usage:
#
# run_automated_tests <project file> <mode> <rundir>
#
# where <project file> indicates the project file.
# The project file is processed line-by-line.  Each
# line is expected to have the following information
# and format:
#
# ProjectName:BranchName:BranchPath
#
# The "ProjectName" is the name of the project to
# be built/tested.  The "BranchName" is an arbitrary
# name to describe the branch, and "BranchPath" is the 
# actual path to the branch in the project base repository.
# 
#

# ===== Initialization and setup =========
set mode="$2"
set project_file="$1"
set rundir="$3"
set specific_test="$4"
set mydir=${rundir}
set HOSTNAME=`hostname -s`
cd ${mydir}

# Use a different shell or shell config if you want
#source ${HOME}/.tcshrc
use cmake
foreach user_configuration_file (`ls user_config/*`)
    source ${user_configuration_file}
end

# ========================================

# === Build/Test Environment Settings ====
setenv SVNCOMMAND `which svn`
setenv GITCOMMAND `which git`
setenv CTESTCOMMAND `which ctest`
setenv CMAKECOMMAND `which cmake`
# The following two lines are optional.  Rocpack requires this
# setting for CMAKE_PREFIX_PATH. Your project may require
# different paths, or a whole set of options - which can be
# set as the PROJECT_CONFIGURATION_OPTIONS environment variable.
setenv PROJECT_CONFIGURATION_OPTIONS 

setenv MODULEPATHSAV $MODULEPATH
setenv MODULEPATH "${mydir}/modules:$MODULEPATH"
#
#  add a variable to enable building documentation
#  eventually read this from input
#
#setenv BUILD_DOCUMENTATION "ON"
#setenv PROJECT_CONFIGURATION_OPTIONS "-DBUILD_DOCUMENTATION=ON"

# ========================================

# == Loop over projects and invoke ctest =
foreach project (`cat ${project_file} | grep -v "#"`)
  set PROJECT_NAME=`printf "${project}" | cut -d "|" -f 1`
  set BRANCH_NAME=`printf "${project}" | cut -d "|" -f 2`
  set BRANCH_NAME_SAFE=`printf "${project}" | cut -d "|" -f 2 | sed 's/\//_/g'`
  set BRANCH_PATH=`printf "${project}" | cut -d "|" -f 3`
  if( ("${specific_test}" == "${PROJECT_NAME}") || ("${specific_test}" == "") ) then
    set REPO_TYPE=`printf "${project}" | cut -d "|" -f 4`
    setenv REPO_TYPE ${REPO_TYPE}
    set CONFIG_TYPE=`printf "${project}" | cut -d "|" -f 5`
    setenv PROJECT_CONFIGURATION_TYPE ${CONFIG_TYPE}
    setenv DOC_TARGET `printf "${project}" | cut -d "|" -f 6`

    printf "***************************************************\n"
    printf "Processing ${PROJECT_NAME} ${BRANCH_NAME} on ${HOSTNAME}...\n"
    printf "***************************************************\n"
    printf "Mode: ${mode}\n"
    printf "Top level automated build directory: ${mydir}\n"
    printf "Safe branch name: ${BRANCH_NAME_SAFE}\n"

    setenv PROJECT_SOURCE "${mydir}/${PROJECT_NAME}_${BRANCH_NAME_SAFE}"
    setenv PROJECT_ROOT ${BRANCH_PATH}
    setenv PROJECT_BUILD_DIRECTORY "${mydir}/${PROJECT_NAME}-${HOSTNAME}-${BRANCH_NAME_SAFE}-${mode}"
    setenv BRANCH ${BRANCH_NAME}

    printf "PROJECT_SOURCE = ${PROJECT_SOURCE}\n"
    printf "PROJECT_ROOT = ${PROJECT_ROOT}\n"
    printf "PROJECT_BUILD_DIRECTORY = ${PROJECT_BUILD_DIRECTORY}\n"
    printf "BRANCH = ${BRANCH}\n"

    rm -f ${mydir}/${PROJECT_NAME}_${BRANCH_NAME_SAFE}_${mode}_log.txt

    # load project specific environment module
    if (-e modules/${PROJECT_NAME}) then
      printf "Loading module ${PROJECT_NAME}\n"
      module load ${PROJECT_NAME}
    endif

    # source any project-specific configuration
    if(-e configuration/${PROJECT_NAME}) then
      printf "Setting ${PROJECT_NAME}-specific configuration.\n"
      source configuration/${PROJECT_NAME}
    endif

    # look for the project in the documentation directory, this indicates we should
    # build the documentation
    if ( (-e documentation/${PROJECT_NAME}) && (${DOC_TARGET} != "none") ) then
      setenv PROJECT_CONFIGURATION_OPTIONS "-DBUILD_DOCUMENTATION=ON ${PROJECT_CONFIGURATION_OPTIONS}" 
      printf "Enabling documentation build for ${PROJECT_NAME} \n"
      setenv BUILD_DOCS TRUE
    endif

    printf "Running ctest for ${PROJECT_NAME} \n"
    ctest -S ./automated_test_script.cmake,${mode} -V >& ${PROJECT_NAME}_${BRANCH_NAME_SAFE}_${mode}_log.txt 
    printf "Done running ctest for ${PROJECT_NAME} \n"

    unsetenv BUILD_DOCS

    # we have built the documentation, now do something with it
    # the loop allows us to specify multiple destinations for the same project
    if (-e documentation/${PROJECT_NAME}) then
      foreach document (`cat documentation/${PROJECT_NAME} | grep -v "#"`)
        set DOC_DEST=`printf "${document}" | cut -d "|" -f 1`
        set DOC_PATH=`printf "${document}" | cut -d "|" -f 2`
        set DOC_SRC=`printf "${document}" | cut -d "|" -f 3`

        if( "${DOC_DEST}" == "local" ) then
          if (! -e ${DOC_PATH}) then
            mkdir -p ${DOC_PATH}
          endif

          # the documentation can be a lot of files, exceeding the limit of glob
          # instead of a straight copy we do things one file at a time
          # still do a recursive copy of the directories, assuming they do not 
          # contain massive amounts of files
          set files = `ls ${PROJECT_BUILD_DIRECTORY}/${DOC_SRC}`
          foreach file (`echo ${files}`)
            # this is recursive in case the file is a directory
            cp -R ${PROJECT_BUILD_DIRECTORY}/${DOC_SRC}/$file ${DOC_PATH}/.
          end
        else if ( "${DOC_DEST}" == "git" ) then
          printf "Updating git documentation for ${PROJECT_NAME}\n"
          if (-e ${PROJECT_NAME}_${DOC_PATH}) then
            rm -Rf ${PROJECT_NAME}_${DOC_PATH}
          endif
          ${GITCOMMAND} clone -b ${DOC_PATH} --recursive ${PROJECT_ROOT} ${PROJECT_NAME}_${DOC_PATH}
          cd ${PROJECT_NAME}_${DOC_PATH}

          set files = `ls`
          foreach file (`echo ${files}`)
            # this is recursive in case the file is a directory
            rm -R $file
          end

          set files = `ls ${PROJECT_BUILD_DIRECTORY}/${DOC_SRC}`
          foreach file (`echo ${files}`)
            # this is recursive in case the file is a directory
            cp -R ${PROJECT_BUILD_DIRECTORY}/${DOC_SRC}/$file .
          ${GITCOMMAND} add $file
          end

          ${GITCOMMAND} commit -a -m "Automated documentation update" 
          ${GITCOMMAND} push origin ${DOC_PATH}
          cd ..
        else
          printf "Unknown document destination ${DOC_DEST} for project ${PROJECT_NAME}\n"
        endif
      end
    endif

    # unload project specific environment module
    if (-e modules/${PROJECT_NAME}) then
      module unload ${PROJECT_NAME}
    endif
  endif
end

#reset environment
setenv MODULEPATH $MODULEPATHSAV
# ========================================

