!
! Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!program ImpingingJetAxisym

  use ModExamplesCommon
  use ModBC
  use ModParseArguments
  use ModStdIO
  use ModPLOT3D_IO
  implicit none

  integer(ik) :: i, j, k
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments
  type(t_cmd_opt), dimension(12) :: Options
  integer(ik) :: Nx, Ny
  integer(ik) :: OutputFormat
  real(rk) :: U, V
  real(rk),dimension(:,:), allocatable :: xfac, yfac
  integer(ik) :: i1, j1, k1, i2, j2, k2, IBcheck
  real(rk) :: rhoinv
  character(120) :: ExecPath,GridPath,SolPath
  character :: cmd*100
  integer(ik), dimension(2) :: nPoints
  real(rk), dimension(2) :: Length
  real(rk), dimension(:,:,:), allocatable :: X
  real(rk), dimension(:,:,:,:,:), pointer :: XTemp, QTemp, QauxTemp
  integer(ik), dimension(:,:), allocatable :: IB
  integer(ik), dimension(:,:,:,:), pointer :: SSIB
  real(rk), dimension(:,:,:), allocatable :: Q, Qaux,massFractions
  type(t_bc), dimension(:),allocatable :: BCs
  real(rk), parameter :: Gamma = 1.4_rk
  real (rk), parameter :: GasConstant = 8314.4621_rk !J/(K kmol)
  real(rk) :: YO2, YH2, YH, YH2O, YHO2, nitrogenOxygenMoleRatio
  real(rk) :: MwRef, Mwa 
  real(rk) :: MachFactor,GL_csq
  real(rk) :: xtilde, ytilde!, Vtemp
  real(rk) :: Density, DensInv, Lref
  real(rk) :: Ly,Lx
  real(rk), dimension(2) :: Velocity
  integer(ik), dimension(2) :: nSponge
  real(rk), dimension(:), allocatable :: Mwcoeff, MwcoeffInv
  Integer (ik) :: VolRateSpecified = 0
  real(rk)::exp_x,exp_y
  integer(ik)::useDimensional 

  real(rk), dimension(:), allocatable :: thermo_mwts
  real(rk), dimension(:), allocatable :: thermo_h0,thermo_cp0,thermo_Y
  real(rk) :: thermo_rho, thermo_p, thermo_e, thermo_h, thermo_T, thermo_rhoe
  real(rk) :: densRef, Rair 

  Integer (ik), parameter :: nspecies=8
  Integer (ik), parameter :: iH2 = 1
  Integer (ik), parameter :: iO2 = 2
  Integer (ik), parameter :: iH = 3
  Integer (ik), parameter :: iH2O = 4
  Integer (ik), parameter :: iHO2 = 5
  Integer (ik), parameter :: iH2O2 = 6
  Integer (ik), parameter :: iO = 7
  Integer (ik), parameter :: iOH = 8
  Integer (ik) :: iN2 =9 
  Integer (ik), parameter :: GridStretch = 1
  Integer (ik), parameter :: includeElectrode = 1

  integer(ik)::iplug,jplug
  integer(ik)::jSlottedJetStart,jSlottedJetEnd
  real(rk)::AmbientPressure,AmbientDensity
  real(rk),dimension(:,:),allocatable::P ,T
  integer(ik)::jSpongeInlet
  integer(ik)::jm
  real(rk)::ystartJet,yendJet,ymid,y
  integer(ik),parameter::Nrefine=1
  real(rk),parameter::ReferenceLength=1.0
  real(rk)::xplug,xpos
  integer(ik),parameter::depthSlottedJet=20
  real(rk)::Mjet
  real(rk):: CPR 
  real(rk):: CDR
  real(rk):: Po
  real(rk):: rhoo
  real(rk):: Pjet
  real(rk):: rhojet

  real(rk)::plugRadius
  real(rk)::slotWidth
  real(rk)::dx,dy 
  real(rk)::machCrossStream

  real(rk)::Tref,rhoref,cref,rhoYO2Ref,rhoH2ref,DensityRatio
  real(rk)::Lp,dummyVal
  character(len=1000)::filename,dummyline
  integer::s,nskip
  logical::AIR_INTO_AIR, HYDROGEN_INTO_AIR
  integer::jetStart
  real(rk)::uvel,vvel

  real(rk)::tmp,tmp2,pressure,temperature
  real(rk)::MwcoeffRho

  real(rk)::densityLIBREF,temperatureLIBREF,velocityLIBREF
  real(rk)::densityLIB,vvelLIB,uvelLIB,temperatureLIB,YOLIB,YO2LIB,YO2LIBREF

  integer :: SSptsX, SSptsY, SSNdim, SSngrid, SSftype, SSNvar
  Character(LEN=2) :: SSprec, SSgf, SSvf, SSiblocal
  real(rk) :: SStau(4)
  integer,pointer,dimension(:,:) :: SSND
  integer,allocatable,dimension(:,:) :: ND

  real(rk)::unstartLoc

  real(rk):: MachNumberPreShock
  real(rk):: DensityPreShock
  real(rk):: TemperaturePreShock
  real(rk):: SoundSpeedPreShock

  real(rk):: MachNumberPostShock
  real(rk):: DensityPostShock
  real(rk):: TemperaturePostShock
  real(rk):: SoundSpeedPostShock

  real(rk)::VerticalOffSetFromJet
  real(rk)::HorizontalOffSetFromCenterline
  integer::jSlotJetCenter,iPlaceLIB,jPlaceLIB

  !inputs from Gyu Sub [labeled GS] 
  real(rk),parameter::airPoGS=167.1_rk*1.e3 !kPa
  real(rk),parameter::airToGs=298.0_rk !Kelvin
  real(rk),parameter::fuelPoGS=386.6_rk*1.e3 !kPa
  real(rk),parameter::fuelToGS=298.0_rk !Kelvin
  real(rk),parameter::airMdotGS=56.38_rk !g/s
  real(rk),parameter::fuelMdotGS=1.842_rk !g/s 
  real(rk)::airRoGS,fuelRoGs,airCoGS,fuelCoGs !derived below from Gyu Sub data
  real(rk),parameter::testSectionMachNumber=2.7_rk 
  real(rk)::testSectionDensity,testSectionPressure,testSectionTemperature,testSectionSoundSpeed,testSectionVelocity
  real(rk),parameter::fuelMachNumber=1.0_rk
  real(rk)::fuelDensity,fuelPressure,fuelTemperature,fuelSoundSpeed,fuelVelocity

  !to nondimensionalize the pressure and temperature
  real(rk),parameter::nondimT=298.*(gamma-1.0)
  real(rk),parameter::nondimP=101.325 !kPa
  real(rk),parameter::nondimR=1.2041 !kg/m^3 !what was in the input deck
  real(rk),parameter::nondimVel=343. !m/s

  airRoGS = airPoGS  / airToGs / 259.8 !indiviaul gas constant for oxygen [259.8]
  fuelRoGS = fuelPoGS  / fuelToGS / 4124. !individual gas constant for hydrogen [4124]
  airCoGS = sqrt(gamma * airPoGs  / airRoGS)
  fuelCoGs = sqrt(gamma * fuelPoGS  / fuelRoGS)

  print *,"**************************************************************"
  print *,"Print the Gyu Sub inputs"
  print *,"airPo [Pa] = ",airPoGS
  print *,"airTo [K] = ",airToGS
  print *,"airRo [kg/m^3] (derived) = ",airRoGS
  print *,"fuelPo [Pa] = ",fuelPoGS
  print *,"fuelTo [K] = ",fuelToGS
  print *,"fuelRo [kg/m^3] (derived) = ",fuelRoGS
  print *,"**************************************************************"

  testSectionDensity=get_isentropic_density(testSectionMachNumber,airRoGS,gamma)
  testSectionPressure=get_isentropic_pressure(testSectionMachNumber,airPoGS,gamma) 
  testSectionTemperature=get_isentropic_temperature(testSectionMachNumber,airToGs,gamma)
  testSectionSoundSpeed=get_isentropic_soundSpeed(testSectionMachNumber,airCoGS,gamma)
  testSectionVelocity=testSectionMachNumber*testSectionSoundSpeed

  print *,"**************************************************************"
  print *,"Print isentropic values in test section (M=2.7)"
  print *,"testSectionP [Pa] (derived) = ",testSectionPressure
  print *,"testSectionT [K] (derived) = ",testSectionTemperature
  print *,"testSectionR [kg/m^3] (derived) = ",testSectionDensity
  print *,"testSectionVel [m/s] (derived) = ",testSectionVelocity
  print *,"Recheck Mach = ",testSectionVelocity/testSectionSoundSpeed 
  print *,"**************************************************************"

  fuelDensity=get_isentropic_density(fuelMachNumber,fuelRoGS,gamma)
  fuelPressure=get_isentropic_pressure(fuelMachNumber,fuelPoGS,gamma)
  fuelTemperature=get_isentropic_temperature(fuelMachNumber,fuelToGs,gamma)
  fuelSoundSpeed=get_isentropic_soundSpeed(fuelMachNumber,fuelCoGS,gamma)
  fuelVelocity=fuelMachNumber*fuelSoundSpeed

  print *,"**************************************************************"
  print *,"Print isentropic values exiting fuel (M=1.0)"
  print *,"fuelP [Pa] (derived) = ",fuelPressure
  print *,"fuelT [K] (derived) = ",fuelTemperature
  print *,"fuelR [kg/m^3] (derived) = ",fuelDensity
  print *,"fuelVel [m/s] (derived) = ",fuelVelocity
  print *,"Recheck Mach = ",fuelVelocity/fuelSoundSpeed
  print *,"**************************************************************"

  print *,"**************************************************************"
  print *,"Momentum Ratio of Test Section to Jet"
  print*,"J=",fuelDensity*fuelVelocity**2/testSectionDensity/testSectionVelocity**2
  print *,"**************************************************************"

  print *,"**************************************************************"
  print *,"Stagnation jet pressure to ambient test section pressure"
  print*,"PR=",fuelPoGS/testSectionPressure
  print *,"**************************************************************"

  print *,"**************************************************************"
  print *,"jet pressure to ambient test section pressure"
  print*,"PR_jet=",fuelPressure/testSectionPressure
  print *,"**************************************************************"

  print *,"**************************************************************"
  print *,"Reynolds number of the test section"
  print*,"Re=0.008 * testSectionVel * testSectionRho/9.0865272E-6=",0.008*testSectionVelocity*testSectionDensity/9.0865272E-6
  print *,"**************************************************************"

  print *,"**************************************************************"
  print *,"Reynolds number of the jet exit"
  print*,"Re=0.000195 * fueVel * fuelRho/7.7724307E-6=",0.000195*fuelVelocity*fuelDensity/7.7724307E-6
  print *,"**************************************************************"

  !9.0865272E-6
  
  !get location of executable
  call get_command_argument(0, cmd)
  Execpath(1:len_trim(cmd))=cmd(1:len_trim(cmd))
  !get command line arguments
  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  Options(1) = t_cmd_opt_("format", "f", OPT_VALUE_TYPE_STRING) !HDF5 or PLOT3D I/O
  Options(2) = t_cmd_opt_("nptsx", "i", OPT_VALUE_TYPE_INTEGER) !number of points in x direction
  Options(3) = t_cmd_opt_("nptsy", "j", OPT_VALUE_TYPE_INTEGER) !number of points in x direction
  Options(4) = t_cmd_opt_("ly", "H", OPT_VALUE_TYPE_REAL) !height of domain
  Options(5) = t_cmd_opt_("lx", "W", OPT_VALUE_TYPE_REAL) !width of domain
  Options(6) = t_cmd_opt_("plug radius ", "r", OPT_VALUE_TYPE_REAL) !plug radius
  Options(7) = t_cmd_opt_("slot width ", "w", OPT_VALUE_TYPE_REAL) !slotted jet width
  Options(8) = t_cmd_opt_("cross stream Mach", "M", OPT_VALUE_TYPE_REAL) !slotted jet width
  Options(9) = t_cmd_opt_("dimensional or nondimensional", "D", OPT_VALUE_TYPE_INTEGER)!slotted jet width 
  Options(10) = t_cmd_opt_("pressure ratio", "p",OPT_VALUE_TYPE_REAL)!slotted jet width
  Options(11) = t_cmd_opt_("x", "x",OPT_VALUE_TYPE_REAL)!slottedjet width
  Options(12) = t_cmd_opt_("y", "y",OPT_VALUE_TYPE_REAL)!slottedjet width


  call ParseArguments(RawArguments, Options=Options)
  
  !I/O Stuff
  if (Options(1)%is_present) then
    if (Options(1)%value == "plot3d") then
      OutputFormat = OUTPUT_FORMAT_PLOT3D
    else if (Options(1)%value == "hdf5") then
      OutputFormat = OUTPUT_FORMAT_HDF5
    else
      write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized grid format '", trim(Options(1)%value), "'."
      stop 1
    end if
  else
    OutputFormat = DefaultOutputFormat
  end if

  if (Options(2)%is_present) then
    read (Options(2)%value, *) Nx
  else
    Nx = 500*Nrefine 
  end if

  if (Options(3)%is_present) then
    read (Options(3)%value, *) Ny
  else
    Ny = 1000*Nrefine 
  end if

  if (Options(4)%is_present) then
    read (Options(4)%value, *) Ly
  else
    Ly = 56.0_rk*ReferenceLength 
  end if

  if (Options(5)%is_present) then
    read (Options(5)%value, *) Lx
  else
    Lx = 14.0_rk*ReferenceLength  
  end if
  
  if (Options(6)%is_present) then
    read (Options(6)%value, *) plugRadius
  else
    plugRadius = 4.0_rk*ReferenceLength
  end if

  if (Options(7)%is_present) then
    read (Options(7)%value, *) slotWidth
  else
    slotWidth = 5.0_rk*ReferenceLength
  end if

  if (Options(8)%is_present) then
    read (Options(8)%value, *) machCrossStream
  else
    machCrossStream = 0._rk
  end if

  if (Options(10)%is_present) then
    read (Options(10)%value, *) DensityRatio
  else
    DensityRatio=1.
  end if

!LIB POSITIONS RELATIVE TO THE JET
if (Options(11)%is_present) then
    read (Options(11)%value, *) VerticalOffSetFromJet
  else
    VerticalOffSetFromJet=-4.00
  end if

if (Options(12)%is_present) then
    read (Options(12)%value, *) HorizontalOffSetFromCenterline
  else
    HorizontalOffSetFromCenterline=7.43
  end if

  !grid creation
  nPoints = [Nx, Ny]
  Length = [Lx, Ly]

  dx=Length(1)/real(nPoints(1)-1)
  dy=Length(2)/real(nPoints(2)-1)

  allocate(X(2,nPoints(1),nPoints(2)))
  allocate(IB(nPoints(1),nPoints(2)))
  IB(:,:) = 1

  !simple uniform mesh stretching
  do j = 1, nPoints(2)
    do i = 1, nPoints(1)
      xtilde = real(i-1, kind=rk)/real(nPoints(1)-1, kind=rk)
      ytilde = real(j-1, kind=rk)/real(nPoints(2)-1, kind=rk)
      U = xtilde 
      V = ytilde
      X(:,i,j) = (Length * [U,V])- [0.0_rk,0.0_rk]
    end do
  end do

  !allocate Q vars
  allocate(Q(4,nPoints(1),nPoints(2)))
  allocate(Qaux(nspecies,nPoints(1),nPoints(2)))
  allocate(massFractions(nspecies,nPoints(1),nPoints(2)))
  allocate(P(nPoints(1),nPoints(2)))
  allocate(T(npoints(1),npoints(2)))

  !plug (i.e. the 'sting') radius is 4 mm
  !start the jet exit at 5\times 4 mm
  jSlottedJetStart=int(5.0*plugRadius/dy)
  jSlottedJetEnd=jSlottedJetStart+int(slotWidth/dy)
  print *,"Number of points across slot=",abs(jSlottedJetStart-jSlottedJetEnd)

  iplug=int(plugRadius/dx)
  ystartJet=X(2,iplug,jSlottedJetStart)
  yendJet=X(2,iplug,jSlottedJetEnd)

  !iblank the plug
  do j = 1, nPoints(2)
  do i = 1, iplug-1
  IB(i,j)=0.
  end do
  end do
 
  jetStart=7*iplug/8
  do j = jSlottedJetStart,jSlottedJetEnd
  do i = jetStart, iplug
  IB(i,j)=1.
  end do
  end do

  !copied directly from jet in cross flow initialization per model0 specification
  iN2 = Nspecies + 1
  allocate(Mwcoeff(nspecies+1), MwcoeffInv(nspecies+1))
  Mwcoeff(iH2) = 2.0;
  Mwcoeff(iO2) = 32.0;
  if  (nspecies>=3) Mwcoeff(iH) = 1.0_rk
  if  (nspecies>=4) Mwcoeff(iH2O) = 18.0_rk
  if  (nspecies>=5) Mwcoeff(iHO2) = 33.0_rk
  if  (nspecies>=6) Mwcoeff(iH2O2) = 34.0_rk
  if  (nspecies>=7) Mwcoeff(iO) = 16.0_rk
  if  (nspecies>=8) Mwcoeff(iOH) = 17.0_rk
  Mwcoeff(iN2) = 28.0_rk
  nitrogenOxygenMoleRatio = 3.76_rk
  YO2 = 1.0_rk/(1.0_rk+7.0_rk/8.0_rk*nitrogenOxygenMoleRatio);
  Mwa =  (Mwcoeff(iO2) +  Mwcoeff(iN2)*nitrogenOxygenMoleRatio)/(1.0_rk+nitrogenOxygenMoleRatio)
  Rair = GasConstant / Mwa
  Cref = sqrt(gamma*Rair*Tref)
  Cref = Cref / MachFactor
  do i=1,nspecies+1
    Mwcoeff(i) = 1.0_rk / Mwcoeff(i)
  enddo
  MwRef = YO2*Mwcoeff(iO2) + (1.0_rk-YO2)*Mwcoeff(iN2)
  MwcoeffRho=1.0_rk/Mwcoeff(iN2)/MwRef
  Mwcoeff = Mwcoeff/MwRef
  do i=1,nspecies
    Mwcoeff(i) = Mwcoeff(i)-Mwcoeff(iN2)
  enddo

  !some gut checks to make sure `luca code' makes physical sense
  print *,"**************************************************************"
  print *,"some gut checks to make sure `luca code' makes physical sense"
  print *,"**************************************************************"
  massFractions(:,:,:)=0.0_rk !pure nitrogen
  massFractions(iN2,:,:)=1.0_rk !pure nitrogen 
  print*,"density of nitrogen[kg/m^3]=",1.0_rk/(Mwcoeff(iN2)+sum(Mwcoeff(1:nspecies)*massFractions(1:nspecies,1,1)))*nondimR
  massFractions(:,:,:)=0.0_rk !pure nitrogen
  massFractions(iH2,:,:)=1.0_rk !pure hydrogen 
  print*,"density of hydrogen[kg/m^3]=",1.0_rk/(Mwcoeff(iN2)+sum(Mwcoeff(1:nspecies)*massFractions(1:nspecies,1,1)))*nondimR
  massFractions(:,:,:)=0.0_rk 
  massFractions(iO2,:,:)=YO2               
  massFractions(iN2,:,:)=1.0-YO2 
  print *,"density of air[kg/m^3]=",1.0_rk/(Mwcoeff(iN2) +sum(Mwcoeff(1:nspecies)*massFractions(1:nspecies,1,1)))*nondimR
  print *,"**************************************************************"

  print *,"**************************************************************"
  print *,"ad-hoc boundary layer of delta_99 = slot width"
  print *,"The functional form=tanh(2.646652412/(slotwidth)*(xpos-*xplug))"
  print *,"**************************************************************"
  cref=1.0
  massFractions(:,:,:)=0._rk
  massFractions(iO2,:,:)=1.0_rk !YO2
  xplug=X(1,iplug,1)

  !density(l0)*temperature(l0) / gamma

  do i=1,nPoints(1)
  do j=1,nPoints(2) 

   if (i .ge. iplug) then
   xpos=X(1,i,j)
   Q(1,i,j)=testSectionDensity/nondimR !1.0_rk/(Mwcoeff(iN2) + sum(Mwcoeff(1:nspecies)*massFractions(1:nspecies,i,j)))
   Q(3,i,j)=Q(1,i,j)*testSectionDensity/nondimVel*tanh(2.646652412/(slotwidth)*(xpos-xplug))
   Q(4,i,j)=Q(1,i,j)*testSectionTemperature/nondimT/gamma + 0.5_rk * Q(3,i,j)**2/Q(1,i,j)
   Qaux(1:nspecies,i,j)=Q(1,i,j) * massFractions(1:nspecies,i,j)
   else
   Q(1,i,j)=testSectionDensity/nondimR !1.0_rk/(Mwcoeff(iN2) + sum(Mwcoeff(1:nspecies)*massFractions(1:nspecies,i,j)))
   Q(2,i,j)=0. 
   Q(3,i,j)=0. 
   Q(4,i,j)=Q(1,i,j)*testSectionTemperature/nondimT/gamma + 0.5_rk * Q(3,i,j)**2/Q(1,i,j)
   Qaux(1:nspecies,i,j)=Q(1,i,j) * massFractions(1:nspecies,i,j)
   end if

  end do
  end do


!This adds in the LIB on top of the provided Q solution
if (.false.) then 

   !read grid from example directory
   !GridPath = trim(ExecPath)//trim(TemplateFlnm)
   SSprec = ""; SSgf = ""; SSvf = ""; SSiblocal = ""
   print *,"made it here"
   print *,"sizeof(SSND)",sizeof(SSND)
   allocate(SSND(1,3))
   print *,SSND
   deallocate(SSND)
   print *,"going to read the mesh"
   GridPath=trim("grid.xyz")
   SolPath=trim("solution.q")
   call Read_Grid_Size(SSNDIM, SSngrid, SSND, GridPath, 1, SSiblocal)
   Call Read_Grid(SSNDIM, SSngrid, SSND, XTemp, SSIB,SSprec,SSgf,SSvf,SSiblocal,GridPath, 1)
   print*,'SS grid X extent is',minval(XTemp(:,:,:,:,1)),maxval(XTemp(:,:,:,:,1))
   print*,'SS grid Y extent is',minval(XTemp(:,:,:,:,2)),maxval(XTemp(:,:,:,:,2))
   Call Read_Soln_Header(SSNDIM, SSngrid, SSND,SolPath, SStau)
   print*,'Read soln, size is', SSNDIM,SSngrid,SSNd,SStau
   Call Read_Soln(SSNDIM, SSngrid, SSND, QTemp, SStau, SSprec, SSgf, SSvf,SolPath, 1)
   print*,'Read soln, size is', shape(QTemp)

   do j=1,nPoints(2)
   do i=1,nPoints(1)
    Q(1,i,j) = QTemp(1,i,j,1,1) 
    Q(2,i,j) = QTemp(1,i,j,1,2) 
    Q(3,i,j) = QTemp(1,i,j,1,3) 
    Q(4,i,j) = QTemp(1,i,j,1,5) 
   end do
   end do

  jSlotJetCenter=int((jSlottedJetStart+jSlottedJetEnd)/2)
  VerticalOffSetFromJet=VerticalOffSetFromJet+XTemp(1,1,jSlotJetCenter,1,2)
  iPlaceLIB=0
  jPlaceLIB=0
  do j=1,nPoints(2)
  do i=1,nPoints(1)
    if (XTemp(1,i,j,1,1).le.HorizontalOffSetFromCenterline) iPlaceLIB=i
    if (XTemp(1,i,j,1,2).le.VerticalOffSetFromJet)jPlaceLIB=j 
  end do
  end do

  print *,"i,j of LIB",iPlaceLIB,jPlaceLIB
  print*,XTemp(1,iPlaceLIB,jPlaceLIB,1,1),XTemp(1,iPlaceLIB,jPlaceLIB,1,2)-XTemp(1,iPlaceLIB,jSlotJetCenter,1,2)
 
  massFractions(:,:,:)=0. 
  filename='128x128.dat'
  print *,"Reading lib data file: ",trim(filename)
  nskip=18
  OPEN(1,FILE=trim(filename))
  do s=1,nskip
  READ(1,*) dummyline
  end do
  do j=jPlaceLIB-64,jPlaceLIB+63
   do i=iPlaceLIB-64,iPlaceLIB+63
     xpos=X(1,i,j)
     read(1,*),dummyVal,dummyVal,dummyVal,&
               dummyVal,& !Qaux(7,i,j),&
               dummyVal,& !Qaux(2,i,j),&
               uvelLIB,&! Q(2,i,j),&
               vvelLIB,&!Q(3,i,j),&
               dummyVal,& !ignore the w-velocity
               temperatureLIB,& !Q(4,i,j),&
               densityLIB,&  !Q(1,i,j)
               YOLIB,&
               YO2LIB

      if (temperatureLIB .ge. 300.) then

      YO2LIBREF=0.232917502522468567 
      densityLIBREF=1.22
      temperatureLIBREF=288.
      velocityLIBREF=343. 

      Q(4,i,j)=1.89*temperatureLIB/temperatureLIBREF*Tref/gamma*Q(1,i,j) + 0.5_rk *Q(3,i,j)**2/Q(1,i,j)+ 0.5_rk * Q(2,i,j)**2/Q(1,i,j)

      end if
    end do
  end do
  close(1)

end if
  
  call WritePLOT3DSolution("initial.q", nDims=2, nPoints=nPoints, Q=Q)
  call WritePLOT3DFunc("initial.aux.q", nDims=2, nPoints=nPoints, F=Qaux)

  !initializing the target data for the sonic hydrogen jet 
  !underexapnded with the pressure from the input deck
  massFractions(:,:,:)=0._rk
  massFractions(iH2,:,:)=1.0_rk
  do j=jSlottedJetStart,jSlottedJetEnd
  do i=jetStart,iplug
   y=X(2,i,j)
   xpos=X(1,i,j)
   Q(1,i,j)=fuelDensity/nonDimR !DensityRatio/(Mwcoeff(iN2) +sum(Mwcoeff(1:nspecies)*massFractions(1:nspecies,i,j)))
   Q(2,i,j)=Q(1,i,j)*fuelVelocity/nonDimVel!*(tanh(10.*(y-ystartjet))-tanh(10.*(y-yendJet))-1.)
   Q(3,i,j)=0._rk
   Q(4,i,j)=Q(1,i,j)*fuelTemperature/nonDimT/gamma+0.5*(Q(2,i,j)**2)/Q(1,i,j)
   Qaux(1:nspecies,i,j)=Q(1,i,j)*massFractions(1:nspecies,i,j)  
  end do
  end do 

  call WritePLOT3DSolution("initial.target.q", nDims=2, nPoints=nPoints, Q=Q)
  call WritePLOT3DFunc("initial.target.aux.q", nDims=2, nPoints=nPoints, F=Qaux)

  print *,"**************************************************************"
  print *,"The ambient pressure to jet pressure ratio="!,!P 
  print *,"**************************************************************"

  allocate(BCs(11))
  BCs(1) =  t_bc_(1, BC_TYPE_SAT_SLIP_ADIABATIC_WALL, -1, [-1,-1], [1,-1])
  BCs(2) =  t_bc_(1, BC_TYPE_SAT_FAR_FIELD, 2, [iplug,-1], [1,1])
  BCs(3) =  t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -2, [iplug,-1], [-1,-1])
  BCs(4) =  t_bc_(1, BC_TYPE_SAT_SLIP_ADIABATIC_WALL,1,[iplug,iplug],[1,jSlottedJetStart])
  BCs(5) =  t_bc_(1, BC_TYPE_SAT_SLIP_ADIABATIC_WALL,1,[iplug,iplug],[jSlottedJetEnd,-1])
  BCs(6) =  t_bc_(1, BC_TYPE_SPONGE, -2, [iplug,-1],[nPoints(2)-int(plugRadius/dy),-1])
  BCs(7) =  t_bc_(1, BC_TYPE_SPONGE, 2, [iplug,-1], [1,int(plugRadius/dy)])
  BCs(8) =  t_bc_(1, BC_TYPE_SAT_FAR_FIELD,1,[jetStart,jetStart],[jSlottedJetStart,jSlottedJetEnd])
  BCs(9) =  t_bc_(1, BC_TYPE_SAT_SLIP_ADIABATIC_WALL,2,[jetStart,iplug],[jSlottedJetStart,jSlottedJetStart])
  BCs(10) = t_bc_(1, BC_TYPE_SAT_SLIP_ADIABATIC_WALL,-2,[jetStart,iplug],[jSlottedJetEnd,jSlottedJetEnd])
  BCs(11) = t_bc_(1, BC_TYPE_SPONGE, 1,[jetStart,iplug],[jSlottedJetStart,jSlottedJetEnd])

  call WritePLOT3DGrid("grid.xyz", nDims=2, nPoints=nPoints, X=X, IBlank = IB)
  call WriteBCs(BCs, "bc.dat")

contains

!consider having total (stagnation properties as inputs)
real function get_isentropic_pressure(M,Po,gamma)
real(rk)::M
real(rk)::Po
real(rk)::gamma
get_isentropic_pressure=0.
get_isentropic_pressure=(1.+(gamma-1.)/2.*M**2)**(-gamma/(gamma-1.))*Po
end function
real function get_isentropic_density(M,rhoo,gamma)
real(rk)::M
real(rk)::rhoo
real(rk)::gamma
get_isentropic_density=0.
get_isentropic_density=(1.+(gamma-1.)/2.*M**2)**(-1./(gamma-1.))*rhoo
end function
real function get_isentropic_temperature(M,To,gamma)
real(rk)::M
real(rk)::To
real(rk)::gamma
get_isentropic_temperature=0.
get_isentropic_temperature=(1.+(gamma-1.)/2.*M**2)**(-1)*To
end function
real function get_isentropic_soundSpeed(M,co,gamma)
real(rk)::M
real(rk)::co
real(rk)::gamma
get_isentropic_soundSpeed=0.
get_isentropic_soundSpeed=(1.+(gamma-1.)/2.*M**2)**(-1./2.)*co
end function
 
end program ImpingingJetAxisym

Subroutine P3D_Read_Soln_Header(NDIM, ngrid, ND, fname, tau)

    ! ... function call variables
    Integer :: NDIM, ngrid
    Integer, Dimension(:,:), Pointer :: ND
    Character(LEN=*) :: fname
    Real(KIND=8) :: tau(4)

    ! ... local variables
    Integer :: ibuf, I, J

    ! ... open the file
    Open (Unit=10, file=trim(fname), form='unformatted', status='old')

    ! ... read the number of grids, exit if error
    Read (10) ibuf
    print *,ibuf

    ! ... read the size of each grid, throw value away
    Read (10) ((ibuf,J=1,NDIM),I=1,ngrid)
    print *,NDIM

    ! ... read the header
    Read (10) tau
    print *,tau

    ! ... close and exit
    Close (10)

  End Subroutine P3D_Read_Soln_Header

  Subroutine P3D_Read_Soln(NDIM, ngrid, ND, X, tau, prec, gf, vf, file, OF)

    Implicit None

    Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X
    Real(KIND=8) :: tau(4)
    Character(LEN=2) :: prec, gf, vf, ib
    Character(LEN=80) :: file
    Integer :: ngrid, I, J, K, L, M, Nx, Ny, Nz, NDIM, ftype
    Integer, Dimension(:,:), Pointer :: ND
    Real(KIND=4), Dimension(:,:,:,:), Pointer :: fX
    Real(KIND=4) :: ftau(4)
    Logical :: gf_exists
    Integer :: OF

    Inquire(file=Trim(file),exist=gf_exists)
    If (.NOT.gf_exists) Then
      Write (*,'(A,A,A)') 'File ', file(1:LEN_TRIM(file)), ' is not readable'
      Stop
    End If

    prec = ""; gf = ""; vf = ""; ib = ""
    Call p3d_detect(LEN(Trim(file)),Trim(file),prec,NDIM,gf,vf,ib,ftype)

    Open (unit=10, file=trim(file), form='unformatted', status='old')

    !  Read number of grids
    If (gf(1:1) .eq. 's') Then
      ngrid = 1
    Else
      Read (10) ngrid
    End If


    !  Read grid sizes
    Allocate(ND(ngrid,3))
    ND(:,3) = 1
    Read (10) ((ND(I,J),J=1,NDIM),I=1,ngrid)

    !  Allocate soln memory
    Nx = MAXVAL(ND(:,1))
    Ny = MAXVAL(ND(:,2))
    If (NDIM .EQ. 3) Then
      Nz = MAXVAL(ND(:,3))
    Else
      Nz = 1
    End If
    If (associated(X)) deallocate(X)
    Allocate(X(ngrid,Nx,Ny,Nz,NDIM+2))
    If (prec(1:1) .eq. 's') Allocate(fX(Nx,Ny,Nz,NDIM+2))
    X = 0D0

    !  Read soln values
    If (vf(1:1) .eq. 'w') Then
      If (prec(1:1) .eq. 'd') Then
        Do L = 1, ngrid
          Read (10) tau(1), tau(2), tau(3), tau(4)
          Read (10)((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM+2)
        End Do
      Else
        Do L = 1, ngrid
          Read (10) ftau(1), ftau(2), ftau(3), ftau(4)
          Read (10)((((fX(I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM+2)
          X(L,:,:,:,:) = Dble(fX(:,:,:,:))
          tau = Dble(ftau)
        End Do
      End If

    Else
      If (prec(1:1) .eq. 'd') Then
        Do L = 1, ngrid
          Read (10) tau(1), tau(2), tau(3), tau(4)
          Do K = 1, ND(L,3)
            Read (10) (((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),M=1,NDIM+2)
          End Do
        End Do
      Else
        Do L = 1, ngrid
          Read (10) ftau(1), ftau(2), ftau(3), ftau(4)
          Do K = 1, ND(L,3)
            Read (10) (((fX(I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),M=1,NDIM+2)
          End Do
          X(L,:,:,:,:) = Dble(fX(:,:,:,:))
          tau = Dble(ftau)
        End Do
      End If
    End If

    Close (10)

    If (prec(1:1) .eq. 's')  Deallocate(fX)

    Return
  End Subroutine P3D_Read_Soln

  Subroutine P3D_Read_Grid(NDIM, ngrid, ND, X, IBLANK, prec, gf, vf, ib, file,OF)

    Implicit None

    Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Character(LEN=80) :: file
    Integer :: ngrid, I, J, K, L, Nx, Ny, Nz, NDIM, M, ftype
    Integer, Dimension(:,:), Pointer :: ND
    Integer :: OMap(3), NMap(3), ND_copy(3)
    Real(KIND=4), Dimension(:,:,:,:), Pointer :: fX
    Logical :: gf_exists
    Integer :: OF

    Inquire(file=Trim(file),exist=gf_exists)
    If (.NOT.gf_exists) Then
      Write (*,'(A,A,A)') 'File ', file(1:LEN_TRIM(file)), ' is not readable'
      Stop
    End If

    prec = ""; gf = ""; vf = ""; ib = ""
    Call p3d_detect(LEN(Trim(file)),Trim(file),prec,NDIM,gf,vf,ib,ftype)

    Open (unit=10, file=trim(file), form='unformatted', status='old')

    !  Read number of grids
    If (gf(1:1) .eq. 's') Then
      ngrid = 1
    Else
      Read (10) ngrid
    End If
    Allocate(ND(ngrid,3))

    !  Read grid sizes
    ND(:,3) = 1
    Read (10) ((ND(I,J),J=1,NDIM),I=1,ngrid)

    !  Allocate grid memory
    Nx = MAXVAL(ND(:,1))
    Ny = MAXVAL(ND(:,2))
    Nz = MAXVAL(ND(:,3))
    Allocate(X(ngrid,Nx,Ny,Nz,NDIM))
    If (prec(1:1) .eq. 's') Allocate(fX(Nx,Ny,Nz,NDIM))
    Allocate(IBLANK(ngrid,Nx,Ny,Nz))

    !  Read grid values
    If (vf(1:1) .eq. 'w') Then
      If (prec(1:1) .eq. 'd') Then
        If (ib(1:1) .eq. 'n') Then
          Do L = 1, ngrid
            Read (10)((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM)
          End Do
        Else
          Do L = 1, ngrid
            Read (10)((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM), &
                 (((IBLANK(L,I,J,K),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3))
          End Do
        End If
      Else
        If (ib(1:1) .eq. 'n') Then
          Do L = 1, ngrid
            Read (10)((((fX(I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM)
            X(L,:,:,:,:) = Dble(fX(:,:,:,:))
          End Do
        Else
          Do L = 1, ngrid
            Read (10)((((fX(I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM), &
                 (((IBLANK(L,I,J,K),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3))
            X(L,:,:,:,:) = Dble(fX(:,:,:,:))
          End Do
        End If
      End If

    Else
      If (NDIM .EQ. 3) Then
        If (prec(1:1) .eq. 'd') Then
          If (ib(1:1) .eq. 'n') Then
            Do L = 1, ngrid
              Do K = 1, ND(L,3)
                Read (10) ((X(L,I,J,K,1),I=1,ND(L,1)),J=1,ND(L,2)), &
                     ((X(L,I,J,K,2),I=1,ND(L,1)),J=1,ND(L,2)), &
                     ((X(L,I,J,K,3),I=1,ND(L,1)),J=1,ND(L,2))
              End Do
            End Do
          Else
            Do L = 1, ngrid
              Do K = 1, ND(L,3)
                Read (10) ((X(L,I,J,K,1),I=1,ND(L,1)),J=1,ND(L,2)), &
                     ((X(L,I,J,K,2),I=1,ND(L,1)),J=1,ND(L,2)), &
                     ((X(L,I,J,K,3),I=1,ND(L,1)),J=1,ND(L,2)), &
                     ((IBLANK(L,I,J,K),I=1,ND(L,1)),J=1,ND(L,2))
              End Do
            End Do
          End If
        Else
          If (ib(1:1) .eq. 'n') Then
            Do L = 1, ngrid
              Do K = 1, ND(L,3)
                Read (10) ((fX(I,J,K,1),I=1,ND(L,1)),J=1,ND(L,2)), &
                     ((fX(I,J,K,2),I=1,ND(L,1)),J=1,ND(L,2)), &
                     ((fX(I,J,K,3),I=1,ND(L,1)),J=1,ND(L,2))
              End Do
              X(L,:,:,:,:) = Dble(fX(:,:,:,:))
            End Do
          Else
            Do L = 1, ngrid
              Do K = 1, ND(L,3)
                Read (10) ((fX(I,J,K,1),I=1,ND(L,1)),J=1,ND(L,2)), &
                     ((fX(I,J,K,2),I=1,ND(L,1)),J=1,ND(L,2)), &
                     ((fX(I,J,K,3),I=1,ND(L,1)),J=1,ND(L,2)), &
                     ((IBLANK(L,I,J,K),I=1,ND(L,1)),J=1,ND(L,2))
              End Do
              X(L,:,:,:,:) = Dble(fX(:,:,:,:))
            End Do
          End If
        End If
      Else
        If (prec(1:1) .eq. 'd') Then
          If (ib(1:1) .eq. 'n') Then
            Do L = 1, ngrid
              Do J = 1, ND(L,2)
                Read (10) (X(L,I,J,1,1),I=1,ND(L,1)), &
                     (X(L,I,J,1,2),I=1,ND(L,1))
              End Do
            End Do
          Else
            Do L = 1, ngrid
              Do J = 1, ND(L,2)
                Read (10) (X(L,I,J,1,1),I=1,ND(L,1)), &
                     (X(L,I,J,1,2),I=1,ND(L,1)), &
                     (IBLANK(L,I,J,1),I=1,ND(L,1))
              End Do
            End Do
          End If
        Else
          If (ib(1:1) .eq. 'n') Then
            Do L = 1, ngrid
              Do K = J, ND(L,2)
                Read (10) (fX(I,J,1,1),I=1,ND(L,1)), &
                     (fX(I,J,1,2),I=1,ND(L,1))
              End Do
              X(L,:,:,:,:) = Dble(fX(:,:,:,:))
            End Do
          Else
            Do L = 1, ngrid
              Do K = J, ND(L,2)
                Read (10) (fX(I,J,1,1),I=1,ND(L,1)), &
                     (fX(I,J,1,2),I=1,ND(L,1)), &
                     (IBLANK(L,I,J,1),I=1,ND(L,1))
              End Do
              X(L,:,:,:,:) = Dble(fX(:,:,:,:))
            End Do
          End If
        End If
      End If
    End If

    Close (10)

    If (prec(1:1) .eq. 's')  Deallocate(fX)
    If (ib(1:1) .eq. 'n') IBLANK = 1

    Return
  End Subroutine P3D_Read_Grid


