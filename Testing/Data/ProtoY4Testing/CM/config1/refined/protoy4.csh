#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/csh
#MSUB -l nodes=2
#MSUB -l partition=quartz
#MSUB -l walltime=04:00:00
#MSUB -q pbatch
#MSUB -V

##### These are shell commands
date
cd /g/g17/mtcampbe/xpacc/Users/mtcampbe/PlasComCM/build-protoy4/examples/InitialY4/coarse
##### Launch parallel job using srun
srun -n 32 -N 2 ./plascomcm RocFlo-CM.00000000.h5
echo 'Done'
