shock-test:Init:CaseName = Shock1D
# shock Parameters: Mach# direction location width reference_frame (0 stationary, 1 shock moving)
shock-test:Init:Shock1D:Params = 2.0 1.0 0.50 0.10 1
#  ProtoY4 configuration bits: [acousticPulse(1) crossFlow(2) crossFlowProfile(4) injectionFlow(8) injectionFlowProfile(16)]
#shock-test:Init:shock:Flags = 3 # set acoustic pulse with profiled crossflow
shock-test:GridNames = geometry:shock2d3

shock-test:PeriodicDirs = 1 1 
shock-test:SpatialOrder = 2
shock-test:NumStepsProbe = 100
shock-test:IO:Fields = time deltaT rho rhoV rhoE pressure temperature
shock-test:Advancer:Fields = rho rhoV rhoE
# Data dictionary configuration
shock-test:State:Fields = simTime rho rhoV rhoE pressure temperature rhom1 velocity sconn imask
shock-test:Param:Fields = gamma sigma inputDT inputCFL refRe refPr refLength refRho refPressure refTemperature power beta bulkViscFac Flags nonDimensional
# Field metadata
shock-test:Field:simTime:Meta        = 1 m 8 time
shock-test:Field:refLength:Meta      = 1 m 8 length
shock-test:Field:refRho:Meta         = 1 m 8 density
shock-test:Field:refPressure:Meta    = 1 m 8 pressure
shock-test:Field:refTemperature:Meta = 1 m 8 temperature
shock-test:Field:inputDT:Meta        = 1 m 8 time
shock-test:Field:sconn:Meta          = 2 n 4 sconn
shock-test:Field:imask:Meta          = 1 n 4 imask
shock-test:Field:rho:Meta            = 1 n 8 density
shock-test:Field:rhoV:Meta           = 2 n 8 momentum
shock-test:Field:rhoE:Meta           = 1 n 8 energy
shock-test:Field:pressure:Meta       = 1 n 8 pressure
shock-test:Field:temperature:Meta    = 1 n 8 temperature
shock-test:Field:rhom1:Meta          = 1 n 8 1/density
shock-test:Field:velocity:Meta       = 2 n 8 velocity   
shock-test:Field:gamma:Meta          = 1 s 8 shratio
shock-test:Field:power:Meta          = 1 s 8 powerLawTransport
shock-test:Field:beta:Meta           = 1 s 8 powerLawTransport
shock-test:Field:bulkViscFac:Meta    = 1 s 8 powerLawTransport
shock-test:Field:sigma:Meta          = 1 s 8 filterstrength
shock-test:Field:inputCFL:Meta       = 1 s 8 stabilityno
shock-test:Field:refRe:Meta          = 1 s 8 ReynoldsNo
shock-test:Field:refPr:Meta          = 1 s 8 PrandtlNo
shock-test:Field:Flags:Meta          = 2 s 4 EulerFlags
shock-test:Field:nonDimensional:Meta = 1 s 4 non-dimensional
#
# Set some parameter values
shock-test:Param:gamma          = 1.4
shock-test:Param:power          = 0.0 # mu = beta
shock-test:Param:beta           = 1.0
shock-test:Param:bulkViscFac    = 0.0
shock-test:Param:sigma          = 0.0
shock-test:Param:inputCFL       = 1.0
#shock-test:Param:refRe          = 10.0 
shock-test:Param:refRe          = 20.8429
#shock-test:Param:refRe          = 0.0
shock-test:Param:refPr          = 0.75 
shock-test:Param:Flags          = 3   #  ALTERNATERHS=1,EXCHANGEFLUX=2,USEWENO=4
shock-test:Param:inputDT        = 1e-4
shock-test:Param:refLength      = 1.0
shock-test:Param:refRho         = 1.225
shock-test:Param:refPressure    = 101325
shock-test:Param:refTemperature = 288.15
shock-test:Param:nonDimensional = 2

# 
# ------------- BOUNDARY CONFIGURATION --------------------------
#
# These are the boundary condition names
shock-test:BCNames = FarField
#
# Configuration of the FarField boundary condition
shock-test:FarField:BCType = SAT_FARFIELD
shock-test:FarField:Param:Fields = Numbers Flags # sigma1 sigma2 boundaryWeight
shock-test:FarField:Field:Numbers:Meta = 3 s 8
shock-test:FarField:Field:Flags:Meta = 1 s 4
shock-test:FarField:Param:Numbers = 0.5 1.0 0.0
shock-test:FarField:Param:Flags = 0
# Apply FarField BC to these grid regions
shock-test:FarField:RegionNames = Left Right
shock-test:FarField:BoundaryNames = Inlet Outlet # optional nicknames
#
