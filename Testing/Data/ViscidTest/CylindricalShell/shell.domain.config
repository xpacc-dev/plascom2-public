Init:CaseName = ShellFlow
# ShellFlow Parameters: Signed flow speed (+Z,-Z direction)
Init:ShellFlow:Params = 3.0
GridNames = shellgeom:shell1
#
PeriodicDirs = 1 1 
SpatialOrder = 2
NumStepsProbe = 100
# Set some parameter values
Param:gamma          = 1.4
Param:power          = 0.0 # mu = beta
Param:beta           = 1.0
Param:bulkViscFac    = 0.0
Param:sigma          = 0.0
Param:sigmaDissipation = 0.0
Param:sigmaDilatation = 0.0
Param:inputCFL       = 1.0
#Param:refRe          = 10.0 
Param:refRe          = 1000000
#Param:refRe          = 0.0
Param:refPr          = 0.75 
Param:Flags          = 3   #  ALTERNATERHS=1,EXCHANGEFLUX=2,USEWENO=4
Param:inputDT        = 1e-6
Param:refLength      = 1.0
Param:refRho         = 1.225
Param:refPressure    = 101325
Param:refTemperature = 288.15
Param:nonDimensional = 2

# 
# ------------- BOUNDARY CONFIGURATION --------------------------
#
# These are the boundary condition names
BCNames = Wall
#
# Configuration of the Inlet and Outlet
InOut:BCType = SAT_FARFIELD
InOut:Param:Fields = Numbers Flags # sigma1 sigma2 boundaryWeight
InOut:Field:Numbers:Meta = 3 s 8
InOut:Field:Flags:Meta = 1 s 4
InOut:Param:Numbers = 0.5 1.0 0.0
InOut:Param:Flags = 0
InOut:RegionNames = Left Right
InOut:BoundaryNames = Inlet Outlet # optional nicknames
#
# Configuration of the wall on inner and outer surfaces
Wall:BCType = SAT_NOSLIP_ISOTHERMAL
Wall:Param:Fields = Numbers Flags # sigma1 sigma2 temperature boundaryWeight
Wall:Field:Numbers:Meta = 4 s 8
Wall:Field:Flags:Meta = 0
Wall:Param:Numbers = 2.0 2.0 -1.0 0.0
Wall:Flags = 0
Wall:RegionNames = Inner Outer
Wall:BoundaryNames = Inner Outer

IO:Fields = time deltaT rho rhoV rhoE pressure temperature
Advancer:Fields = rho rhoV rhoE
# Data dictionary configuration
State:Fields = simTime rho rhoV rhoE pressure temperature rhom1 velocity sconn imask
Param:Fields = gamma sigma inputDT inputCFL refRe refPr refLength refRho refPressure refTemperature power beta bulkViscFac Flags nonDimensional sigmaDissipation sigmaDilatation
# Field metadata
Field:simTime:Meta        = 1 m 8 time
Field:refLength:Meta      = 1 m 8 length
Field:refRho:Meta         = 1 m 8 density
Field:refPressure:Meta    = 1 m 8 pressure
Field:refTemperature:Meta = 1 m 8 temperature
Field:inputDT:Meta        = 1 m 8 time
Field:sconn:Meta          = 3 n 4 sconn
Field:imask:Meta          = 1 n 4 imask
Field:rho:Meta            = 1 n 8 density
Field:rhoV:Meta           = 3 n 8 momentum
Field:rhoE:Meta           = 1 n 8 energy
Field:pressure:Meta       = 1 n 8 pressure
Field:temperature:Meta    = 1 n 8 temperature
Field:rhom1:Meta          = 1 n 8 1/density
Field:velocity:Meta       = 3 n 8 velocity   
Field:gamma:Meta          = 1 s 8 shratio
Field:power:Meta          = 1 s 8 powerLawTransport
Field:beta:Meta           = 1 s 8 powerLawTransport
Field:bulkViscFac:Meta    = 1 s 8 powerLawTransport
Field:sigma:Meta          = 1 s 8 filterstrength
Field:inputCFL:Meta       = 1 s 8 stabilityno
Field:refRe:Meta          = 1 s 8 ReynoldsNo
Field:refPr:Meta          = 1 s 8 PrandtlNo
Field:Flags:Meta          = 2 s 4 EulerFlags
Field:nonDimensional:Meta = 1 s 4 non-dimensional
Field:sigmaDissipation:Meta = 1 m 8 artificialDissipation
Field:sigmaDilatation:Meta = 1 m 8 artDissDilatationWt
#
