!
! Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!!hash:0efd458ef22fdcfc1f5532dc1bedd17a61f290d0
SUBROUTINE TESTKERNEL(result)

  INTEGER :: result

  result = 1
  WRITE(*,*) 'Hello Hoth.'

END SUBROUTINE TESTKERNEL
