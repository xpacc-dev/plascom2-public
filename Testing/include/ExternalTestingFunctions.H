//
// Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
// License: MIT, http://opensource.org/licenses/MIT
//
///
/// @file Testing function signatures
///
// Serial test signatures

// Halo tests
void TestHaloBasic(ix::test::results &result);
void TestHaloThreaded(ix::test::results &result);

// Monolithic finite difference tests
// void TestFD1(ix::test::results &result);
// void TestFD1_2(ix::test::results &result);
// void TestFD1_3(ix::test::results &result);
// void TestFD1_Div3All(ix::test::results &result);

// Advancer tests
//void Test1DAdvectionAdvancer(ix::test::results &result);
void TestRK4Advancer(ix::test::results &result);
void TestRK4Advancer2(ix::test::results &result);

// Euler stuff
//void TestRK4EulerAdvancer(ix::test::results &result);
//void TestEulerRHS(ix::test::results &result);
void TestEulerKernels(ix::test::results &result);

void TestViscidKernels(ix::test::results &result);
void TestViscidKernelsMetrics(ix::test::results &result);
void TestViscidKernelsCurvilinear(ix::test::results &result);

// State tests
void TestStateBasic(ix::test::results &result);
void TestStateOperations(ix::test::results &result);
void TestStateHandles(ix::test::results &result);


// SBP operators and such
void TestSBPInitialize(ix::test::results &result);
void TestOperatorSBP12(ix::test::results &result);
void TestOperatorSBP24(ix::test::results &result);
void TestOperatorSBP36(ix::test::results &result);
void TestApplyOperatorBlobs(ix::test::results &result);
//void TestApplyOperator1(ix::test::results &result);
void TestApplyOperatorAppl(ix::test::results &result);
void TestCurlOperator(ix::test::results &result);
void TestHoleDetection(ix::test::results &result);

// void TestMaxwellRHS_Bfield(ix::test::results &result);
// void TestMaxwellRHS_Dfield(ix::test::results &result);
// void TestComputeRecipEpsMu(ix::test::results &result);
// void TestConvertFields(ix::test::results &result);
// void TestDirichletBC(ix::test::results &result);
// void TestMaxwellRHS(ix::test::results &result);

// Arithmetic operators
void TestOperators(ix::test::results &result);

// Grid tests
//void TestGrid_PUBS(ix::test::results &result);
void TestGrid_SubRegion(ix::test::results &result);

// WENO tests
void TestWENO_Stencils(ix::test::results &result);
void TestWENO_ReconstPointVal(ix::test::results &result);
void TestWENO_ReconstPointValSten(ix::test::results &result);
void TestWENO_SmoothInd(ix::test::results &result);
void TestWENO_Project(ix::test::results &result);
void TestWENO_EntropyFix(ix::test::results &result);

// EOS Unit Tests
void TestEOS_ComputeBuffers(ix::test::results &result);

#ifdef USE_OVERKIT
// Overkit tests
void TestOverkit_Config(ix::test::results &,pcpp::CommunicatorType &);
#endif

// Various junk
void TestVarious(ix::test::results &result);
void TestMatMat(ix::test::results &result);
void TestMatVec(ix::test::results &result);
void TestProbe(ix::test::results &result);
void TestIntervalBasic(ix::test::results &result);
#ifdef HAVE_ICE
void TestICE(ix::test::results &result);
#ifdef ICE_HAVE_ROSE
void TestICE_Rose(ix::test::results &result);
#endif
#endif

// Parallel test signatures
void TestGrid_PBS_IntegratedHalo(ix::test::results &parallelUnitResults,
                                 pcpp::CommunicatorType &testComm);
void TestGrid_CartesianMetric(ix::test::results &parallelUnitResults,
                              pcpp::CommunicatorType &testComm);
void TestGrid_RectilinearMetric(ix::test::results &parallelUnitResults,
                                pcpp::CommunicatorType &testComm);
void TestGrid_CurvilinearMetric(ix::test::results &parallelUnitResults,
                                pcpp::CommunicatorType &testComm);
void TestGrid_CurvilinearVGWavy(ix::test::results &parallelUnitResults,
                                pcpp::CommunicatorType &testComm);
void TestHaloParallel(ix::test::results &,pcpp::CommunicatorType &);
void TestHaloPeriodic(ix::test::results &,pcpp::CommunicatorType &);
#ifdef ENABLE_HDF5
void TestHDF5LegacyFileInfo(ix::test::results&,pcpp::CommunicatorType &);
void TestHDF5Read2DBlockStructuredGrid(ix::test::results &,pcpp::CommunicatorType &);
void TestPCPPHDF5Read(ix::test::results &,pcpp::CommunicatorType &);
void TestPCPPHDF5Write(ix::test::results &,pcpp::CommunicatorType &);
void TestHDF5HyperSlab(ix::test::results &,pcpp::CommunicatorType &);
void TestHDF5GridIO(ix::test::results &,pcpp::CommunicatorType &);
void TestHDF5PBSGridIO(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2Output(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2Input(ix::test::results &,pcpp::CommunicatorType &);
#endif
void TestEulerRHS(ix::test::results &,pcpp::CommunicatorType &);
void TestEulerRHS2(ix::test::results &,pcpp::CommunicatorType &);
void TestViscidRHS(ix::test::results &,pcpp::CommunicatorType &);
void TestVelocityGradient(ix::test::results &,pcpp::CommunicatorType &);
void TestVelocityGradientPeriodic(ix::test::results &,pcpp::CommunicatorType &);
//void TestMaxwellRHSTimeIntegrate(ix::test::results &, pcpp::CommunicatorType &);
void TestWENO_RHS(ix::test::results &, pcpp::CommunicatorType &);
void TestWENO_ApplyWENO(ix::test::results &, pcpp::CommunicatorType &);
void TestComm_Split(ix::test::results &,pcpp::CommunicatorType &);
void TestWENO_ApplyWENOBounded(ix::test::results &, pcpp::CommunicatorType &);
void TestWENO_StencilConnectivity(ix::test::results &, pcpp::CommunicatorType &);

// Integrated PlasCom2 tests
void TestIntegrated_Poiseuille2DX(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_PFRectilinear2DX(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_PFCurvilinear2DX(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_Poiseuille2DY(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_PFRectilinear2DY(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_PFCurvilinear2DY(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_Poiseuille3DZ(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_PFRectilinear3DZ(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_PFCurvilinear3DZ(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_ViscousShock2DX(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_VSRectilinear2DX(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_VSCurvilinear2DX(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_ViscousShock2DY(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_VSRectilinear2DY(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_VSCurvilinear2DY(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_ViscousShock3DZ(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_VSRectilinear3DZ(ix::test::results &,pcpp::CommunicatorType &);
void TestIntegrated_VSCurvilinear3DZ(ix::test::results &,pcpp::CommunicatorType &);


// PlasCom2 regression tests
void TestPlasCom2_Poiseuille2DX_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_Poiseuille2DY_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_Poiseuille3DZ_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_ViscousShock2DX_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_ViscousShock2DY_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_ViscousShock3DZ_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_VSCurvilinear2DX_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_VSRectilinear2DX_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_VSCurvilinear2DY_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_VSRectilinear2DY_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_VSCurvilinear3DZ_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_VSRectilinear3DZ_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_PFCurvilinear2DX_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_PFRectilinear2DX_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_PFCurvilinear2DY_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_PFRectilinear2DY_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_PFCurvilinear3DZ_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_PFRectilinear3DZ_Regression(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_VSCurvilinearAD3DZ_Regression(ix::test::results &,pcpp::CommunicatorType &);



void TestPlasCom2_AcousticPulsePeriodic2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_AcousticPulsePeriodic3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_AcousticPulseFarfield2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_AcousticPulseFarfield3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_AcousticPulseSlipwall2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_AcousticPulseSlipwall3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_AcousticPulseSponge2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_AcousticPulseSponge3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_Scalar_Advection2DX(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_Scalar_Advection2DY(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_Scalar_AdvectionDiffusion2DX(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_Scalar_AdvectionDiffusion2DY(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APRectilinearPeriodic2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APCurvilinearPeriodic2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APRectilinearFarfield2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APCurvilinearFarfield2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APRectilinearSlipwall2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APCurvilinearSlipwall2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APRectilinearSponge2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APCurvilinearSponge2D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APRectilinearPeriodic3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APRectilinearSlipwall3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APRectilinearFarfield3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APRectilinearSponge3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APCurvilinearPeriodic3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APCurvilinearSlipwall3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APCurvilinearFarfield3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_APCurvilinearSponge3D(ix::test::results &,pcpp::CommunicatorType &);
void TestPlasCom2_Restart(ix::test::results &,pcpp::CommunicatorType &);

void TestIntegrated_Scalar_Advection3DZ(ix::test::results &,pcpp::CommunicatorType &);

// Integrated WENO tests
void TestWENO_Convergence(ix::test::results &, pcpp::CommunicatorType &);

