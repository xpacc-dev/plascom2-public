#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#

include_directories(include)

FILE(GLOB LIB_CPP_SRCS "src/Test*.C")
#FILE(GLOB LIB_F90_SRCS "src/Test*.f90")

IF(NOT USE_HDF)
  FILE(GLOB HDF_TEST_FILES "src/*HDF5*")
  LIST(REMOVE_ITEM LIB_CPP_SRCS ${HDF_TEST_FILES})  
ENDIF()
IF(NOT ENABLE_OVERKIT)
  FILE(GLOB OVERKIT_TEST_FILES "src/*Overkit*")
  LIST(REMOVE_ITEM LIB_CPP_SRCS ${OVERKIT_TEST_FILES})
ENDIF()
  
SET(Testing_KernelSourcesCPP kernels/CSTestKernelsCXX.C)
SET(Testing_KernelSourcesFortran kernels/CSTestKernelsFortran.f90)

if(USE_ICE)
  # build a list of all the optimization files to be used
  set(tuningFileIn ${CMAKE_CURRENT_SOURCE_DIR}/ice/tuneTesting.opt)
  if(ICE_HAVE_ROSE)
    list(APPEND tuningFileIn ${CMAKE_CURRENT_SOURCE_DIR}/ice/tuneTestingRose.opt)
  endif()

  set(masterTuningFile ${CMAKE_CURRENT_BINARY_DIR}/ice/masterTuningFile.opt)
  file(WRITE ${masterTuningFile} "")
  foreach(tuningFile ${tuningFileIn})
    cat(${tuningFile} ${masterTuningFile})
  endforeach()
  #if(USE_MOYA)
    #set(tuningFileIn ${CMAKE_CURRENT_SOURCE_DIR}/ice/tuneHandMoya.yaml)
  #endif()
  set(tuningFile ${CMAKE_CURRENT_BINARY_DIR}/ice/tuningFile.opt)
  #configure_file(${tuningFileIn} ${tuningFile} @ONLY)
  configure_file(${masterTuningFile} ${tuningFile} @ONLY)

  # C++ LibSources
  foreach(fileIn ${LIB_CPP_SRCS})
    PreProcessICE(${fileIn} ${tuningFile} 
      ${CMAKE_CURRENT_BINARY_DIR}/ice/src fileOut "ice.C")
    list(APPEND LIB_CPP_SRCS-ICE ${fileOut})
  endforeach()
  set(LIB_CPP_SRCS ${LIB_CPP_SRCS-ICE})

  # KernelSources
  foreach(fileIn ${Testing_KernelSourcesFortran})
    PreProcessICE(${CMAKE_CURRENT_SOURCE_DIR}/${fileIn} ${tuningFile} 
      ${CMAKE_CURRENT_BINARY_DIR}/ice/kernels fileOut "ice.f90")
    list(APPEND Testing_KernelSourcesFortran-ICE ${fileOut})
  endforeach()
  set(Testing_KernelSourcesFortran ${Testing_KernelSourcesFortran-ICE})

  # KernelSources
  foreach(fileIn ${Testing_KernelSourcesCPP})
    PreProcessICE(${CMAKE_CURRENT_SOURCE_DIR}/${fileIn} ${tuningFile} 
      ${CMAKE_CURRENT_BINARY_DIR}/ice/kernels fileOut "ice.C")
      # doing something here to pass in library dependencies
      #${CMAKE_CURRENT_BINARY_DIR}/ice/kernels fileOut "ice.C" "TestingKernels")
    list(APPEND Testing_KernelSourcesCPP-ICE ${fileOut})
  endforeach()
  set(Testing_KernelSourcesCPP ${Testing_KernelSourcesCPP-ICE})

  ## Fortran LibSources
  #foreach(fileIn ${LIB_F90_SRCS})
    #PreProcessICE(${fileIn} ${tuningFile} 
      #${CMAKE_CURRENT_BINARY_DIR}/ice/src fileOut "ice.f90")
    #list(APPEND LIB_F90_SRCS-ICE ${fileOut})
  #endforeach()
  #set(LIB_F90_SRCS ${LIB_F90_SRCS-ICE})
endif()

set (LIB_SRCS ${LIB_CPP_SRCS} ${LIB_F90_SRCS})
set (ALL_SRCS ${LIB_SRCS} src/PC2TestDriver.C src/PC2ParallelTestDriver.C)


set_source_files_properties(${ALL_SRCS} PROPERTIES COMPILE_FLAGS "-fPIC" )
MESSAGE(STATUS "ALL TEST SOURCES: ${ALL_SRCS}")
MESSAGE(STATUS "ALL C++ KERNEL SOURCES: ${Testing_KernelSourcesCPP}")
MESSAGE(STATUS "ALL Fortran KERNEL SOURCES: ${Testing_KernelSourcesFortran}")
# rpath settings
IF(NOT BUILD_STATIC)
  SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
  SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
ENDIF()

IF(BUILD_STATIC)
  ADD_LIBRARY(PC2UnitTest STATIC ${LIB_SRCS})
  ADD_LIBRARY(TestingKernels STATIC ${Testing_KernelSourcesFortran} ${Testing_KernelSourcesCPP})
  set_target_properties(PC2UnitTest PROPERTIES LINK_SEARCH_START_STATIC 1)
  set_target_properties(PC2UnitTest PROPERTIES LINK_SEARCH_END_STATIC 1)
ELSE()
  ADD_LIBRARY(PC2UnitTest SHARED ${LIB_SRCS})
  ADD_LIBRARY(TestingKernels SHARED ${Testing_KernelSourcesFortran} ${Testing_KernelSourcesCPP})
ENDIF()
TARGET_LINK_LIBRARIES(PC2UnitTest PlasCom2 ${PCPP_LIB} ${IX_LIB} Kernels TestingKernels)

ADD_EXECUTABLE(plascom2_test src/PC2TestDriver.C)
TARGET_LINK_LIBRARIES(plascom2_test PC2UnitTest PlasCom2 ${PCPP_LIB} ${IX_LIB} Kernels)

ADD_DEFINITIONS(-D_PC2_PARALLEL_)

ADD_EXECUTABLE(plascom2_parallel_test src/PC2ParallelTestDriver.C)
TARGET_LINK_LIBRARIES(plascom2_parallel_test PC2UnitTest PlasCom2 ${PCPP_LIB} ${IX_LIB} Kernels)
#IF(MPI_LINK_FLAGS)
#  SET_TARGET_PROPERTIES(plascom2_parallel_test PROPERTIES LINK_FLAGS "${MPI_CXX_LINK_FLAGS}")
#ENDIF()


ADD_TEST(TestSetup ${RUNTEST} -f ${CMAKE_CURRENT_SOURCE_DIR}/Scripts/test_setup.sh -s ${CMAKE_CURRENT_SOURCE_DIR} -b ${CMAKE_BINARY_DIR}/bin -o setup.txt)

ADD_TEST(RunTestScripts ${RUNTEST} -l ${CMAKE_CURRENT_SOURCE_DIR}/Scripts/tests.list -s ${CMAKE_CURRENT_SOURCE_DIR} -b ${CMAKE_BINARY_DIR}/bin -v 2 -o plascom2_testresults.txt)
SET_PROPERTY(TEST RunTestScripts APPEND PROPERTY DEPENDS TestSetup)

ADD_TEST(RunSpecificTestScript    ${RUNTEST} -f ${CMAKE_CURRENT_SOURCE_DIR}/Platforms/test_stub.sh -s ${CMAKE_CURRENT_SOURCE_DIR} -b ${CMAKE_BINARY_DIR}/bin -o plascom2_testresults.txt)
SET_PROPERTY(TEST RunSpecificTestScript APPEND PROPERTY DEPENDS TestSetup)

ADD_TEST(PlasCom2:RunSerialPlatformTests ${RUNTEST} -p ${CMAKE_CURRENT_SOURCE_DIR}/Platforms/platforms -s ${CMAKE_CURRENT_SOURCE_DIR} -b ${CMAKE_BINARY_DIR}/bin -v 3 -o plascom2_testresults.txt)
SET_PROPERTY(TEST PlasCom2:RunSerialPlatformTests APPEND PROPERTY DEPENDS TestSetup)

ADD_TEST(PlasCom2:RunParallelPlatformTests ${RUNTEST} -p ${CMAKE_CURRENT_SOURCE_DIR}/Platforms/parallel_platforms -s ${CMAKE_CURRENT_SOURCE_DIR} -b ${CMAKE_BINARY_DIR}/bin -v 3 -o plascom2_testresults.txt)
SET_PROPERTY(TEST PlasCom2:RunParallelPlatformTests APPEND PROPERTY DEPENDS TestSetup)


ADD_TEST(PlasCom2:RunsInSerial ${TESTRESULT} PlasCom2:RunsInSerial plascom2_testresults.txt)
#ADD_TEST(PlasCom2:TestStubWorks ${TESTRESULT} PlascTestStubWorks plascom2_testresults.txt)
ADD_TEST(PlasCom2:RunsInParallel ${TESTRESULT} PlasCom2:RunsInParallel plascom2_testresults.txt)


ADD_TEST(Compare:Legacy:Same ${EXECUTABLE_OUTPUT_PATH}/pc2compare PlasComCM/hdf5/legacyCMFile.h5 PlasComCM/hdf5/legacyCMFile.h5)
ADD_TEST(Compare:Legacy:Different ${EXECUTABLE_OUTPUT_PATH}/pc2compare PlasComCM/hdf5/flocm2d_005.h5 InviscidTests/AcousticPulsePeriodic2D/CM/flocm_acousticpulse_1000.h5)
ADD_TEST(Compare:Legacy:Incompatible ${EXECUTABLE_OUTPUT_PATH}/pc2compare HDF5Examples/MultipleGrids2D.h5 HDF5Examples/MultipleGrids3D.h5)
ADD_TEST(Compare:PC2:Same ${EXECUTABLE_OUTPUT_PATH}/pc2compare HDF5Examples/pc2_2d_0.h5 HDF5Examples/pc2_2d_0.h5)
ADD_TEST(Compare:PC2:Different ${EXECUTABLE_OUTPUT_PATH}/pc2compare HDF5Examples/pc2_2d_1000.h5 HDF5Examples/pc2_2d_0.h5)
ADD_TEST(Compare:PC2:Incompatible ${EXECUTABLE_OUTPUT_PATH}/pc2compare HDF5Examples/MultipleGrids2D.h5 HDF5Examples/pc2_2d_1000.h5)
ADD_TEST(Compare:LegacyPC2:Same ${EXECUTABLE_OUTPUT_PATH}/pc2compare HDF5Examples/pc2_2d_1000.h5 InviscidTests/AcousticPulsePeriodic2D/CM/flocm_acousticpulse_1000.h5 1e-4)
ADD_TEST(Compare:LegacyPC2:Different ${EXECUTABLE_OUTPUT_PATH}/pc2compare HDF5Examples/pc2_2d_1000.h5 PlasComCM/hdf5/flocm_2d_005.h5)
ADD_TEST(Compare:LegacyPC2:Incompatible ${EXECUTABLE_OUTPUT_PATH}/pc2compare HDF5Examples/pc2_2d_1000.h5 InviscidTests/AcousticPulsePeriodic3D/CM/flocm_acousticpulse_5.h5)

SET_TESTS_PROPERTIES(Compare:Legacy:Different Compare:Legacy:Incompatible 
                     Compare:PC2:Different Compare:PC2:Incompatible 
                     Compare:LegacyPC2:Different Compare:LegacyPC2:Incompatible PROPERTIES WILL_FAIL true)

#ADD_TEST(ComparisonUtil:Legacy:Same ${EXECUTABLE_OUTPUT_PATH}/pc2compare legacyCMFile.h5 legacyCMFile.h5)
#ADD_TEST(ComparisonUtil:Legacy:Different ${EXECUTABLE_OUTPUT_PATH}/pc2compare legacyCMFile.h5 legacyCMFile.h5)
#ADD_TEST(ComparisonUtil:Different pc2compare legacyCMFile.h5 )

#
# Build the suites file that specifies which tests to run
# First read the defaults from the master list
#
SET(numSerialSuites 0)
message(STATUS "${Blue}Discovering serial unit suites ...${ColorReset}")
file(READ ${CMAKE_CURRENT_SOURCE_DIR}/Scripts/serialUnitSuites.txt serialTestSuites)
string(REGEX REPLACE "\n" " " serialTestSuiteList ${serialTestSuites})
#set(testSuiteList ${testSuites})
separate_arguments(serialTestSuiteList)

#
# Add optional suites that are enabled by various options
#
if(USE_HDF)
  list(APPEND serialTestSuiteList HDF5)
endif()

# ICE tests
if(USE_ICE)
  list(APPEND serialTestSuiteList ICE)
  # tests specific to modules enabled by ICE
  # Rose
  if(ICE_HAVE_ROSE)
    list(APPEND serialTestSuiteList ICE-Rose)
  endif()
  # Pips
  #if(ICE_HAVE_PIPS)
    #list(APPEND serialTestSuiteList ICE-Pips)
  #endif()
endif()

#
# write the list out to the file that is read by the test framework
#
set(serialSuiteFile ${CMAKE_CURRENT_BINARY_DIR}/serialUnitSuites)
file(WRITE ${serialSuiteFile} "")
foreach(suiteName ${serialTestSuiteList})
  message(STATUS "${Cyan}\t Running tests from suite ${suiteName}${ColorReset}")
  file(APPEND ${serialSuiteFile} "${suiteName} \n")
  MATH(EXPR numSerialSuites "${numSerialSuites} + 1") 
endforeach()

#
# discover and add serial unit tests
#
SET(numSerialTests 0)
message(STATUS "${Blue}Discovering serial unit tests ...${ColorReset}")
execute_process(COMMAND grep -I -r --exclude=CMakeLists.txt --exclude=*~ --exclude=\#* "serialUnitResults.UpdateResult" ${CMAKE_CURRENT_SOURCE_DIR}
                COMMAND grep -v "//"
                COMMAND cut -d "(" -f 2
                COMMAND cut -d "," -f 1
                OUTPUT_VARIABLE allSerialUnitTests
                )
#message(${allSerialUnitTests})
string(REPLACE "\"" "" allSerialUnitTests ${allSerialUnitTests})
string(REGEX REPLACE "\n" " " allSerialUnitTests ${allSerialUnitTests})
set(serialUnitTestList ${allSerialUnitTests})
separate_arguments(serialUnitTestList)
list(SORT serialUnitTestList)
list(REMOVE_DUPLICATES serialUnitTestList)

foreach(indTest ${serialUnitTestList})
  # only add tests from the active suites
  foreach(suiteName ${serialTestSuiteList})
    if("${indTest}" MATCHES "^${suiteName}:")
      if(SHOWTESTS) 
	message(STATUS "${Cyan}\t adding test ${indTest}${ColorReset}")
      endif()
      add_test(${indTest} ${TESTRESULT} ${indTest} plascom2_testresults.txt)
      MATH(EXPR numSerialTests "${numSerialTests} + 1")
    endif()
  endforeach()
endforeach()
MESSAGE(STATUS "${Blue}Found ${numSerialTests} tests in ${numSerialSuites} serial suites.${ColorReset}")

#
# parallel test suites
#
SET(numParallelSuites 0)
message(STATUS "${Blue}Discovering parallel unit testing suites ...${ColorReset}")
file(READ ${CMAKE_CURRENT_SOURCE_DIR}/Scripts/parallelUnitSuites.txt parallelTestSuites)
string(REGEX REPLACE "\n" " " parallelTestSuiteList ${parallelTestSuites})
#set(testSuiteList ${testSuites})
separate_arguments(parallelTestSuiteList)

#
# Add optional suites that are enabled by various options
#
if(USE_HDF)
  list(APPEND parallelTestSuiteList HDF5)
endif()
if(ENABLE_OVERKIT)
  list(APPEND parallelTestSuiteList Overkit)
endif()
if(INTEGRATED_TESTING)
  list(APPEND parallelTestSuiteList Integrated)
endif()
#
# write the list out to the file that is read by the test framework
#
set(parallelSuiteFile ${CMAKE_CURRENT_BINARY_DIR}/parallelUnitSuites)
file(WRITE ${parallelSuiteFile} "")
foreach(suiteName ${parallelTestSuiteList})
  message(STATUS "${Cyan}\t Running tests from suite ${suiteName}${ColorReset}")
  file(APPEND ${parallelSuiteFile} "${suiteName} \n")
  MATH(EXPR numParallelSuites "${numParallelSuites} + 1")
endforeach()

#
# discover and add parallel unit tests
#
SET(numParallelTests 0)
message(STATUS "${Blue}Discovering parallel unit tests ...${ColorReset}")
execute_process(COMMAND grep -I -r --exclude=CMakeLists.txt --exclude=*~ --exclude=\#* "parallelUnitResults.UpdateResult" ${CMAKE_CURRENT_SOURCE_DIR}
                COMMAND grep -v "//"
                COMMAND cut -d "(" -f 2
                COMMAND cut -d "," -f 1
                OUTPUT_VARIABLE allParallelUnitTests
                )
string(REPLACE "\"" "" allParallelUnitTests ${allParallelUnitTests})
string(REGEX REPLACE "\n" " " allParallelUnitTests ${allParallelUnitTests})
set(parallelUnitTestList ${allParallelUnitTests})
separate_arguments(parallelUnitTestList)
list(SORT parallelUnitTestList)
list(REMOVE_DUPLICATES parallelUnitTestList)

foreach(indTest ${parallelUnitTestList})
  # only add tests from the active suites
  foreach(suiteName ${parallelTestSuiteList})
    if("${indTest}" MATCHES "^${suiteName}:")
      if(SHOWTESTS) 
        message(STATUS "${Cyan}\t adding test ${indTest}${ColorReset}")
      endif()
      add_test(${indTest} ${TESTRESULT} ${indTest} plascom2_testresults.txt)
      MATH(EXPR numParallelTests "${numParallelTests} + 1")
    endif()
  endforeach()
endforeach()
MESSAGE(STATUS "${Blue}Found ${numParallelTests} tests in ${numParallelSuites} parallel suites.${ColorReset}")

#INCLUDE(parallel_unit_tests.cmake)
