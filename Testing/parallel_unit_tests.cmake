#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
ADD_TEST(Euler:RHS:RunsInParallel ${TESTRESULT} Euler:RHS:RunsInParallel plascom2_testresults.txt)
ADD_TEST(Grid:PBS:BufferSize ${TESTRESULT} Grid:PBS:BufferSize plascom2_testresults.txt)
ADD_TEST(Grid:PBS:CreateMessageBuffers ${TESTRESULT} Grid:PBS:CreateMessageBuffers plascom2_testresults.txt)
ADD_TEST(Grid:PBS:GridSizing ${TESTRESULT} Grid:PBS:GridSizing plascom2_testresults.txt)
ADD_TEST(Grid:PBS:HaloExtents ${TESTRESULT} Grid:PBS:HaloExtents plascom2_testresults.txt)
ADD_TEST(Grid:PBS:HaloRecvIndices ${TESTRESULT} Grid:PBS:HaloRecvIndices plascom2_testresults.txt)
ADD_TEST(Grid:PBS:HaloSendIndices ${TESTRESULT} Grid:PBS:HaloSendIndices plascom2_testresults.txt)
ADD_TEST(Grid:PBS:RecvHaloData ${TESTRESULT} Grid:PBS:RecvHaloData plascom2_testresults.txt)
ADD_TEST(halo:CompleteSimpleRecv ${TESTRESULT} halo:CompleteSimpleRecv plascom2_testresults.txt)
ADD_TEST(halo:CreateLLocalHalo ${TESTRESULT} halo:CreateLLocalHalo plascom2_testresults.txt)
ADD_TEST(halo:CreateLRemoteHalo ${TESTRESULT} halo:CreateLRemoteHalo plascom2_testresults.txt)
ADD_TEST(halo:CreateRLocalHalo ${TESTRESULT} halo:CreateRLocalHalo plascom2_testresults.txt)
ADD_TEST(halo:CreateRRemoteHalo ${TESTRESULT} halo:CreateRRemoteHalo plascom2_testresults.txt)
ADD_TEST(halo:LLocalHalo ${TESTRESULT} halo:LLocalHalo plascom2_testresults.txt)
ADD_TEST(halo:LRemoteHalo ${TESTRESULT} halo:LRemoteHalo plascom2_testresults.txt)
ADD_TEST(halo:PackPeriodicSends ${TESTRESULT} halo:PackPeriodicSends plascom2_testresults.txt)
ADD_TEST(halo:PeriodicCommunication ${TESTRESULT} halo:PeriodicCommunication plascom2_testresults.txt)
ADD_TEST(halo:PeriodicCompleteSimpleRecv ${TESTRESULT} halo:PeriodicCompleteSimpleRecv plascom2_testresults.txt)
ADD_TEST(halo:PeriodicLLocalHalo ${TESTRESULT} halo:PeriodicLLocalHalo plascom2_testresults.txt)
ADD_TEST(halo:PeriodicLRemoteHalo ${TESTRESULT} halo:PeriodicLRemoteHalo plascom2_testresults.txt)
ADD_TEST(halo:PeriodicRLocalHalo ${TESTRESULT} halo:PeriodicRLocalHalo plascom2_testresults.txt)
ADD_TEST(halo:PeriodicRRemoteHalo ${TESTRESULT} halo:PeriodicRRemoteHalo plascom2_testresults.txt)
ADD_TEST(halo:PeriodicSend ${TESTRESULT} halo:PeriodicSend plascom2_testresults.txt)
ADD_TEST(halo:PeriodicSendBuffers ${TESTRESULT} halo:PeriodicSendBuffers plascom2_testresults.txt)
ADD_TEST(halo:PeriodicUnpackSimpleRecv ${TESTRESULT} halo:PeriodicUnpackSimpleRecv plascom2_testresults.txt)
ADD_TEST(halo:RLocalHalo ${TESTRESULT} halo:RLocalHalo plascom2_testresults.txt)
ADD_TEST(halo:RRemoteHalo ${TESTRESULT} halo:RRemoteHalo plascom2_testresults.txt)
ADD_TEST(halo:SimpleSend ${TESTRESULT} halo:SimpleSend plascom2_testresults.txt)
ADD_TEST(halo:UnpackSimpleRcvBuffers ${TESTRESULT} halo:UnpackSimpleRcvBuffers plascom2_testresults.txt)
ADD_TEST(HDF5:Basic:testran ${TESTRESULT} HDF5:Basic:testran plascom2_testresults.txt)
ADD_TEST(HDF5:Interface:ReadGrid ${TESTRESULT} HDF5:Interface:ReadGrid plascom2_testresults.txt)
ADD_TEST(HDF5:Interface:ReadPBSGrid ${TESTRESULT} HDF5:Interface:ReadPBSGrid plascom2_testresults.txt)
ADD_TEST(HDF5:Interface:StateAttributeIO ${TESTRESULT} HDF5:Interface:StateAttributeIO plascom2_testresults.txt)
ADD_TEST(HDF5:Interface:StateMetaDataIO ${TESTRESULT} HDF5:Interface:StateMetaDataIO plascom2_testresults.txt)
ADD_TEST(HDF5:Interface:StateNodeDataIO ${TESTRESULT} HDF5:Interface:StateNodeDataIO plascom2_testresults.txt)
ADD_TEST(HDF5:Interface:WriteGrid ${TESTRESULT} HDF5:Interface:WriteGrid plascom2_testresults.txt)
ADD_TEST(HDF5:Interface:WritePBSGrid ${TESTRESULT} HDF5:Interface:WritePBSGrid plascom2_testresults.txt)
ADD_TEST(HDF5:Multi3DFileInfo ${TESTRESULT} HDF5:Multi3DFileInfo plascom2_testresults.txt)
ADD_TEST(HDF5:Object:AttributeDimensions ${TESTRESULT} HDF5:Object:AttributeDimensions plascom2_testresults.txt)
ADD_TEST(HDF5:Object:AttributeExists ${TESTRESULT} HDF5:Object:AttributeExists plascom2_testresults.txt)
ADD_TEST(HDF5:Object:ConfigIO ${TESTRESULT} HDF5:Object:ConfigIO plascom2_testresults.txt)
ADD_TEST(HDF5:Object:CreateAttribute ${TESTRESULT} HDF5:Object:CreateAttribute plascom2_testresults.txt)
ADD_TEST(HDF5:Object:CreateData ${TESTRESULT} HDF5:Object:CreateData plascom2_testresults.txt)
ADD_TEST(HDF5:Object:CreateGroup ${TESTRESULT} HDF5:Object:CreateGroup plascom2_testresults.txt)
ADD_TEST(HDF5:Object:DataDimensions ${TESTRESULT} HDF5:Object:DataDimensions plascom2_testresults.txt)
ADD_TEST(HDF5:Object:DataDims ${TESTRESULT} HDF5:Object:DataDims plascom2_testresults.txt)
ADD_TEST(HDF5:Object:GlobalExistence ${TESTRESULT} HDF5:Object:GlobalExistence plascom2_testresults.txt)
ADD_TEST(HDF5:Object:ParallelOpen ${TESTRESULT} HDF5:Object:ParallelOpen plascom2_testresults.txt)
ADD_TEST(HDF5:Object:ReadAttribute ${TESTRESULT} HDF5:Object:ReadAttribute plascom2_testresults.txt)
ADD_TEST(HDF5:Object:ReadAttVector ${TESTRESULT} HDF5:Object:ReadAttVector plascom2_testresults.txt)
ADD_TEST(HDF5:Object:ReadDataVector ${TESTRESULT} HDF5:Object:ReadDataVector plascom2_testresults.txt)
ADD_TEST(HDF5:Object:ReadHyperSlab ${TESTRESULT} HDF5:Object:ReadHyperSlab plascom2_testresults.txt)
ADD_TEST(HDF5:Object:ReadStringAttribute ${TESTRESULT} HDF5:Object:ReadStringAttribute plascom2_testresults.txt)
ADD_TEST(HDF5:Object:ScopeExistence ${TESTRESULT} HDF5:Object:ScopeExistence plascom2_testresults.txt)
ADD_TEST(HDF5:Object:SerialOpen ${TESTRESULT} HDF5:Object:SerialOpen plascom2_testresults.txt)
ADD_TEST(HDF5:Object:SerialOpenGroup ${TESTRESULT} HDF5:Object:SerialOpenGroup plascom2_testresults.txt)
ADD_TEST(HDF5:Object:WriteHyperSlab ${TESTRESULT} HDF5:Object:WriteHyperSlab plascom2_testresults.txt)
ADD_TEST(HDF5:Parallel2DFileInfo ${TESTRESULT} HDF5:Parallel2DFileInfo plascom2_testresults.txt)
ADD_TEST(HDF5:Parallel3DFileInfo ${TESTRESULT} HDF5:Parallel3DFileInfo plascom2_testresults.txt)
ADD_TEST(HDF5:ParMulti3DFileInfo ${TESTRESULT} HDF5:ParMulti3DFileInfo plascom2_testresults.txt)
ADD_TEST(HDF5:Serial2DFileInfo ${TESTRESULT} HDF5:Serial2DFileInfo plascom2_testresults.txt)
ADD_TEST(HDF5:Serial3DFileInfo ${TESTRESULT} HDF5:Serial3DFileInfo plascom2_testresults.txt)
ADD_TEST(PlasCom2:Output ${TESTRESULT} PlasCom2:Output plascom2_testresults.txt)
