#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/sh

RESULTSFILE=${1}
SRCDIR=${2}
BINDIR=${3}

source ${SRCDIR}/Scripts/platforms.sh

suiteFile=parallelUnitSuites
if [ ! -e ${suiteFile} ]; then
  suiteFile=${SRCDIR}/Scripts/parallelUnitSuites.txt
fi

for parallelUnitSuite in `cat ${suiteFile}`; do
  printf "srun -n 4 -ppdebug -t 5 ${BINDIR}/plascom2_parallel_test -n ${parallelUnitSuite} -o ${RESULTSFILE}\n"
  srun -n 4 -ppdebug -t 5 ${BINDIR}/plascom2_parallel_test -n ${parallelUnitSuite} -o ${RESULTSFILE}
done

exit 0
