#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/sh

RESULTSFILE=${1}
SRCDIR=${2}
BINDIR=${3}

source ${SRCDIR}/Scripts/titan.sh

for parallelUnitSuite in `cat ${SRCDIR}/Scripts/parallelunitsuites.txt`; do
  rm -f ${parallelUnitSuite}_batch.sh
  printf "#!/bin/sh\n" > ${parallelUnitSuite}_batch.sh
  printf "cd \${PBS_O_WORKDIR}\n" >> ${parallelUnitSuite}_batch.sh
  printf "${RUNCOMMAND} -n4 ${BINDIR}/plascom2_parallel_test -n ${parallelUnitSuite} -o ${RESULTSFILE}\n" >> ${parallelUnitSuite}_batch.sh
  chmod +x ./${parallelUnitSuite}_batch.sh
  ${BATCHCOMMAND} ./${parallelUnitSuite}_batch.sh
done
exit 0
