#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/sh

outFile=${1}
srcDir=${2} 
binDir=${3}

source ${srcDir}/Scripts/platforms.sh

suiteFile=serialUnitSuites
if [ ! -e ${suiteFile} ]; then
  suiteFile=${SRCDIR}/Scripts/serialUnitSuites.txt
fi

for serialUnitSuite in `cat ${suiteFile}`; do
  ${binDir}/plascom2_test -n ${serialUnitSuite} -o ${outFile}
done



