#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#!/bin/sh

RESULTSFILE=${1}
SRCDIR=${2}
BINDIR=${3}

source ${SRCDIR}/Scripts/platforms.sh

suiteFile=parallelUnitSuites
if [ ! -e ${suiteFile} ]; then
  suiteFile=${SRCDIR}/Scripts/parallelUnitSuites.txt
fi

for parallelUnitSuite in `cat ${suiteFile}`; do
  printf "mpirun -n 4 ${BINDIR}/plascom2_parallel_test -n ${parallelUnitSuite} -o ${RESULTSFILE}\n"
  mpirun -n 4 ${BINDIR}/plascom2_parallel_test -n ${parallelUnitSuite} -o ${RESULTSFILE}
done

#rm -f tmpresults_1.txt
#mpiexec -n 4 ${BINDIR}/plascom2_parallel_test -o tmpresults_1.txt
#
#@ i = 1
#while($i <= 8)
    #@ i += 1
    #if( -e tmpresults_1.txt ) then
        #@ i += 8;
    #else
        #sleep 30;
    #endif
#end
#
#if( -e tmpresults_1.txt ) then
  #cat tmpresults_1.txt >> ${RESULTSFILE}
#else
  #exit 1
#endif

exit 0


