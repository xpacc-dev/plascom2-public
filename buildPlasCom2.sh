#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
#! /bin/bash
#
# CS-Tool build system wrapper
#
# This script is intended to wrap a projects build system and provide
# a way of detecting any specific compilers that should be used. This 
# helps with build systems like CMake where, in general, the compiler
# should not be changed during configuration.
#
# This wrapper takes as input a list of arguments that should be passed 
# to the native projects build system. Currently only CMake is supported
# These arguments may be augmented
# to contain information about a given compiler. This determination is 
# made by parsing the CMake-based build system that accompanies the CS-Tool
# framework.
#
# The script should be executed from the intended directory (seperate from the source)
#
# Usage:
# ./buildPlasCom2.sh <options> <native build system options>
#
# The following options are available:
#   -h, print this help
#   -H, print a list of the project options provided by the underlying build system
#   -D, CMake specific build system options
#   --testColors
#   --testConfigure
#
# Example:
#   ./buildPlasCom2.sh -DUSE_ICE=ON -DCMAKE_PREFIX_PATH=<path to ICE>
#

# setup colors
NC='\033[0m' # No Color

BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
WHITE='\033[0;37m'

BOLDBLACK='\033[1;30m'
BOLDRED='\033[1;31m'
BOLDGREEN='\033[1;32m'
BOLDORANGE='\033[1;33m'
BOLDBLUE='\033[1;34m'
BOLDPURPLE='\033[1;35m'
BOLDCYAN='\033[1;36m'
BOLDWHITE='\033[1;37m'

UNDERBLACK='\033[4;30m'
UNDERRED='\033[4;31m'
UNDERGREEN='\033[4;32m'
UNDERORANGE='\033[4;33m'
UNDERBLUE='\033[4;34m'
UNDERPURPLE='\033[4;35m'
UNDERCYAN='\033[4;36m'
UNDERWHITE='\033[4;37m'

FLASHBLACK='\033[5;30m'
FLASHRED='\033[5;31m'
FLASHGREEN='\033[5;32m'
FLASHORANGE='\033[5;33m'
FLASHBLUE='\033[5;34m'
FLASHPURPLE='\033[5;35m'
FLASHCYAN='\033[5;36m'
FLASHWHITE='\033[5;37m'

# color test
function testColors() {
  echo "Colors 0;30m-0;37m"
  printf "${BLACK}BLACK${NC}\n"
  printf "${RED}RED${NC}\n"
  printf "${GREEN}GREEN${NC}\n"
  printf "${ORANGE}ORANGE${NC}\n"
  printf "${BLUE}BLUE${NC}\n"
  printf "${PURPLE}PURPLE${NC}\n"
  printf "${CYAN}CYAN${NC}\n"
  printf "${LIGHTGREY}LIGHTGREY${NC}\n"
  printf "${ZEBRA}ZEBRA${NC}\n\n"
  
  echo "Colors 1;30m-1;37m"
  printf "${BOLDBLACK}BOLDBLACK${NC}\n"
  printf "${BOLDRED}BOLDRED${NC}\n"
  printf "${BOLDGREEN}BOLDGREEN${NC}\n"
  printf "${BOLDORANGE}BOLDORANGE${NC}\n"
  printf "${BOLDBLUE}BOLDBLUE${NC}\n"
  printf "${BOLDPURPLE}BOLDPURPLE${NC}\n"
  printf "${BOLDCYAN}BOLDCYAN${NC}\n"
  printf "${BOLDWHITE}BOLDWHITE${NC}\n\n"
  
  echo "Colors 4;30m-4;37m"
  printf "${UNDERBLACK}UNDERBLACK${NC}\n"
  printf "${UNDERRED}UNDERRED${NC}\n"
  printf "${UNDERGREEN}UNDERGREEN${NC}\n"
  printf "${UNDERORANGE}UNDERORANGE${NC}\n"
  printf "${UNDERBLUE}UNDERBLUE${NC}\n"
  printf "${UNDERPURPLE}UNDERPURPLE${NC}\n"
  printf "${UNDERCYAN}UNDERCYAN${NC}\n"
  printf "${UNDERWHITE}UNDERWHITE${NC}\n\n"
  
  echo "Colors 5;30m-5;37m"
  printf "${FLASHBLACK}FLASHBLACK${NC}\n"
  printf "${FLASHRED}FLASHRED${NC}\n"
  printf "${FLASHGREEN}FLASHGREEN${NC}\n"
  printf "${FLASHORANGE}FLASHORANGE${NC}\n"
  printf "${FLASHBLUE}FLASHBLUE${NC}\n"
  printf "${FLASHPURPLE}FLASHPURPLE${NC}\n"
  printf "${FLASHCYAN}FLASHCYAN${NC}\n"
  printf "${FLASHWHITE}FLASHWHITE${NC}\n\n"
}
  
#
# Display build system options
#
function getBuildOptions() {
  # add check that there is a $1!
  topDir=$1
  # start with top level directory
  oldIFS=$IFS
  IFS=$'\n'
  allLines=($(grep -iw option $topDir/CMakeLists.txt))
  IFS=$oldIFS

  index=0
  declare -a optionArray
  declare -a descriptionArray
  declare -a defaultArray

  for line in "${allLines[@]}"; do
    #skip any lines that are commented
    if [[ ${line:0:1} != "#" ]]; then
      # remove option and trailing ), CMake is case-insentive...try to support this
      optionString=`echo $line | grep -io "option("`
      line=${line#$optionString}
      line=${line%)}

      # The description will have "" around it, use this to our advantage
      # character index in the string for the first "
      prefix=${line%%\"*}
      startDesc=$((${#prefix}+1))

      # string containing everything after the last " 
      # we will reuse this to check the default value
      endDesc=${line##*\"}
      
      # get just the description
      totalLength=${#line}
      endLength=${#endDesc}
      descLength=$((totalLength - startDesc - endLength - 1))
      description=${line:$startDesc:$descLength}

      # the name of the option is everything before the description
      optionEnd=$((startDesc - 1))
      option=${line:0:$optionEnd}

      # the default value is set after the description, if it's missing, then it's OFF
      default="OFF"
      if [[ $endDesc == *"ON"* ]]; then
        default="ON"
      fi

      # save back to arrays so we can write the whole table at once with nice formatting
      optionArray[$index]=${option}
      descriptionArray[$index]=${description}
      defaultArray[$index]=${default}
      ((index++))
    fi
  done


  # create a table of all the options
  # changing this will mess up the table formatting
  maxDescLen=40
  echo "|-----------------------|-------------------------------------------|------------|"
  printf "| ${GREEN}%20s ${WHITE} | ${CYAN}%-${maxDescLen}s ${WHITE} | ${ORANGE}%10s${NC} |\n" \
    "       OPTION       " \
    "             DESCRIPTION             " \
    " DEFAULT  "
  echo "|-----------------------|-------------------------------------------|------------|"

  while [ "$index" -ne "0" ]; do
    ((index--))
    # check to see if the description needs multiple lines
    unset descRecombined
    if [ ${#descriptionArray[${index}]} -gt "${maxDescLen}" ]; then
      # break the description into a series of space delimited strings
      # concatenate together until we reach the limit
      descSplit=(${descriptionArray[${index}]})
      index2=0
      for string in "${descSplit[@]}"; do
        currentLength=${#descRecombined[$index2]}
        nextLength=${#string}
        nextLength=$((currentLength+nextLength))

        if [ ${nextLength} -gt ${maxDescLen} ]; then
          ((index2++))
        fi
        descRecombined[${index2}]+="${string} "
      done
    else
      descRecombined[0]="${descriptionArray[${index}]}"
    fi

    # loop over the number of description lines and print the table
    first=1
    for description in "${descRecombined[@]}"; do
      if [ ${first} -eq "1" ]; then
        option="${optionArray[${index}]}"
        default="${defaultArray[${index}]}"
      else
        option=""
        default=""
      fi

      printf "| ${GREEN}%-20s ${WHITE} | ${CYAN}%-${maxDescLen}s ${WHITE} | ${ORANGE}%-10s${NC} |\n" \
        "$option" \
        "$description" \
        "$default"
      first=0
    done
    echo "|-----------------------|-------------------------------------------|------------|"
  done
  
}

# get origin location of the build script, this is assumed to be the top of the source tree
project="PlasCom2"
#script=$(readlink -f "$0")

savDir=`pwd`
TARGET_FILE=$0

cd `dirname $TARGET_FILE`
TARGET_FILE=`basename $TARGET_FILE`

# Iterate down a (possible) chain of symlinks
while [ -L "$TARGET_FILE" ]
do
    TARGET_FILE=`readlink $TARGET_FILE`
    cd `dirname $TARGET_FILE`
    TARGET_FILE=`basename $TARGET_FILE`
done

# Compute the canonicalized name by finding the physical path 
# for the directory we're in and appending the target file.
PHYS_DIR=`pwd -P`
script=$PHYS_DIR/$TARGET_FILE
cd $savDir

scriptPath=$(dirname "$script")
echo $scriptPath
CSToolPath="$scriptPath/CS-Tools"

# initialization of some options that can be set from the command line
cmakeOpts=''
testConfigure=0


# add some stuff here to do command line argument parsing, I'm thinking
# about adding some easy to use support so the user can easily detect
# potential options that can be used with the build system
# we can use our knowledge of how CMake is setup to build a list

while getopts ":thHD:-:" option; do
  case "${option}" in
    -)  # special case that handles long options with a double dash
        case "${OPTARG}" in
          testColors)
          
            #val="${!OPTIND}"; OPTIND=$(( $OPTIND+1 ))
            #echo "Parsing option: '--${OPTARG}', value: '${val}'" >&2;   
            echo "Running terminal color test"
            testColors
            ;;
          testConfigure)
            #val="${!OPTIND}"; OPTIND=$(( $OPTIND+1 ))
            #echo "Parsing option: '--${OPTARG}', value: '${val}'" >&2;   
            echo "Testing configuration only"
            testConfigure=1
            ;;
          *) echo "Invalid option: --${OPTARG}" >&2
              echo "Try -h for usage information"
              exit 1;;
        esac;;
    h)  echo "Printing help for buildPlasCom2.sh"
        head -n 31 $script | tail -n 30
        exit 0;;
    H)  echo ""
        echo "****************************************************************"
        echo "* Printing build options supplied by the $project build system *"
        echo "****************************************************************"
        echo ""
        getBuildOptions $scriptPath
        echo ""
        echo "****************************************************************"
        echo "* Printing build options supplied by the CS-Tools build system *"
        echo "****************************************************************"
        echo ""
        getBuildOptions $CSToolPath
        exit 0;;
    D)  #anything with -D is a cmake argument and gets stored
        cmakeOpts="${cmakeOpts} -D${OPTARG}"
        ;;
    \?) echo "Invalid option: -${OPTARG}" >&2
        echo "Try -h for usage information"
        exit 1;;
    :)  echo "Option -${OPTARG} requires an argument"
        echo "Try -h for usage information"
        exit 2;;
  esac
done
shift $((OPTIND-1))

printf "${BLUE}Configuring ${project} from $scriptPath${NC}\n"

# now parse the argument list
argList="$*"

#
# Call the CS-Tools build system to detect any required compiler wrappers
# These will be passed in the actual call to the build system
#
# fill this list with the options used to turn on CS-Tools that utilize a 
# compiler wrapper
checkCompiler=0
declare -a wrapperList=("USE_MOYA" "USE_AMPI")
for i in "${wrapperList[@]}"
do
  if [[ $cmakeOpts == *"${i}"* ]]; then
    checkCompiler=$((checkCompiler+1))
  fi
done

if [ "${checkCompiler}" -gt 0 ]; then
  printf "${BLUE}Looking for associated compiler wrappers${NC}\n" 
  tmpBuildDir=.CS-ToolsBuildTemp

  if [ -e $tmpBuildDir ]; then
    rm $tmpBuildDir/CMakeCache.txt
  else
    mkdir $tmpBuildDir
  fi

  cd $tmpBuildDir
  cmake $CSToolPath $cmakeOpts -DFindCompiler=ON > cmake.out 2>&1

  # check Moya
  # we can avoid using the USE_MOYA/USE_AMPI twice if we use checkCompiler as a bit-mask
  if [[ $cmakeOpts == *"USE_MOYA=ON"* ]]; then
    printf "${ORANGE}Looking for Moya${NC}\n"
    #make sure CMake found Moya
    grep "Found Moya" cmake.out > /dev/null
    foundMoya=$?
    if [ $foundMoya -eq "0" ]; then
      printf "${CYAN}Found Moya${NC}\n"
      moyaCC=`grep "MOYA_CC" cmake.out | cut -d " " -f 2`
      moyaCXX=`grep "MOYA_CXX" cmake.out | cut -d " " -f 2`
      moyaFC=`grep "MOYA_FC" cmake.out | cut -d " " -f 2`
      moyaAR=`grep "MOYA_AR" cmake.out | cut -d " " -f 2`
      printf "${GREEN} Moya C compiler \t ${moyaCC}${NC}\n"
      printf "${GREEN} Moya C++ compiler \t ${moyaCXX}${NC}\n"
      printf "${GREEN} Moya Fortran compiler \t ${moyaFC}${NC}\n"
      printf "${GREEN} Moya AR \t\t ${moyaAR}${NC}\n"
      cmakeOpts+=" -DCMAKE_C_COMPILER=${moyaCC}"
      cmakeOpts+=" -DCMAKE_CXX_COMPILER=${moyaCXX}"
      cmakeOpts+=" -DCMAKE_Fortran_COMPILER=${moyaFC}"
      cmakeOpts+=" -DCMAKE_AR=${moyaAR}"
    else
      printf "${RED}CS-Tools could not find Moya. Check options and resubmit. Aborting${NC}\n"
      exit 2
    fi
  elif [[ $cmakeOpts == *"USE_AMPI=ON"* ]]; then
    printf "${ORANGE}Looking for AMPI${NC}\n"
    #make sure CMake found Moya
    grep "Found AMPI" cmake.out > /dev/null
    foundAMPI=$?
    if [ $foundAMPI -eq "0" ]; then
      echo "not yet supported"
    fi
  fi
  # move back to top level
  cd ..
fi


# execute the main cmake configuration
printf "${BLUE}Configuring ${project}${NC}\n" 
printf "cmake ${scriptPath} ${cmakeOpts}"
if [ ${testConfigure} -eq "0" ]; then
  #cmake ${scriptPath} ${cmakeOpts} |& tee cmake.out
  cmake ${scriptPath} ${cmakeOpts} 
fi

exit 0
