#!/usr/bin/env python


# TODO bug : parsing args seems to take the first arg and just use everything else as its parameter, resulting in bad c compiler / bad path for hdf5
import os, sys, subprocess
import getopt

def printc(m, c):
    print(c + m + NC)

def color(m, c):
    return c + m + NC


def print_help():
    print("Printing help for buildPlasCom2.sh")
    print("\n \
CS-Tool build system wrapper\n \
\n\
 This script is intended to wrap a projects build system and provide\n \
a way of detecting any specific compilers that should be used. This\n \
helps with build systems like CMake where, in general, the compiler\n \
should not be changed during configuration.\n \
\n\
 This wrapper takes as input a list of arguments that should be passed\n \
to the native projects build system. Currently only CMake is supported\n \
These arguments may be augmented\n \
to contain information about a given compiler. This determination is\n \
made by parsing the CMake-based build system that accompanies the CS-Tool\n \
framework.\n \
\n\
 The script should be executed from the intended directory (seperate from the source)\n \
\n\
 Usage:\n \
./buildPlasCom2.sh <options> <native build system options>\n \
\n\
 The following options are available:\n \
  -h, print this help\n \
  -H, print a list of the project options provided by the underlying build system\n \
  -D, CMake specific build system options\n \
  --testColors\n \
  --testConfigure\n \
\n\
Example:\n \
  ./buildPlasCom2.sh -DUSE_ICE=ON -DCMAKE_PREFIX_PATH=<path to ICE>\n \
")


# simply checks if a program_name exists, returns None otherwise
def which(program_name):
    def is_exec(path):
        return os.path.isfile(path) and os.access(path, os.X_OK)

    fpath, fname = os.path.split(program_name)
    if fpath:
        if is_exec(fpath):
            return program_name
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exec_file = os.path.join(path, program_name)
            if is_exec(exec_file):
                return exec_file
    return None


# setup colors
NC = '\033[0m'  # No Color

BLACK = '\033[0;30m'
RED = '\033[0;31m'
GREEN = '\033[0;32m'
ORANGE = '\033[0;33m'
BLUE = '\033[0;34m'
PURPLE = '\033[0;35m'
CYAN = '\033[0;36m'
WHITE = '\033[0;37m'

BOLDBLACK = '\033[1;30m'
BOLDRED = '\033[1;31m'
BOLDGREEN = '\033[1;32m'
BOLDORANGE = '\033[1;33m'
BOLDBLUE = '\033[1;34m'
BOLDPURPLE = '\033[1;35m'
BOLDCYAN = '\033[1;36m'
BOLDWHITE = '\033[1;37m'

UNDERBLACK = '\033[4;30m'
UNDERRED = '\033[4;31m'
UNDERGREEN = '\033[4;32m'
UNDERORANGE = '\033[4;33m'
UNDERBLUE = '\033[4;34m'
UNDERPURPLE = '\033[4;35m'
UNDERCYAN = '\033[4;36m'
UNDERWHITE = '\033[4;37m'

FLASHBLACK = '\033[5;30m'
FLASHRED = '\033[5;31m'
FLASHGREEN = '\033[5;32m'
FLASHORANGE = '\033[5;33m'
FLASHBLUE = '\033[5;34m'
FLASHPURPLE = '\033[5;35m'
FLASHCYAN = '\033[5;36m'
FLASHWHITE = '\033[5;37m'


# color test

def testColors():
    print("Colors 0;30m-0;37m")
    printc("BLACK", BLACK)
    printc("RED", RED)
    printc("GREEN", GREEN)
    printc("ORANGE", ORANGE)
    printc("BLUE", BLUE)
    printc("PURPLE", PURPLE)
    printc("CYAN", CYAN)
    printc("WHITE", WHITE)
    print("\n")

    print("Colors 1;30m-1;37m")
    printc("BOLDBLACK", BOLDBLACK)
    printc("BOLDRED", BOLDRED)
    printc("BOLDGREEN", BOLDGREEN)
    printc("BOLDORANGE", BOLDORANGE)
    printc("BOLDBLUE", BOLDBLUE)
    printc("BOLDPURPLE", BOLDPURPLE)
    printc("BOLDCYAN", BOLDCYAN)
    printc("BOLDWHITE", BOLDWHITE)
    print("\n")

    print("Colors 4;30m-4;37m")
    printc("UNDERBLACK", UNDERBLACK)
    printc("UNDERRED", UNDERRED)
    printc("UNDERGREEN", UNDERGREEN)
    printc("UNDERORANGE", UNDERORANGE)
    printc("UNDERBLUE", UNDERBLUE)
    printc("UNDERPURPLE", UNDERPURPLE)
    printc("UNDERCYAN", UNDERCYAN)
    printc("UNDERWHITE", UNDERWHITE)
    print("\n")

    print("Colors 5;30m-5;37m")
    printc("FLASHBLACK", FLASHBLACK)
    printc("FLASHRED", FLASHRED)
    printc("FLASHGREEN", FLASHGREEN)
    printc("FLASHORANGE", FLASHORANGE)
    printc("FLASHBLUE", FLASHBLUE)
    printc("FLASHPURPLE", FLASHPURPLE)
    printc("FLASHCYAN", FLASHCYAN)
    printc("FLASHWHITE", FLASHWHITE)
    print("\n")


def getBuildOptionLists(path):
    # TODO more in depth error handling?
    if not os.path.isdir(os.path.abspath(path)):
        printc("Failed due to invalid path for build options", RED)
        exit(1)
    topDir = path
    # IFS is probably not relevant here.
    ''' oldIFS = os.getenv("IFS", None)
    if oldIFS is None:
        print("No former IFS")
    os.putenv("IFS", '\n')
    '''
    if which("grep") is None or which("cmake") is None:  # not sure if this can even happen
        printc("grep or cmake not found, aborting.", RED)
        exit(1)
    if not os.path.exists(topDir + "CMakeLists.txt"):
	printc("There isn't a CMakeLists.txt present in " + path + ", aborting", RED)
	exit(1)
    grep_proc = subprocess.Popen(["grep", "-iw", "option", topDir + "CMakeLists.txt"],
                                 stdout=subprocess.PIPE)  # be more careful here with grep / output
    grep_proc.wait()
    allLines = list(
        map(lambda s: s.decode("UTF-8"), grep_proc.stdout))  # Probably doesn't matter too much what encoding is
    '''
    if oldIFS:
        os.putenv("IFS", oldIFS)
    '''

    optionArray = []
    descriptionArray = []
    defaultArray = []
    # didn't follow Mike's script here, since python can do this in fewer lines
    for line in allLines:
        if line[0] != '#':
            line = line[len("option") + 1:line.rfind(")")].rstrip().split()  # non option
            # First one is always the option, last (if available) is default, rest is desc.
            optionArray.append(line[0])
            lastIndex = len(line) - 1
            if line[-1] == "ON" or line[-1] == "OFF":
                defaultArray.append(line[-1])
            else:
                defaultArray.append("OFF")
                lastIndex += 1
            descriptionArray.append(" ".join([word for word in line[1:lastIndex]]))
    return optionArray, defaultArray, descriptionArray

# prints formatted table of Options, Descriptions and Default settings for given build system
# for each list in optupl, the first element is the name of the build system
# can take any number of lists

#TODO word wrap on description columns for build script
#TODO max total table length 80, hard 100 char max.
def printBuildOptions(optupl, defaulttupl, desctupl):
    max_table_length = 80

    longest_option_len = max([len(max(op_list, key=len)) for op_list in optupl]) + 2

    longest_default_len = max(max([len(max(default_list, key=len)) for default_list in defaulttupl]), 10)

    # now irrelevant, doing line wraps. longest_description_len = max([len(max(desc_list, key=len)) for desc_list in desctupl]) + 2

    table_width = 80 #(longest_option_len + longest_default_len + longest_description_len + 4)
    # use greedy algorithm as follows: calculate prefix sum list of word lengths for each word in the description on each iteration through. fit max words under remainder of characters allowed (calculated by max_table_length - allocated option name + default option string lengths + spaces + formatting stuff, and put remainder of words on next lines w/ same strategy
    for option_list, default_list, desc_list in zip(optupl, defaulttupl, desctupl):
        print("*" * table_width)
        build_string = "Printing build options supplied by the {} build system".format(option_list[0])

	# Table adjusting for odd/even string lengths

	option_default_header_len = 1 + longest_option_len + 1 + 1 + longest_default_len + 1

	description_remainder = table_width - option_default_header_len # space padding
	extra_option_bar = 1
	if longest_option_len % 2 == 0:
            extra_option_bar = 0
        
        extra_desc_space = 0
	if description_remainder % 2 == 0:
            extra_desc_space = 1	

	build_string_header_len = table_width - (1 + int((table_width - len(build_string))/2) + len(build_string)) 
        print("*" + " " * int((table_width - len(build_string))/2) + build_string + " " * (build_string_header_len - 1) + "*")
        print("*" * table_width)
        extra_space = " "

        print("|" + (longest_option_len * "-") + "|" + (description_remainder * "-") + "|" + (longest_default_len * "-") + "|")

        print("|" + " " * int((longest_option_len - len("OPTION"))/2) + color("OPTION", GREEN) + " " * int((longest_option_len - len("OPTION"))/2 + extra_option_bar) + "|" + 
              " " * int((description_remainder - len("DESCRIPTION"))/2) + color("DESCRIPTION", CYAN) + " " * int((description_remainder - len("DESCRIPTION"))/2) + (extra_desc_space * " ") + "|" + 
              " " * int((longest_default_len - len("DEFAULT"))/2) + color("DEFAULT", ORANGE) + " " * int((longest_default_len - len("DEFAULT"))/2) + " |")
        del option_list[0] # project name
        for option, description, default in zip(option_list, desc_list, default_list):
            desc_words = description.split()
            wrap_idxs = [-1]
            wrap_enable = False
            curr_len = 0
	    for i, desc in enumerate(desc_words):
                curr_len += len(desc) + 1
                if curr_len + 2 >= description_remainder:
                    curr_len = 0
                    wrap_enable = True
                    wrap_idxs.append(i)
                
            if wrap_enable:
                description = ' '.join([word for word in desc_words[:wrap_idxs[1]]])
            print("|" + "-" * longest_option_len + "|" + "-" * description_remainder + "|" + "-" * longest_default_len + "|")
            print("| " + color(option, GREEN) + (longest_option_len - len(option) - 1) * " " + "|" + 
                  " " + color(description, CYAN) + " " * (description_remainder - len(description) - 2) + " |" + 
                  " " + color(default, ORANGE) + " " * (longest_default_len - len(default) - 2) + " |")
            if wrap_enable:
                for i, idx in enumerate(wrap_idxs[1:]):
                    wrapped_text = " ".join(desc_words[wrap_idxs[i-1]:])
                    end_padding = 0
		    #print(i, wrap_idxs)
                    if i == len(wrap_idxs) - 2:
                        end_padding = description_remainder - len(wrapped_text) - 1
                    print("| " + " " * (longest_option_len - 1) + "| " + color(wrapped_text, CYAN) + (" " * end_padding) + "| " + " " * (longest_default_len - 2) + " |")
			# TODO test with 3 line wrap, this might break
        print("|" + (longest_option_len * "-") + "|" + (description_remainder * "-") + "|" + (longest_default_len * "-") + "|")

        print("\n")

project = "PlasCom2"


savDir = os.getcwd()  # There isn't a -P flag here, but I think its the same
TARGET_FILE = sys.argv[0]

if os.path.dirname(TARGET_FILE) != '':  # If we're in the directory its in, then it returns ''.
    os.chdir(os.path.dirname(TARGET_FILE))
TARGET_FILE = os.path.basename(TARGET_FILE)

while (os.path.islink(TARGET_FILE)):
    TARGET_FILE = os.readlink(TARGET_FILE)
    os.chdir(os.path.dirname(TARGET_FILE))
    TARGET_FILE = os.path.basename(TARGET_FILE)

PHYS_DIR = os.getcwd()
script = PHYS_DIR + "/" + TARGET_FILE
os.chdir(savDir)

scriptPath = os.path.dirname(script) + "/"
CSToolPath = scriptPath + "CS-Tools/"



cmakeOpts = []
testConfigure = 0

try:
    opts, args = getopt.getopt(sys.argv[1:], ":thHD:-:", ["testColors", "testConfigure="])
except getopt.GetoptError as err:
    printc("Invalid option error: " + str(err), ORANGE)
    printc("Try -h for usage information", ORANGE)
    exit(1)
for opt in opts:
    if opt[0] == "--testColors":
        print("Running terminal color test")
        testColors()
	exit(0)
    elif opt[0] == "--testConfigure":
        print("Testing configuration only")
        testConfigure = 1
    elif opt[0] == "-h":
        print_help()
        exit(0)
    elif opt[0] == "-H":
        plas2op, plas2default, plas2desc = getBuildOptionLists(scriptPath)
        plas2op.insert(0, project)

        cstoolop, cstooldefault, cstooldesc = getBuildOptionLists(CSToolPath)
        cstoolop.insert(0, "CS-Tools")
        # Note: this can actually be adjusted for any amount of build systems! currently just 2, but can be more
        printBuildOptions((plas2op, cstoolop), (plas2default, cstooldefault), (plas2desc, cstooldesc))
        
	exit(0)
    elif opt[0] == "-D":
        equals_check = opt[1].split("=")
        if opt[1] == "" or len(equals_check) == 1 or equals_check[1] == "":
            print("Option " + "".join([opt[0], equals_check[0]]) + " requires an argument")
            print("Try -h for usage information")
            exit(2)
        cmakeOpts.append("".join(opt))

printc("Configuring " + project + " from " + scriptPath, BLUE)

checkCompiler = 0
wrapperList = ["USE_MOYA", "USE_AMPI"]

# Looks silly and can be better 100%, but was shortest thing I found for the bash equivalent
for wrapper in wrapperList:
    for opt in cmakeOpts:
        if wrapper in opt:
            checkCompiler += 1

cmakeout = open(scriptPath + "cmake.out", "w+")

if checkCompiler > 0:
    printc("Looking for associated compiler wrappers", BLUE)
    tmpBuildDir = ".CS-ToolsBuildTemp"

    if os.path.exists(tmpBuildDir):
        if os.path.exists(tmpBuildDir + "/CMakeCache.txt"):
            os.remove(tmpBuildDir + "/CMakeCache.txt")
    else:
        os.mkdir(tmpBuildDir)
    os.chdir(tmpBuildDir)
    cmake_CSTool = ["cmake", CSToolPath, " ".join(cmakeOpts), "-DFindCompiler=ON"]
    cmake_proc = subprocess.Popen(cmake_CSTool, stdout=cmakeout, stderr=cmakeout)

    devnull = open(os.devnull, "w")

    if any("USE_MOYA=ON" in opt for opt in cmakeOpts):
        printc("Looking for Moya", ORANGE)
        if subprocess.call(["grep", "Found Moya", "cmake.out"], stdout=devnull) == 0:
            printc("Found Moya", CYAN)
            grepCC = subprocess.Popen(["grep", "MOYA_CC", "cmake.out"], stdout=subprocess.PIPE)
            moyaCC = subprocess.check_output(["cut", "-d", " ", "-f", "2"], stdin=grepCC.stdout).decode("UTF-8")

            grepCXX = subprocess.Popen(["grep", "MOYA_CXX", "cmake.out"], stdout=subprocess.PIPE)
            moyaCXX = subprocess.check_output(["cut", "-d", " ", "-f", "2"], stdin=grepCXX.stdout).decode("UTF-8")

            grepFC = subprocess.Popen(["grep", "MOYA_FC", "cmake.out"], stdout=subprocess.PIPE)
            moyaFC = subprocess.check_output(["cut", "-d", " ", "-f", "2"], stdin=grepFC.stdout).decode("UTF-8")

            grepAR = subprocess.Popen(["grep", "MOYA_AR", "cmake.out"], stdout=subprocess.PIPE)
            moyaAR = subprocess.check_output(["cut", "-d", " ", "-f", "2"], stdin=grepAR.stdout).decode("UTF-8")

            printc("Moya C compiler \t " + moyaCC, GREEN)
            printc("Moya C++ compiler \t " + moyaCXX, GREEN)
            printc("Moya Fortran compiler \t " + moyaFC, GREEN)
            printc("Moya AR compiler \t\t " + moyaAR, GREEN)

            cmakeOpts.append("-DCMAKE_C_COMPILER=" + moyaCC)
            cmakeOpts.append("-DCMAKE_CXX_COMPILER=" + moyaCXX)
            cmakeOpts.append("-DCMAKE_Fortran_COMPILER=" + moyaFC)
            cmakeOpts.append("-DCMAKE_AR=" + moyaAR)
        else:
            printc("CS-Tools could not find Moya. Check options and resubmit. Aborting", RED)
            exit(2)
    elif any("USE_AMPI=ON" in opt for opt in cmakeOpts):
        printc("Looking for AMPI", ORANGE)
        if subprocess.call(["grep", "Found AMPI", "cmake.out"], stdout=devnull) == 0:
            printc("This isn't yet supported..", CYAN)
    os.chdir("..")

printc("Configuring " + project, BLUE)
print("cmake " + scriptPath + " ".join(cmakeOpts))

if testConfigure == 0:
    optsString = " ".join(
        cmakeOpts)
    print(optsString)
    if len(optsString) > 0:
        cmakeArr = ["cmake", scriptPath, optsString]
    else:
        cmakeArr = ["cmake", scriptPath]
    #print(" ".join(cmakeArr))
    final_cmake = subprocess.Popen(cmakeArr, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    for line in final_cmake.stdout:
        sys.stdout.write(line)
        cmakeout.write(line)
    for line in final_cmake.stderr:
        sys.stderr.write(line)
        cmakeout.write(line)
    final_cmake.wait()
