//
// Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
// License: MIT, http://opensource.org/licenses/MIT
//
#include "PCPPReport.H"

namespace pcpp {
  namespace report {

    void SimFileInfo(std::ostream &outStream,
                     const pcpp::io::simfileinfo &simFileInfo)
    {
      std::string fileName(simFileInfo.fileName);
      const std::bitset<pcpp::io::NUMFORMATBITS> &formatBits(simFileInfo.formatBits);
      bool legacyFile = false;

      if(fileName.empty())
        fileName = "[NULL]";
      outStream << "---------------------------" << std::endl
                << "XPACC File Information:"                << std::endl
                << "FileName    = " << fileName   << std::endl
                << "Format      = ";
      if(formatBits.test(pcpp::io::ISLEGACY)){
        outStream << "Legacy"       << std::endl;
        legacyFile = true;
        outStream << "SimStep     = " << simFileInfo.simStep << std::endl
                  << "SimTime     = " << simFileInfo.simTime << std::endl
                  << "GridData    = "
                  << (formatBits.test(pcpp::io::HASGRID) ?
                      "Yes" : "No") << std::endl
                  << "IBlankData  = "
                  << (formatBits.test(pcpp::io::HASIBLANK) ?
                      "Yes" : "No") << std::endl
                  << "StateData   = " 
                  << (formatBits.test(pcpp::io::HASSTATE) ?
                      "Yes" : "No") << std::endl
                  << "TargetData  = "
                  << (formatBits.test(pcpp::io::HASTARGDATA) ?
                      "Yes" : "No") << std::endl
                  << "AuxData     = "
                  << (formatBits.test(pcpp::io::HASAUXDATA) ?
                      "Yes" : "No") << std::endl;
        if(formatBits.test(pcpp::io::HASAUXDATA))
          outStream << "NumAuxVar = "
                    << simFileInfo.numAuxVars << std::endl;        
        int numGrids = simFileInfo.numGrids;
        if(numGrids > 0){
          outStream << "====== Grid Information =====" << std::endl;
          outStream << "NumGrids    = " << simFileInfo.numGrids << std::endl;
          std::vector<int> gridNumDim(simFileInfo.gridNumDimensions);
          if(gridNumDim.size() != numGrids){
            gridNumDim.resize(numGrids,0);
          }
          const std::vector<std::vector<size_t> > &gridSizes(simFileInfo.gridSizes);
          if(gridSizes.size() != numGrids){
            outStream << "ERROR with legacy grid sizes != number of grids" << std::endl;
            return;
          }
          std::vector<std::string> gridNames(simFileInfo.gridNames);
          if(gridNames.size() != numGrids){
            gridNames.resize(numGrids,"[NULL]");
          }
          for(int iGrid = 0;iGrid < numGrids;iGrid++){
            outStream << "- - - ( Grid " << iGrid << " ) - - -"    << std::endl
                      << "  Grid Name:      " << gridNames[iGrid]  << std::endl
                      << "  Grid Dimension: " << gridNumDim[iGrid] << std::endl
                      << "  Grid Size:     (";
            pcpp::io::DumpContents(outStream,gridSizes[iGrid],",");
            outStream << ")" << std::endl;
          }
        }
        outStream << "===== Simulation Information =====" << std::endl;
        if(!simFileInfo.simulationParamDictionary.empty()){
          outStream << " -- Simulation Parameters -- " << std::endl
                    << simFileInfo.simulationParamDictionary.Report() 
                    << std::endl;
        }
        if(!simFileInfo.simulationStateDictionary.empty()){
          outStream << " -- Simulation State Data -- " << std::endl
                    << simFileInfo.simulationStateDictionary.Report()
                    << std::endl;
        }
      } else {
        outStream << "NextGen"      << std::endl
                  << "Provenance  = " 
                  << (formatBits.test(pcpp::io::HASPROVENANCE) ?
                      "Yes" : "No") << std::endl
                  << "ConfigData  = "
                  << (formatBits.test(pcpp::io::HASCONFIG) ?
                      "Yes" : "No") << std::endl
                  << "DataDict    = "
                  << (formatBits.test(pcpp::io::HASDATADICT) ?
                      "Yes" : "No") << std::endl
                  << "Geometry    = "
                  << (formatBits.test(pcpp::io::HASGEOMETRY) ?
                      "Yes" : "No") << std::endl
                  << "Simulation  = "
                  << (formatBits.test(pcpp::io::HASSIMULATION) ?
                      "Yes" : "No") << std::endl;
        int numGeometries = simFileInfo.numGeometries;
        if(numGeometries > 0){
          if(simFileInfo.geometryNames.size()     != numGeometries ||
             simFileInfo.geometryGridNames.size() != numGeometries ||
             simFileInfo.geometryGridSizes.size() != numGeometries){
            outStream << "Geometries improperly formatted." << std::endl;
            return;
          }
          outStream << "====== Geometry Information ======" << std::endl;
          outStream << "NumGeometries  = " << numGeometries << std::endl;
          for(int iGeom = 0;iGeom < numGeometries;iGeom++){
            const std::string &geometryName(simFileInfo.geometryNames[iGeom]);
            outStream << "Geometry(" << iGeom << ") = " << geometryName << std::endl;
            int numGrids = simFileInfo.geometryGridNames[iGeom].size();
            outStream << "  Number of grids: " << numGrids << std::endl;
            for(int iGrid = 0;iGrid < numGrids;iGrid++){
              const std::string &gridName(simFileInfo.geometryGridNames[iGeom][iGrid]);
              outStream << "  Grid(" << iGrid << ") := " << gridName << std::endl;
              int numDim = simFileInfo.geometryGridSizes[iGeom][iGrid].size();
              outStream << "    Dimension: " << numDim << std::endl
                        << "    Size: (";
              pcpp::io::DumpContents(outStream,simFileInfo.geometryGridSizes[iGeom][iGrid],",");
              outStream << ")" << std::endl;
            }
          }
        }
        int numDomains = simFileInfo.numDomains;
        if(numDomains > 0){
          outStream << "===== Simulation Information =====" << std::endl
                    << "Simulation Step: " << simFileInfo.simStep << std::endl;
          if(!simFileInfo.simulationParamDictionary.empty()){
            outStream << " -- Simulation Parameters -- " << std::endl
                      << simFileInfo.simulationParamDictionary.Report() 
                      << std::endl;
          }
          if(!simFileInfo.simulationStateDictionary.empty()){
            outStream << " -- Simulation State Data -- " << std::endl
                      << simFileInfo.simulationStateDictionary.Report()
                      << std::endl;
          }
          outStream << "Number of Domains: " << numDomains  << std::endl;
          const std::vector<std::string> &domainNames(simFileInfo.domainNames);
          const std::vector<std::vector<std::string> > &allDomainGridNames(simFileInfo.domainGridNames);
          const std::vector<std::vector<std::string> > &allDomainGridPaths(simFileInfo.domainGridPaths);
          if(domainNames.size()        != numDomains ||
             allDomainGridNames.size() != numDomains ||
             allDomainGridPaths.size() != numDomains){
            outStream << "Domain information improperly formatted." << std::endl;
            return;
          }
          std::vector<std::string>::const_iterator domainNameIt = domainNames.begin();
          std::vector<std::vector<std::string> >::const_iterator allDomainGridNamesIt =
            allDomainGridNames.begin();
          std::vector<std::vector<std::string> >::const_iterator allDomainGridPathsIt = 
            allDomainGridPaths.begin();
          std::vector<pcpp::field::metadataset>::const_iterator domainParamDictionaryIt = 
            simFileInfo.domainParamDictionaries.begin(); 
          std::vector<pcpp::field::metadataset>::const_iterator domainStateDictionaryIt = 
            simFileInfo.domainStateDictionaries.begin(); 
          while(domainNameIt != domainNames.end()){
            const pcpp::field::metadataset &domainParamDictionary(*domainParamDictionaryIt++);
            const pcpp::field::metadataset &domainStateDictionary(*domainStateDictionaryIt++);
            const std::vector<std::string> &domainGridNames(*allDomainGridNamesIt++);
            const std::vector<std::string> &domainGridPaths(*allDomainGridPathsIt++);
            const std::string &domainName(*domainNameIt++);
            outStream << " --- " << domainName << " --- " << std::endl;
            int numDomainGrids = domainGridNames.size();
            std::vector<std::string>::const_iterator domainGridNameIt = domainGridNames.begin();
            std::vector<std::string>::const_iterator domainGridPathIt = domainGridPaths.begin();
            outStream << "   Grids: ";
            while(domainGridNameIt != domainGridNames.end()){
              outStream << *domainGridNameIt++ << "@" << *domainGridPathIt++ << " ";
            }
            outStream << std::endl;
            if(!domainParamDictionary.empty()){
              outStream << "   ---- Domain Parameters ----" << std::endl
                        << domainParamDictionary.Report()
                        << std::endl;
            }
            if(!domainStateDictionary.empty()){
              outStream << "  ---- Domain State ----" << std::endl
                        << domainStateDictionary.Report()
                        << std::endl;
            }
          }
        }
      }
      outStream << "---------------------------" << std::endl;
    };
  
    void Configuration(std::ostream &outStream,
                       const fixtures::ConfigurationType &inConfig)
    {
      outStream << "---------------------------" << std::endl
                << "Configuration:             " << std::endl;
      fixtures::ConfigurationType::const_iterator configIt = inConfig.begin();
      while(configIt != inConfig.end()){
        const fixtures::ConfigurationType::ParamType &configParam(*configIt++);
        outStream << configParam.first << " = " << configParam.second << std::endl;
      }
      outStream << "---------------------------" << std::endl;
    }

    void CartesianSetup(std::ostream &outStream,
                        const std::vector<int> &cartCoords,
                        const std::vector<int> &cartDims)
    {
      outStream << "-------------------------"
                << std::endl
                << "Cartesian setup: " << std::endl
                << "CartCoords: (";
      pcpp::io::DumpContents(outStream,cartCoords,",");
      outStream << ") CartDims: (";
      pcpp::io::DumpContents(outStream,cartDims,",");
      outStream << ")" << std::endl
                << "-------------------------"
                << std::endl;
    }
    
    void CartesianSetup(std::ostream &outStream,pcpp::ParallelTopologyInfoType &inTopo)
    {
      outStream << "-------------------------"
                << std::endl
                << "Cartesian setup: " << std::endl
                << "Number of dimensions: " << inTopo.numDimensions << std::endl
                << "Topo rank: " << inTopo.rank << std::endl
                << "Configuration---" << std::endl
                << "  Decomp directions: (";
      pcpp::io::DumpContents(outStream,inTopo.cartDecompDirections,",");
      outStream << ")" << std::endl
                << "  Cart decomp: (";
      pcpp::io::DumpContents(outStream,inTopo.cartDecomposition,",");
      outStream << ")" << std::endl
                << "  Periodic dirs: (";
      pcpp::io::DumpContents(outStream,inTopo.isPeriodic,",");
      outStream << ")" << std::endl
                << "Topology info---" << std::endl
                << "  Cart Dimensions: (";
      pcpp::io::DumpContents(outStream,inTopo.dimDir,",");
      outStream << ")" << std::endl
                << "  Cart Coordinates: (";
      pcpp::io::DumpContents(outStream,inTopo.topoCoords,",");
      outStream << ")" << std::endl
                << "  Cart Neighbors: (";
      pcpp::io::DumpContents(outStream,inTopo.neighborRanks,",");
      outStream << ")" << std::endl
                << "-------------------------"
                << std::endl;
    }
    
  };
};
