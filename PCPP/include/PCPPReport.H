//
// Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
// License: MIT, http://opensource.org/licenses/MIT
//
#ifndef __PCPP_REPORT_H__
#define __PCPP_REPORT_H__

#include "PCPPIO.H"
#include "PCPPCommUtil.H"

namespace pcpp {
  namespace report {

    void SimFileInfo(std::ostream &outStream,
                     const pcpp::io::simfileinfo &simFileInfo);

    void CartesianSetup(std::ostream &outStream,
                        const std::vector<int> &cartCoords,
                        const std::vector<int> &cartDims);

    void CartesianSetup(std::ostream &outStream,pcpp::ParallelTopologyInfoType &inTopo);

    void Configuration(std::ostream &outStream,
                       const fixtures::ConfigurationType &inConfig);

    template<typename BufferDataType>
    void StructuredBufferContents(std::ostream &outStream,
                                  const BufferDataType *dataBuffer,
                                  const pcpp::IndexIntervalType &bufferExtent)
    {
      size_t numX = bufferExtent[0].second - bufferExtent[0].first + 1;
      size_t numY = bufferExtent[1].second - bufferExtent[1].first + 1;
      size_t numZ = bufferExtent[2].second - bufferExtent[2].first + 1;
      for(size_t k = bufferExtent[2].first;k <= bufferExtent[2].second;k++){
        size_t kIndex = k - bufferExtent[2].first;
        for(size_t j = bufferExtent[1].first;j <= bufferExtent[1].second;j++){
          size_t jIndex = j - bufferExtent[1].first;
          for(size_t i = bufferExtent[0].first;i <= bufferExtent[0].second;i++){
            size_t iIndex = i - bufferExtent[0].first;
            size_t bufferIndex = kIndex*numX*numZ + jIndex*numX + iIndex;
            outStream << "Buffer(" << i << "," << j << "," << k << ") = "
                      << dataBuffer[bufferIndex] << std::endl;
          }
        }
      }
    };

    template<typename BufferDataType>
    void StructuredSubBufferContents(std::ostream &outStream,
                                     const BufferDataType *dataBuffer,
                                     const pcpp::IndexIntervalType &bufferExtent,
                                     const pcpp::IndexIntervalType &regionExtent)
    {
      size_t numX = bufferExtent[0].second - bufferExtent[0].first + 1;
      size_t numY = bufferExtent[1].second - bufferExtent[1].first + 1;
      size_t numZ = bufferExtent[2].second - bufferExtent[2].first + 1;
      for(size_t k = regionExtent[2].first;k <= regionExtent[2].second;k++){
        size_t kIndex = k - bufferExtent[2].first;
        for(size_t j = regionExtent[1].first;j <= regionExtent[1].second;j++){
          size_t jIndex = j - bufferExtent[1].first;
          for(size_t i = regionExtent[0].first;i <= regionExtent[0].second;i++){
            size_t iIndex = i - bufferExtent[0].first;
            size_t bufferIndex = kIndex*numX*numZ + jIndex*numX + iIndex;
            outStream << "Buffer(" << i << "," << j << "," << k << ") = "
                      << dataBuffer[bufferIndex] << std::endl;
          }
        }
      }
    };

  };
};

#endif
