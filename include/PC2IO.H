//
// Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
// License: MIT, http://opensource.org/licenses/MIT
//
#ifndef __PC2_IO_H__
#define __PC2_IO_H__

#include "PlasCom2.H"
#include "PCPPCommUtil.H"
#include "PCPPIO.H"
#include "PCPPReport.H"
#include "PCPPIntervalUtils.H"

namespace plascom2 {
  namespace io {

#ifdef ENABLE_HDF5
    namespace hdf5 {

      template<typename GridType,typename StateType,typename ConfigType>
      int OutputSingle(const std::string &fileName,
                       const std::string &domainName,
                       const std::string &geometryName,
                       const std::string &gridName,
                       GridType     &inGrid,
                       StateType    &inState,
                       StateType    &paramState,
                       ConfigType   &simConfig,
                       std::ostream &outStream,
                       int iStep = 0){

        pcpp::CommunicatorType &gridComm(inGrid.Communicator());

        // Begin PlasCom2 output
        if(ix::sys::FILEEXISTS(fileName))
          ix::sys::Remove(fileName);

        int numDomains = 1;

        int myRank = gridComm.Rank();

        pcpp::io::hdf5::base pc2File(fileName,gridComm);
        pc2File.SetMessageStream(outStream);
        pc2File.CreateGroup("/PlasCom2");
        pc2File.CreateGroup("/PlasCom2/Configuration");
        std::ostringstream configStream;
        configStream << simConfig;
        std::string configString(configStream.str());
        if(!configString.empty())
          pc2File.CreateAttribute("/PlasCom2/Configuration/Parameters",configString);

        int numGeometries = 1;
        pc2File.CreateGroup("/PlasCom2/Geometry");
        std::vector<size_t> attSize(1,1);
        pc2File.CreateAttribute("/PlasCom2/Geometry/numGeometries",attSize,&numGeometries);
        pc2File.CreateAttribute("/PlasCom2/Geometry/geometryNames",geometryName);
        std::string geomGroupName(std::string("/PlasCom2/Geometry/")+geometryName);
        pc2File.CreateGroup(geomGroupName);
        int numGrids = 1;
        pc2File.CreateAttribute(geomGroupName+"/numGrids",attSize,&numGrids);
        pc2File.CreateAttribute(geomGroupName+"/gridNames",gridName);
        pcpp::io::hdf5::WriteGrid(inGrid,gridName,geomGroupName,pc2File);
        std::string geomGridGroupName(geomGroupName+"/"+gridName);
        

        pc2File.CreateGroup("/PlasCom2/Simulation");
        pc2File.CreateAttribute("/PlasCom2/Simulation/numDomains",attSize,&numDomains);
        pc2File.CreateAttribute("/PlasCom2/Simulation/domainNames",domainName);
        std::string domainGroupName(std::string("/PlasCom2/Simulation/")+domainName);
        pc2File.CreateGroup(domainGroupName);

        // Simulation-global parameter and state data
        pcpp::field::metadataset parameterDictionary(paramState.Dictionary('s'));
        pcpp::field::metadataset simulationStateDictionary(inState.Dictionary('s'));

        if(!parameterDictionary.empty()){
          std::ostringstream dictionaryOut;
          dictionaryOut << parameterDictionary;
          std::string dictionaryString(dictionaryOut.str());
          pc2File.CreateAttribute("/PlasCom2/Simulation/paramDictionary",dictionaryString);
        }

        if(!simulationStateDictionary.empty()){
          std::ostringstream dictionaryOut;
          dictionaryOut << simulationStateDictionary;
          std::string dictionaryString(dictionaryOut.str());
          pc2File.CreateAttribute("/PlasCom2/Simulation/stateDictionary",dictionaryString);
        }


        pcpp::io::hdf5::WriteStateAttributes(inState,'s',"/PlasCom2/Simulation",pc2File);
        pcpp::io::hdf5::WriteStateAttributes(paramState,'s',"/PlasCom2/Simulation",pc2File);

        pc2File.CreateAttribute("/PlasCom2/Simulation/stepNumber",attSize,&iStep);
        
        //        pc2File.CreateGroup(domainGroupName+"/Configuration");
        //         std::ostringstream domainConfigStream;
        //         domainConfigStream << domainConfig;
        //         pc2File.CreateAttribute(domainGroupName+"/Configuration/Parameters",domainConfigStream.str());
        int numDomainGrids = 1;
        std::string domainFullGridName(geometryName+":"+gridName);
        std::string domainGridName("grid1");
        pc2File.CreateAttribute(domainGroupName+"/numGrids",attSize,&numDomainGrids);
        pc2File.CreateAttribute(domainGroupName+"/gridNames",domainGridName);
        
        pcpp::field::metadataset domainParameterDictionary(paramState.Dictionary('d'));
        pcpp::field::metadataset gridParameterDictionary(paramState.Dictionary('m'));
        pcpp::field::metadataset domainStateDictionary(inState.Dictionary('d'));
        pcpp::field::metadataset gridStateDictionary(inState.Dictionary('m'));
        pcpp::field::metadataset nodeStateDictionary(inState.Dictionary('n'));
        pcpp::field::metadataset cellStateDictionary(inState.Dictionary('c'));

        domainParameterDictionary.Append(gridParameterDictionary);
        domainStateDictionary.Append(gridStateDictionary);
        domainStateDictionary.Append(nodeStateDictionary);
        domainStateDictionary.Append(cellStateDictionary);

        if(!domainParameterDictionary.empty()){
          std::ostringstream dictionaryOut;
          dictionaryOut << domainParameterDictionary;
          std::string dictionaryString(dictionaryOut.str());
          pc2File.CreateAttribute(domainGroupName+"/paramDictionary",dictionaryString);
        }
        if(!domainStateDictionary.empty()){
          std::ostringstream dictionaryOut;
          dictionaryOut << domainStateDictionary;
          std::string dictionaryString(dictionaryOut.str());
          pc2File.CreateAttribute(domainGroupName+"/stateDictionary",dictionaryString);
        }
        pcpp::io::hdf5::WriteStateAttributes(inState,'d',domainGroupName,pc2File);
        pcpp::io::hdf5::WriteStateAttributes(paramState,'d',domainGroupName,pc2File);

        std::string gridGroupName(domainGroupName+"/"+domainGridName);
        pc2File.CreateGroup(gridGroupName);
        pc2File.CreateAttribute(gridGroupName+"/gridPath",domainFullGridName);
        pcpp::io::hdf5::WriteStateAttributes(paramState,'m',gridGroupName,pc2File);
        pcpp::io::hdf5::WriteStateAttributes(inState,'m',gridGroupName,pc2File);
        
        pcpp::IndexIntervalType partInterval(inGrid.PartitionInterval());
        pcpp::IndexIntervalType partitionBufferInterval(inGrid.PartitionBufferInterval());
        std::vector<size_t> partitionSizes(partInterval.Sizes());
        std::vector<size_t> partitionStarts(partInterval.Starts());
        std::vector<size_t> partitionBufferStarts(partitionBufferInterval.Starts());
        std::vector<size_t> gridSizes(inGrid.GridSizes());
        std::vector<size_t> bufferSizes(inGrid.BufferSizes());

        pcpp::io::hdf5::WriteStateNodeData(inState,gridSizes,partitionSizes,partitionStarts,
                                           bufferSizes,partitionBufferStarts,gridGroupName,pc2File);  

        pc2File.Close();

        
        if(myRank == 0){
          pcpp::io::xdmf::WriteSimulationFile(fileName,gridName,geomGridGroupName,
                                              gridGroupName,inGrid,inState);
        }

        return(0);
      } 

      template<typename DomainType,typename ConfigType>
      int WriteSingleDomain(const std::string &fileName,
                            DomainType &inDomain,
                            ConfigType &simConfig,
                            std::ostream &outStream)
      {
        
        pcpp::CommunicatorType &domainCommunicator(inDomain.Communicator());
        int domainRank = domainCommunicator.Rank();
	int numRankDomain = domainCommunicator.NProc();

        int numDomains = 1;
	
	
        int numDomainGrids = inDomain.NumberOfGrids();
        const std::vector<std::string> &domainGridNames(inDomain.GridNames());
        const std::vector<std::string> &domainGeometryNames(inDomain.GeometryNames());
        int numGeometries = domainGeometryNames.size();
        const std::string &domainName(inDomain.Name());
        std::vector<std::string> uniqueGeometryNames(domainGeometryNames);
        std::sort(uniqueGeometryNames.begin(),uniqueGeometryNames.end());
        uniqueGeometryNames.erase(std::unique(uniqueGeometryNames.begin(),uniqueGeometryNames.end()),
                                  uniqueGeometryNames.end());
        int numUniqueGeometries = uniqueGeometryNames.size();
        std::vector<std::string> uniqueGridNames(domainGridNames);
        std::sort(uniqueGridNames.begin(),uniqueGridNames.end());
        uniqueGridNames.erase(std::unique(uniqueGridNames.begin(),uniqueGridNames.end()),
                              uniqueGridNames.end());
        int numUniqueGrids = uniqueGridNames.size();
        std::string domainGroupName(std::string("/PlasCom2/Simulation/")+domainName);
        std::vector<size_t> attSize1(1,1);
        bool writeGeometry = true;
        if(simConfig.IsSet("PlasCom2:WriteGeometry"))
          writeGeometry = simConfig.GetFlagValue("PlasCom2:WriteGeometry");
        int iStep = inDomain.Step();

	// std::cout << "Writing " << fileName << " on rank " << domainRank << "/"
	// 	  << numRankDomain << std::endl;
	
        if(domainRank == 0){


          // Begin PlasCom2 output
          if(ix::sys::FILEEXISTS(fileName))
            ix::sys::Remove(fileName);

          pcpp::io::hdf5::base pc2File(fileName);
          pc2File.SetMessageStream(outStream);
          pc2File.CreateGroup("/PlasCom2");
          pc2File.CreateGroup("/PlasCom2/Configuration");

          std::ostringstream configStream;
          configStream << simConfig;
          std::string configString(configStream.str());
          if(!configString.empty())
            pc2File.CreateAttribute("/PlasCom2/Configuration/Parameters",configString);

          if(writeGeometry){

            pc2File.CreateGroup("/PlasCom2/Geometry");
            pc2File.CreateAttribute("/PlasCom2/Geometry/numGeometries",attSize1,&numUniqueGeometries);
            std::ostringstream geometryNameStream;
            for(int iGeom = 0;iGeom < numUniqueGeometries;iGeom++){
              geometryNameStream << uniqueGeometryNames[iGeom];
              if(iGeom != (numUniqueGeometries-1))
                geometryNameStream << " ";
            }

            std::map<std::string,int> numGeometryGrids;
            std::string allGeometryNames(geometryNameStream.str());
            pc2File.CreateAttribute("/PlasCom2/Geometry/geometryNames",allGeometryNames);
            for(int iGeom = 0;iGeom < numUniqueGeometries;iGeom++){
              const std::string &geometryName(uniqueGeometryNames[iGeom]);
              const std::string geomGroupName(std::string("/PlasCom2/Geometry/")+geometryName);
              pc2File.CreateGroup(geomGroupName);
              numGeometryGrids.insert(std::make_pair(geometryName,0));
            }

            typedef std::multimap<std::string,std::string> namesmap;
            namesmap geometryGridNames;
            for(int iGrid = 0;iGrid < numUniqueGrids;iGrid++){
              const std::string &domainGridName(uniqueGridNames[iGrid]);
              std::string::size_type x = domainGridName.find_last_of(":");
              const std::string gridGeometryName(domainGridName.substr(0,x));
              const std::string gridOnlyName(domainGridName.substr(x+1));
              const std::string gridPath(std::string("/PlasCom2/Geometry/")+
                                         gridGeometryName+"/"+gridOnlyName);
              numGeometryGrids[gridGeometryName]++;
              geometryGridNames.insert(std::make_pair(gridGeometryName,gridOnlyName));
            }
            
            for(int iGeom = 0;iGeom < numUniqueGeometries;iGeom++){
              const std::string &geometryName(uniqueGeometryNames[iGeom]);
              const std::string geomGroupName(std::string("/PlasCom2/Geometry/")+geometryName);
              int numGeomGrids = numGeometryGrids[geometryName];
              pc2File.CreateAttribute(geomGroupName+"/numGrids",attSize1,&numGeomGrids);
              std::pair<namesmap::iterator,namesmap::iterator> gridsOfGeometry;
              gridsOfGeometry = geometryGridNames.equal_range(geometryName);
              namesmap::iterator gnIt = gridsOfGeometry.first;
              std::ostringstream allGridNamesStream;
              while(gnIt != gridsOfGeometry.second){
                allGridNamesStream << gnIt->second;
                gnIt++;
                if(gnIt != gridsOfGeometry.second)
                  allGridNamesStream << " ";
              }
              const std::string allGridNames(allGridNamesStream.str());
              pc2File.CreateAttribute(geomGroupName+"/gridNames",allGridNames);
            }
          }

          pc2File.CreateGroup("/PlasCom2/Simulation");
          pc2File.CreateAttribute("/PlasCom2/Simulation/numDomains",attSize1,&numDomains);
          pc2File.CreateAttribute("/PlasCom2/Simulation/domainNames",domainName);
          pc2File.CreateGroup(domainGroupName);

          // Simulation-global parameter and state dictionaries
          pcpp::field::metadataset &parameterDictionary(inDomain.ParamDictionary());
          pcpp::field::metadataset &stateDictionary(inDomain.StateDictionary());
          
          pcpp::field::metadataset simulationParamDict(pcpp::util::ExtractDictionary(parameterDictionary,'s'));
          pcpp::field::metadataset simulationStateDict(pcpp::util::ExtractDictionary(stateDictionary,'s'));
          
          if(!simulationParamDict.empty()){
            std::ostringstream dictionaryOut;
            dictionaryOut << simulationParamDict;
            std::string dictionaryString(dictionaryOut.str());
            pc2File.CreateAttribute("/PlasCom2/Simulation/paramDictionary",dictionaryString);
          }
          
          if(!simulationStateDict.empty()){
            std::ostringstream dictionaryOut;
            dictionaryOut << simulationStateDict;
            std::string dictionaryString(dictionaryOut.str());
            pc2File.CreateAttribute("/PlasCom2/Simulation/stateDictionary",dictionaryString);
          }
          
          
          std::ostringstream gridNamesStream;
          std::vector<std::string> gridNames;
          for(int iGrid = 1;iGrid <= numDomainGrids;iGrid++){
            std::ostringstream gridNameStream;
            gridNameStream << "grid" << iGrid;
            gridNames.push_back(gridNameStream.str());
            gridNamesStream << gridNames[iGrid-1];
            if(iGrid != numDomainGrids)
              gridNamesStream << " ";
          }

          const std::string allGridNames(gridNamesStream.str());
          pc2File.CreateAttribute(domainGroupName+"/numGrids",attSize1,&numDomainGrids);
          pc2File.CreateAttribute(domainGroupName+"/gridNames",allGridNames);
        
          pcpp::field::metadataset domainParamDict(pcpp::util::ExtractDictionary(parameterDictionary,'d'));
          pcpp::field::metadataset domainStateDict(pcpp::util::ExtractDictionary(stateDictionary,'d'));
          pcpp::field::metadataset meshParamDictionary(pcpp::util::ExtractDictionary(parameterDictionary,'m'));
          pcpp::field::metadataset meshStateDictionary(pcpp::util::ExtractDictionary(stateDictionary,'m'));
          pcpp::field::metadataset nodeStateDictionary(pcpp::util::ExtractDictionary(stateDictionary,'n'));
          pcpp::field::metadataset cellStateDictionary(pcpp::util::ExtractDictionary(stateDictionary,'c'));

          domainParamDict.Append(meshParamDictionary);
          domainStateDict.Append(meshStateDictionary);
          domainStateDict.Append(nodeStateDictionary);
          domainStateDict.Append(cellStateDictionary);
          
          if(!domainParamDict.empty()){
            std::ostringstream dictionaryOut;
            dictionaryOut << domainParamDict;
            std::string dictionaryString(dictionaryOut.str());
            pc2File.CreateAttribute(domainGroupName+"/paramDictionary",dictionaryString);
          }
          
          if(!domainStateDict.empty()){
            std::ostringstream dictionaryOut;
            dictionaryOut << domainStateDict;
            std::string dictionaryString(dictionaryOut.str());
            pc2File.CreateAttribute(domainGroupName+"/stateDictionary",dictionaryString);
          }
          
          for(int iGrid = 0;iGrid < numDomainGrids;iGrid++){
            std::string gridGroupName(domainGroupName+"/"+gridNames[iGrid]);
            const std::string &domainFullGridName(domainGridNames[iGrid]);
            pc2File.CreateGroup(gridGroupName);
            pc2File.CreateAttribute(gridGroupName+"/gridPath",domainFullGridName);
          }
          pc2File.Close();
        }

	domainCommunicator.Barrier();

	if(domainRank == 0){
	  outStream << "Front matter created for HDF5(" << fileName
		    << ")." << std::endl;
	}
	    
        const std::vector<int> &localGridIndices(inDomain.LocalGridIndices());
        
        std::map<std::string,bool> gridHasBeenWritten;
        for(int iGrid = 0;iGrid < numDomainGrids;iGrid++)
          gridHasBeenWritten.insert(std::make_pair(domainGridNames[iGrid],false));

	bool simStateWritten = false;
        bool domainStateWritten = false;
        for(int iGrid = 0;iGrid < numDomainGrids;iGrid++){
          const std::string &fullGridName(domainGridNames[iGrid]);
	  
	  if(domainRank == 0){
	    outStream << "Processing data for Grid (" << iGrid+1 << ","
		      << fullGridName << ")." << std::endl;
	  }
	  
          std::vector<int>::const_iterator localGridIt = std::find(localGridIndices.begin(),
                                                                   localGridIndices.end(),
                                                                   iGrid);
          if(localGridIt != localGridIndices.end()){ // This processor has a piece of the grid

	    typename DomainType::GridType &currentGrid(inDomain.Grid(iGrid));
	    int numDim = currentGrid.Dimension();
	    const std::vector<size_t> &currentGridSizes(currentGrid.GridSizes());

	    pcpp::CommunicatorType &gridCommunicator(currentGrid.Communicator());
            std::string::size_type x = fullGridName.find_last_of(":");
            const std::string geometryName(fullGridName.substr(0,x));
            const std::string gridName(fullGridName.substr(x+1));
            const std::string geometryPath(std::string("/PlasCom2/Geometry/")+geometryName);

	    //	    std::cout << "Opening " << fileName << std::endl;
            pcpp::io::hdf5::base pc2File(fileName,gridCommunicator);
	    gridCommunicator.Barrier();
	    pc2File.SetMessageStream(outStream);
	    // if(pc2File.Exists(geometryPath)){
	    //   std::cout << "Path exists." << std::endl;
	    // }

	    if(!gridHasBeenWritten[fullGridName]){
              if(pcpp::io::hdf5::WriteGrid(currentGrid,gridName,geometryPath,pc2File)){
		outStream << "Writing grid (" << gridName << ")@(" << geometryPath
			  << ") failed." << std::endl;
		return(1);
	      }
              gridHasBeenWritten[fullGridName] = true;
            }
	    
	    gridCommunicator.Barrier();
	    
	    //	    std::cout << "Grid (" << gridName << ") has been written." << std::endl;
	    
	    std::ostringstream simGridNameStream;
            simGridNameStream << "grid" << iGrid+1;
            const std::string simGridName(simGridNameStream.str());
            typename DomainType::StateType &inState(inDomain.State(iGrid));
            typename DomainType::StateType &paramState(inDomain.Param(iGrid));

            if(iGrid==0 && !simStateWritten){
              pcpp::io::hdf5::WriteStateAttributes(inState,'s',"/PlasCom2/Simulation",pc2File);
              pcpp::io::hdf5::WriteStateAttributes(paramState,'s',"/PlasCom2/Simulation",pc2File);
              int iStep = inDomain.Step();
              //              double simTime = inDomain.Time();
              pc2File.CreateAttribute("/PlasCom2/Simulation/stepNumber",attSize1,&iStep);
              //              pc2File.CreateAttribute("/PlasCom2/Simulation/simTime",attSize1,&simTime);
              simStateWritten = true;
	      gridCommunicator.Barrier();
	      //	      std::cout << "Sim State written for domain" << std::endl;
            }

	    
            if((iGrid == 0) && !domainStateWritten){
              pcpp::io::hdf5::WriteStateAttributes(inState,'d',domainGroupName,pc2File);
              pcpp::io::hdf5::WriteStateAttributes(paramState,'d',domainGroupName,pc2File);
              pc2File.CreateAttribute(domainGroupName+"/stepNumber",attSize1,&iStep);
              domainStateWritten = true;
	      gridCommunicator.Barrier();
	      //	      std::cout << "Domain State written for domain" << std::endl;
            }

	    
            const std::string simGridGroupName(domainGroupName+"/"+simGridName);

	    // gridCommunicator.Barrier();
	    // std::cout << "Writing parameters and grid-global data into group(" << simGridGroupName << ")"
	    // 	      << std::endl;
            std::vector<size_t> sizeAtt(1,numDim);
            pc2File.CreateAttribute(simGridGroupName+"/gridSize",sizeAtt,&currentGridSizes[0]);
            pcpp::io::hdf5::WriteStateAttributes(paramState,'m',simGridGroupName,pc2File);
            pcpp::io::hdf5::WriteStateAttributes(inState,'m',simGridGroupName,pc2File);            
            pcpp::IndexIntervalType partInterval(currentGrid.PartitionInterval());
            pcpp::IndexIntervalType partitionBufferInterval(currentGrid.PartitionBufferInterval());
	    gridCommunicator.Barrier();
	    // std::cout << "parameters and grid-globals written." << std::endl;

	    

            std::vector<size_t> partitionSizes(partInterval.Sizes());
            std::vector<size_t> partitionStarts(partInterval.Starts());
            std::vector<size_t> partitionBufferStarts(partitionBufferInterval.Starts());
            std::vector<size_t> gridSizes(currentGrid.GridSizes());
            std::vector<size_t> bufferSizes(currentGrid.BufferSizes());
            
	    // std::cout << "Writing nodal state data.." << std::endl;
            if(pcpp::io::hdf5::WriteStateNodeData(inState,gridSizes,partitionSizes,partitionStarts,
                                                  bufferSizes,partitionBufferStarts,simGridGroupName,pc2File)){
              outStream << "ERROR! Failed to write state node data for " << simGridGroupName << std::endl;
              return(1);
            }  
            pc2File.Close();
	    gridCommunicator.Barrier();
	    // std::cout << "Writing nodal state data done." << std::endl;
          }
          domainCommunicator.Barrier();          
        }
        
        if(domainRank == 0){
          if(pcpp::io::xdmf::WriteDomainFile(fileName,inDomain))
	    domainCommunicator.SetErr(1);
        }
	
	int returnStatus = 0;
	if(domainCommunicator.Check())
	  returnStatus = 1;

        return(returnStatus);
      };


      template<typename GridType,typename StateType,typename ConfigType>
      int InputSingle(const std::string &fileName,
                      const std::string &domainName,
                      const std::string &geometryName,
                      const std::string &gridName,
                      GridType    &inGrid,
                      StateType   &inState,
                      StateType   &paramState,
                      ConfigType  &simConfig,
                      std::ostream &outStream){
        
        pcpp::CommunicatorType &gridComm(inGrid.Communicator());

        // Begin PlasCom2 input
        if(!ix::sys::FILEEXISTS(fileName))
          return(1);
        
        int numDomains = 1;

        int myRank = gridComm.Rank();

        pcpp::io::hdf5::base pc2File(fileName,gridComm);
        pc2File.SetMessageStream(outStream);

        std::string configString;
        pc2File.ReadAttribute("/PlasCom2/Configuration/Parameters",configString);
        std::istringstream configStream(configString);
        configStream >> simConfig;
        
        
        std::string geomGroupName(std::string("/PlasCom2/Geometry/")+geometryName);
        std::string geomGridGroupName(geomGroupName+"/"+gridName);
        pcpp::io::hdf5::ReadGrid(inGrid,geomGridGroupName,pc2File);
        

        pcpp::io::hdf5::ReadStateAttributes(inState,'s',"/PlasCom2/Simulation",pc2File);
        pcpp::io::hdf5::ReadStateAttributes(paramState,'s',"/PlasCom2/Simulation",pc2File);
        std::string domainGroupName(std::string("/PlasCom2/Simulation/")+domainName);

        std::string domainGridName(gridName);
        pcpp::io::hdf5::ReadStateAttributes(inState,'d',domainGroupName,pc2File);
        pcpp::io::hdf5::ReadStateAttributes(paramState,'d',domainGroupName,pc2File);
        std::string gridGroupName(domainGroupName+"/"+domainGridName);

        pcpp::IndexIntervalType partInterval(inGrid.PartitionInterval());
        pcpp::IndexIntervalType partitionBufferInterval(inGrid.PartitionBufferInterval());
        std::vector<size_t> partitionSizes(partInterval.Sizes());
        std::vector<size_t> partitionStarts(partInterval.Starts());
        std::vector<size_t> partitionBufferStarts(partitionBufferInterval.Starts());
        std::vector<size_t> gridSizes(inGrid.GridSizes());
        std::vector<size_t> bufferSizes(inGrid.BufferSizes());

        pcpp::io::hdf5::ReadStateNodeData(inState,gridSizes,partitionSizes,partitionStarts,
                                          bufferSizes,partitionBufferStarts,gridGroupName,pc2File);  
        pc2File.Close();

        return(0);
      }

      template<typename GridType,typename StateType,typename ConfigType>
      int InputSingle(const std::string &fileName,
                      const std::string &domainName,
                      const std::string &geometryName,
                      const std::string &gridName,
                      GridType     &inGrid,
                      StateType    &inState,
                      StateType    &paramState,
                      ConfigType   &simConfig,
                      std::ostream &outStream,
                      int          &iStep){
        
        pcpp::CommunicatorType &gridComm(inGrid.Communicator());

        // Begin PlasCom2 input
        if(!ix::sys::FILEEXISTS(fileName))
          return(1);
        
        int numDomains = 1;

        int myRank = gridComm.Rank();

        pcpp::io::hdf5::base pc2File(fileName,gridComm);
        pc2File.SetMessageStream(outStream);

        std::string configString;
        pc2File.ReadAttribute("/PlasCom2/Configuration/Parameters",configString);
        std::istringstream configStream(configString);
        configStream >> simConfig;
        
        
        std::string geomGroupName(std::string("/PlasCom2/Geometry/")+geometryName);
        std::string geomGridGroupName(geomGroupName+"/"+gridName);
        pcpp::io::hdf5::ReadGrid(inGrid,geomGridGroupName,pc2File);
        

        pcpp::io::hdf5::ReadStateAttributes(inState,'s',"/PlasCom2/Simulation",pc2File);
        pcpp::io::hdf5::ReadStateAttributes(paramState,'s',"/PlasCom2/Simulation",pc2File);
        pc2File.ReadAttribute("/PlasCom2/Simulation/stepNumber",&iStep);

        std::string domainGroupName(std::string("/PlasCom2/Simulation/")+domainName);

        std::string domainGridName(gridName);
        pcpp::io::hdf5::ReadStateAttributes(inState,'d',domainGroupName,pc2File);
        pcpp::io::hdf5::ReadStateAttributes(paramState,'d',domainGroupName,pc2File);
        std::string gridGroupName(domainGroupName+"/"+domainGridName);

        pcpp::IndexIntervalType partInterval(inGrid.PartitionInterval());
        pcpp::IndexIntervalType partitionBufferInterval(inGrid.PartitionBufferInterval());
        std::vector<size_t> partitionSizes(partInterval.Sizes());
        std::vector<size_t> partitionStarts(partInterval.Starts());
        std::vector<size_t> partitionBufferStarts(partitionBufferInterval.Starts());
        std::vector<size_t> gridSizes(inGrid.GridSizes());
        std::vector<size_t> bufferSizes(inGrid.BufferSizes());

        pcpp::io::hdf5::ReadStateNodeData(inState,gridSizes,partitionSizes,partitionStarts,
                                          bufferSizes,partitionBufferStarts,gridGroupName,pc2File);  
        pc2File.Close();

        return(0);
      }

      template<typename ConfigType>
      int FileInfo(const std::string &fileName,
                   pcpp::io::simfileinfo &fileInfo,
                   ConfigType  &simConfig,
                   std::ostream &outStream,
                   pcpp::CommunicatorType *commPtr = NULL){
        
        
        
        // Begin PlasCom2 output
        if(!ix::sys::FILEEXISTS(fileName))
          return(1);
        
        int myRank = 0;

        if(commPtr)
          myRank = commPtr->Rank();
        
        pcpp::io::hdf5::base pc2File;
        if(commPtr)
          pc2File.Open(fileName,commPtr);
        else
          pc2File.Open(fileName);

        pc2File.SetMessageStream(outStream);

        std::string configString;
        pc2File.ReadAttribute("/PlasCom2/Configuration/Parameters",configString);
        std::istringstream configStream(configString);
        configStream >> simConfig;
        
        if(pcpp::io::hdf5::PlasCom2FileInfo(pc2File,fileInfo,outStream))
          return(1);

        pc2File.Close();
        return(0);
      }
      
      int ReadSingle(const pcpp::io::simfileinfo &fileInfo,
                     plascom2::grid_t            &inGrid,
                     plascom2::state_t           &inState,
                     std::ostream                &infoStream);

      int ReadSingle(const pcpp::io::simfileinfo &fileInfo, int gridIndex,
                     plascom2::grid_t            &inGrid,
                     plascom2::state_t           &inState,
                     std::ostream                &infoStream);

      // Collective call, everyone in the grid communicator must call
      int ReadSingleState(const std::string           &fileName,
                          plascom2::grid_t            &inGrid,
                          plascom2::state_t           &inState,
                          std::ostream                &infoStream);

    }
#endif // hdf5
  }
}

#endif
