//
// Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
// License: MIT, http://opensource.org/licenses/MIT
//
#ifndef __STATE_OPERATORS_H__
#define __STATE_OPERATORS_H__

namespace operators {

  template<typename StateType>
  inline StateType operator*(double inScalar,StateType &inState)
  {
    StateType returnValue;
    returnValue.Copy(inState);
    returnValue *= inScalar;
    return(returnValue);
  };

  template<typename StateType>
  inline void AXPY(double a,StateType &X,StateType &Y)
  {
    Y += a*X;
  };
  
  template<typename StateType>
  inline void Zero(StateType &X)
  {
    X.Zero();
  };
  
  template<typename StateType>
  inline void Assign(StateType &X,StateType &Y)
  {
    Y = X;
  };
  
  template<typename StateType>
  StateType Sum(StateType &X,StateType &Y)
  {
    return(X+Y);
  };

  template<typename StateType>
  StateType Difference(StateType &X,StateType &Y)
  {
    return(Y+(-1.0*X));
  };
}

#endif
