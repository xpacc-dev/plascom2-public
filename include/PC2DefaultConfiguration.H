//
// Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
// License: MIT, http://opensource.org/licenses/MIT
//
#include "PCPPTypes.H"
#include "repositoryinfo.h"
#include "buildinfo.h"

const std::string 
PC2DefaultConfiguration
(
 std::string("System:Discovery = OFF\n")+
 std::string("PlasCom2:RunMode = BareBones\n")
 );

void ConfigureBuildInformation(fixtures::ConfigurationType &inConfig)
{
  fixtures::ConfigurationType::ParamType param;
#ifdef REPONAME
  param.first="Repository";
  param.second=REPONAME;
  inConfig.push_back(param);
#endif
#ifdef BRANCHNAME
  param.first = "Branch";
  param.second = BRANCHNAME;
  inConfig.push_back(param);
#endif
#ifdef REVISION
  param.first = "Revision";
  param.second = REVISION;
  inConfig.push_back(param);
#endif
#ifdef LOCALREV
  param.first = "LocalRevision";
  param.second = LOCALREV;
  inConfig.push_back(param);
#endif
#ifdef REMOTEREV
  param.first = "RemoteRevision";
  param.second = REMOTEREV;
  inConfig.push_back(param);
#endif
#ifdef BUILDHOST 
  param.first = "BuildHost";
  param.second = BUILDHOST;
  inConfig.push_back(param);
#endif
#ifdef BUILDDATE
  param.first  = "BuildDate";
  param.second = BUILDDATE;
  inConfig.push_back(param);
#endif
#ifdef CHANGED
  param.first = "ChangedFiles";
  param.second= CHANGED;
  inConfig.push_back(param);
#endif
#ifdef SOURCEDIR
  param.first = "SourceDirectory";
  param.second = SOURCEDIR;
  inConfig.push_back(param);
#endif
#ifdef BUILDDIR
  param.first = "BuildDirectory";
  param.second = BUILDDIR;
  inConfig.push_back(param);
#endif
#ifdef CXXCOMPILER
  param.first  = "CXXCompiler";
  param.second = CXXCOMPILER;
  inConfig.push_back(param);
#endif
#ifdef CXXCOMPILERID
  param.first = "CXXCompilerID";
  param.second = CXXCOMPILERID;
  inConfig.push_back(param);
#endif
#ifdef FORTRANCOMPILER
  param.first = "FortranCompiler";
  param.second = FORTRANCOMPILER;
  inConfig.push_back(param);
#endif
#ifdef CXXCOMPILERVERSION
  param.first = "CXXCompilerVersion";
  param.second = CXXCOMPILERVERSION;
  inConfig.push_back(param);
#endif
#ifdef FORTRANCOMPILERVERSION
  param.first = "FortranCompilerVersion";
  param.second = FORTRANCOMPILERVERSION;
  inConfig.push_back(param);
#endif
};
