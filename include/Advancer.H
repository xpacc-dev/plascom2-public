//
// Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
// License: MIT, http://opensource.org/licenses/MIT
//
//#include "Simulation.H"
#ifndef __ADVANCER_H__
#define __ADVANCER_H__

#include "PCPPTypes.H"
#include "StateOperators.H"

using namespace fixtures;

namespace simulation {
namespace advancer {

  
  template<typename DomainType>
  class base {

  public:

    typedef typename DomainType::GridType  GridType;
    typedef typename DomainType::StateType StateType;

    bool errCheck;

    base() : errCheck(true), messageStreamPtr(NULL), domainPtr(NULL) 
    {};

    base(DomainType &inDomain) : errCheck(true), messageStreamPtr(NULL),
                                 domainPtr(NULL)
    {
      InitilializeAdvancer(inDomain);
    };

    virtual int Configure(fixtures::ConfigurationType &inConfig)    { return(0); };

    virtual int InitializeAdvance()   { return(0); };

    virtual int InitializeAdvancer(DomainType &inDomain,std::ostream &messageStream) {      
      domainPtr = &inDomain;
      //      numGrids  = inDomain.NumberOfGrids();
      return(0); 
    };

    virtual int AdvanceDomain()     { return(0); };  

    //    virtual int RHS(StateType &inState,StateType &outState) { return(0); };

    virtual int FinalizeAdvance()   { return(0); };

    virtual int InterGridExchange() { return(0); };

    virtual void SetError(const std::string &errorMessage,fixtures::CommunicatorType &inComm){
      inComm.SetErr(1);
      if(messageStreamPtr)
        *messageStreamPtr << "Advancer:Error: " << errorMessage << std::endl;
    };

    virtual int ErrorCheck(fixtures::CommunicatorType &inComm)
    {
      if(errCheck)
        return(0);
      return(inComm.Check());
    };
    
    virtual void SetMessageStream(std::ostream &messageStream)
    { messageStreamPtr = &messageStream; };

  protected:

    fixtures::ConfigurationType advancerConfig;
    DomainType *domainPtr;
    std::ostream *messageStreamPtr;

  };

};
};
#endif
