#
# Copyright (c) 2019, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
cmake_minimum_required(VERSION 2.8)
project(PlasCom2 CXX C Fortran)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMake/Modules")
include(SetColors)

# force full path to libraries
if(NOT CMAKE_VERSION VERSION_LESS "3.3")
  cmake_policy(SET CMP0060 NEW)
endif()

#
# options for using CS-Tools
#
option(CS-Tools         "Enable CS Tools" OFF)

include(FortranCInterface)
FortranCInterface_HEADER(${PROJECT_BINARY_DIR}/include/FC.h MACRO_NAMESPACE "FC_")
FortranCInterface_VERIFY(CXX)
INCLUDE_DIRECTORIES(${PROJECT_BINARY_DIR}/include)

# Project-specific Options
OPTION(BUILD_STATIC "Build static libraries")
OPTION(BUILD_PROVENANCE "Record repository provenance" ON)
OPTION(ENABLE_MPI "Build with MPI Support" ON)
OPTION(ENABLE_OPENMP "Build with OpenMP parallelism")
OPTION(AUTODETECT_MPI "Automatically detect MPI" ON)
OPTION(BUILD_DOCUMENTATION "Create and install the HTML based API documentation (requires Doxygen)" ON)
OPTION(STATIC_KERNELS "Build static library for Fortran computation kernels" OFF)
OPTION(ENABLE_COVERAGE "Enable coverage testing with gcov (GCC only)" OFF)
OPTION(ENABLE_KNL "Build for Intel Knights Landing architecture" OFF)
OPTION(ENABLE_PAPI "Build with support for PAPI hardware performance counters." OFF)
OPTION(ENABLE_EFENCE "Build with ElectricFence memory fault detection." OFF)
OPTION(ENABLE_OVERKIT "Use Overkit" OFF)
OPTION(BUILD_OVERKIT "Build Overkit with PlasCom2 (if Overkit is enabled)" ON)
OPTION(BUILD_TESTS "Build PlasCom2 tests" ON) 
OPTION(INTEGRATED_TESTING "Enable longer integrated tests in testing suite" OFF)
OPTION(DISABLE_WARNINGS "Disable all compiler warnings" OFF)
#OPTION(ENABLE_AGGRESSIVE "Aggressive opt" OFF)

# Dynamic kernels on mac os x automatically
IF("${CMAKE_SYSTEM_VERSION}" STREQUAL "Darwin")
  SET(STATIC_KERNELS OFF)
ENDIF()
IF(APPLE)
  SET(STATIC_KERNELS OFF)
ENDIF()
IF(BUILD_STATIC)
  SET(STATIC_KERNELS ON)  
#  SET(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
  IF("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU") 
    SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static")
  ELSEIF("${CMAKE_CXX_COMPILER_ID}" STREQUAL "IBM")
    SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXIT_LINKER_FLAGS} -qstatic")
  ENDIF()
ENDIF()

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  # using Clang
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  # using GCC
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
  # using Intel C++
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "IBM")
  # using IBM
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "XL")
  # using IBM
endif()

if(ENABLE_PAPI)
  FIND_PROGRAM(PAPI_AVAIL papi_avail)
  IF(NOT PAPI_AVAIL) 
    MESSAGE(WARNING "PAPI performance counter access not found.")
  ELSE()
    MESSAGE(STATUS "Building with PAPI performance counters enabled.")
    GET_FILENAME_COMPONENT(PAPI_PATH ${PAPI_AVAIL} PATH)
    SET(PAPI_PATH ${PAPI_PATH}/..)
    MESSAGE(STATUS "Found PAPI Install: ${PAPI_PATH}")
    FIND_LIBRARY(PAPI_LIB papi PATHS ${PAPI_PATH}/lib NO_DEFAULT_PATH REQUIRED)
    MESSAGE(STATUS "Found PAPI Library: ${PAPI_LIB}")
    GET_FILENAME_COMPONENT(PAPI_LIBPATH ${PAPI_LIB} PATH)
    ADD_DEFINITIONS(-DUSE_PAPI)
    LINK_DIRECTORIES(${PAPI_LIBPATH})
    INCLUDE_DIRECTORIES(${PAPI_LIBPATH}/../include)
  ENDIF()
endif()

IF(ENABLE_EFENCE)
  FIND_LIBRARY(LIBEFENCE efence)
  IF(NOT LIBEFENCE)
    MESSAGE(WARNING "ElectricFence library not found.")
  ELSE()
    MESSAGE(STATUS "Building with ElectricFence memory fault detection ON.")
    SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g")
    SET(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -g")
  ENDIF()
ENDIF()

if(ENABLE_COVERAGE)
  MESSAGE(STATUS "Coverage testing enabled.")
  SET(COVERAGE TRUE)
  SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O0 -g -fprofile-arcs -ftest-coverage")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -g -fprofile-arcs -ftest-coverage")
  SET(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -O0 -g -fprofile-arcs -ftest-coverage")
  SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -lgcov --coverage")
  SET(CMAKE_LIB_LINKER_FLAGS "${CMAKE_LIB_LINKER_FLAGS} -lgcov --coverage")
  SET(CMAKE_BUILD_TYPE "Debug")
endif()
if (NOT CMAKE_BUILD_TYPE)
  message(STATUS "${PROJECT_NAME}: No build type selected, default to Release")
  set(CMAKE_BUILD_TYPE "Release")
endif()

#if(ENABLE_AGGRESSIVE)
#  SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O3 -xHost")
#  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -xHost")
#  SET(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -O3 -xHost")
#endif()

# FFLAGS depend on the compiler
get_filename_component (Fortran_COMPILER_NAME ${CMAKE_Fortran_COMPILER} NAME)
get_filename_component (CXX_COMPILER_NAME ${CMAKE_CXX_COMPILER} NAME)

MESSAGE(STATUS "CMAKE_CXX_COMPILER_ID = ${CMAKE_CXX_COMPILER_ID}")
MESSAGE(STATUS "CXX_COMPILER_NAME = ${CXX_COMPILER_NAME}")
MESSAGE(STATUS "CMAKE_CXX_COMPILER = ${CMAKE_CXX_COMPILER}")
MESSAGE(STATUS "Fortran_COMPILER_NAME = ${Fortran_COMPILER_NAME}")
MESSAGE(STATUS "CMAKE_Fortran_COMPILER = ${Fortran_COMPILER_ID}")
MESSAGE(STATUS "CMAKE_CXX_COMPILER_VERSION = ${CMAKE_CXX_COMPILER_VERSION}")
MESSAGE(STATUS "CMAKE_Fortran_COMPILER_VERSION = ${CMAKE_Fortran_COMPILER_VERSION}")
SET( buildinfo_in          ${CMAKE_SOURCE_DIR}/include/buildinfo.in )
SET( buildinfo             ${CMAKE_BINARY_DIR}/include/buildinfo.h  )
CONFIGURE_FILE( ${buildinfo_in} ${buildinfo} @ONLY )

IF(ENABLE_OPENMP) 
  find_package(OpenMP)
  if (OPENMP_FOUND)
    MESSAGE(STATUS "Building with OpenMP enabled.")
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set (CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${OpenMP_Fortran_FLAGS}")
    ADD_DEFINITIONS(-DUSE_OMP)
  else()
    MESSAGE(WARNING "OpenMP not found.")
  endif()
ENDIF(ENABLE_OPENMP)

IF(ENABLE_KNL)
  set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -xMIC-AVX512")
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -xMIC-AVX512")
  set (CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -xMIC-AVX512")
ENDIF(ENABLE_KNL)


SET(CMAKE_CXX_FLAGS_RELEASE "-g -O3 -DNDEBUG")
SET(CMAKE_Fortran_FLAGS_RELEASE "-g -O3 -DNDEBUG")

IF(DISABLE_WARNINGS)
  SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -w")
  SET(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -w")
  SET(CMAKE_Fortran_FLAGS_RELEASE "${CMAKE_Fortran_FLAGS_RELEASE} -w")
ENDIF(DISABLE_WARNINGS)

# Compiled objects output paths
set (EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin CACHE PATH "Single directory for all executables.")
set (LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/lib CACHE PATH "Single directory for all libraries and archives.")
mark_as_advanced (LIBRARY_OUTPUT_PATH EXECUTABLE_OUTPUT_PATH)
# Set up RPATH
if(NOT BUILD_STATIC)
  SET(CMAKE_MACOSX_RPATH TRUE)
  SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
  SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
endif()
IF(APPLE)
  set (CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -undefined dynamic_lookup")
ENDIF()

if (Fortran_COMPILER_NAME MATCHES "gfortran.*")
  # gfortran
  set (CMAKE_Fortran_FLAGS_RELEASE "-ffree-form -pipe -ffree-line-length-0 -fconvert=big-endian -funroll-all-loops -fno-f2c -cpp -O3")
  set (CMAKE_Fortran_FLAGS_DEBUG "-fno-f2c -cpp -O0 -g")
#  INCLUDE_DIRECTORIES(/usr/lib64/gfortran/modules)
elseif (Fortran_COMPILER_NAME MATCHES "f95.*")
  # gfortran
  set (CMAKE_Fortran_FLAGS_RELEASE "-funroll-all-loops -fno-f2c -cpp -O3")
  set (CMAKE_Fortran_FLAGS_DEBUG "-fno-f2c -cpp -O0 -g")
#  INCLUDE_DIRECTORIES(/usr/lib64/gfortran/modules)
elseif (Fortran_COMPILER_NAME MATCHES "ifort.*")
  # ifort (untested)
  set (CMAKE_Fortran_FLAGS_RELEASE "-f77rtl -O3")
  set (CMAKE_Fortran_FLAGS_DEBUG   "-f77rtl -O0 -g")
elseif (Fortran_COMPILER_NAME MATCHES "g77")
  # g77
  set (CMAKE_Fortran_FLAGS_RELEASE "-funroll-all-loops -fno-f2c -O3 -m32")
  set (CMAKE_Fortran_FLAGS_DEBUG   "-fno-f2c -O0 -g -m32")
else (Fortran_COMPILER_NAME MATCHES "gfortran.*")
  message ("${PROJECT_NAME}: CMAKE_Fortran_COMPILER full path: " ${CMAKE_Fortran_COMPILER})
  message ("${PROJECT_NAME}: Fortran compiler: " ${Fortran_COMPILER_NAME})
  message ("${PROJECT_NAME}: No optimized Fortran compiler flags are known, we just try -O3 ...")
  set (CMAKE_Fortran_FLAGS_RELEASE "-O3")
  set (CMAKE_Fortran_FLAGS_DEBUG   "-O0 -g")
endif (Fortran_COMPILER_NAME MATCHES "gfortran.*")


# 
# ========= USE IN EXISTING PROJECT ============
#
# In order to use this XPACC Project Template
# in an existing project COPY this file from this point
# on and paste it into your project's CMakeLists.txt 
# file *before* your existing INSTALL directives.
#
INCLUDE(CTest)
IF(BUILD_TESTING)
  SET(BUILDNAME "${BUILDNAME}" CACHE STRING "Name of build on the dashboard")
  MARK_AS_ADVANCED(BUILDNAME)
ENDIF(BUILD_TESTING)

IF(AUTODETECT_MPI)
  find_package(MPI REQUIRED)
  include_directories(${MPI_INCLUDE_PATH})
ENDIF()

# Enable the CS-Tools
if(CS-Tools)
  if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/CS-Tools/CMakeLists.txt")
    message("Enabling CS Tools")
    add_subdirectory(CS-Tools)
  else()
    message(WARNING "CS-Tools were enabled but no files were found in CS-Tools." 
    "Try running the following commands: 
    git submodule init
    git submodule update"
    "\nDisabling CS-Tools")
    set(CS-Tools OFF)
  endif()
endif()

#IF(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/googletest/")
#  add_subdirectory(googletest)
#ELSE()
#  add_definitions(-DNOGOOGLETEST)
#ENDIF()

if(BUILD_STATIC OR APPLE)
  set(HDF5_USE_STATIC_LIBRARIES TRUE)
endif()

FIND_PACKAGE(HDF5 COMPONENTS C)
IF(HDF5_FOUND)  
  MESSAGE(STATUS "${PROJECT_NAME}: HDF5_IS_PARALLEL       = ${HDF5_IS_PARALLEL}")
  MESSAGE(STATUS "${PROJECT_NAME}: HDF5_LIBRARY_DIRS      = ${HDF5_LIBRARY_DIRS}")
  MESSAGE(STATUS "${PROJECT_NAME}: HDF5_INCLUDE_DIRS      = ${HDF5_INCLUDE_DIRS}")
  MESSAGE(STATUS "${PROJECT_NAME}: HDF5_INCLUDE_DIR       = ${HDF5_INCLUDE_DIR}")
  MESSAGE(STATUS "${PROJECT_NAME}: HDF5_DEFINITIONS       = ${HDF5_DEFINITIONS}")
  MESSAGE(STATUS "${PROJECT_NAME}: HDF5_LIBRARIES         = ${HDF5_LIBRARIES}")                              
  MESSAGE(STATUS "${PROJECT_NAME}: HDF5_C_LIBRARIES       = ${HDF5_C_LIBRARIES}")                         
  #MESSAGE(STATUS "${PROJECT_NAME}: HDF5_CXX_LIBRARIES     = ${HDF5_CXX_LIBRARIES}")                            
  MESSAGE(STATUS "${PROJECT_NAME}: HDF5_Fortran_LIBRARIES = ${HDF5_Fortran_LIBRARIES}")   
  MESSAGE(STATUS "${PROJECT_NAME}: HDF5_C_COMPILER_EXECUTABLE = ${HDF5_C_COMPILER_EXECUTABLE}")   
  #MESSAGE(STATUS "${PROJECT_NAME}: HDF5_CXX_COMPILER_EXECUTABLE = ${HDF5_CXX_COMPILER_EXECUTABLE}")   
  MESSAGE(STATUS "${PROJECT_NAME}: HDF5_Fortran_COMPILER_EXECUTABLE = ${HDF5_Fortran_COMPILER_EXECUTABLE}")
  IF(HDF5_IS_PARALLEL) 
    MESSAGE(STATUS "${PROJECT_NAME}: Enabling HDF5.")
    SET(USE_HDF TRUE)
  ELSE()
    MESSAGE(STATUS "${PROJECT_NAME}: Disabling HDF5 (not parallel).")
    SET(USE_HDF FALSE)
  ENDIF(HDF5_IS_PARALLEL)
ENDIF() 
IF(USE_HDF)
  MESSAGE(STATUS "${PROJECT_NAME}: Setting up build for HDF5.")
  INCLUDE_DIRECTORIES(${HDF5_INCLUDE_DIRS})                                                    
  ADD_DEFINITIONS(-DENABLE_HDF5)
#  SET(PlasCom2_LibSources ${PlasCom2_LibSources} src/PC2HDF5.C)
ELSE()
  MESSAGE(FATAL_ERROR "${PROJECT_NAME}: Missing parallel HDF5.")
ENDIF()   

IF(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/IX/")
  MESSAGE(STATUS "${PROJECT_NAME}: Building IX from source")
  add_subdirectory(IX)
  SET(IX_INCPATH ${CMAKE_SOURCE_DIR}/IX/include)
  SET(RUNTEST ${EXECUTABLE_OUTPUT_PATH}/runtest)
  SET(TESTRESULT ${EXECUTABLE_OUTPUT_PATH}/testresults)
  SET(IX_LIB IX)
ELSE()
  FIND_LIBRARY(IX_LIB IX)
  FIND_FILE(IX_HDR COMM.H)
  FIND_PROGRAM(RUNTEST runtest)
  FIND_PROGRAM(TESTRESULT testresults)
  GET_FILENAME_COMPONENT(IX_LIBPATH ${IX_LIB} PATH)
  GET_FILENAME_COMPONENT(IX_INCPATH ${IX_HDR} PATH)
  GET_FILENAME_COMPONENT(IX_EXEPATH ${RUNTEST} PATH)
  MESSAGE(STATUS "${PROJECT_NAME}: Using existing IX install (${IX_EXEPATH})")
ENDIF()

IF(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/PCPP/")
  MESSAGE(STATUS "${PROJECT_NAME}: Building PCPP from source")
  add_subdirectory(PCPP)
  SET(PCPP_INCPATH ${CMAKE_SOURCE_DIR}/PCPP/include)
  SET(PCPP_LIB PCPP)
ELSE()
  FIND_LIBRARY(PCPP_LIB PCPP)
  FIND_FILE(PCPP_HDR PCPPTypes.H)
  GET_FILENAME_COMPONENT(PCPP_LIBPATH ${PCPP_LIB} PATH)
  GET_FILENAME_COMPONENT(PCPP_INCPATH ${PCPP_HDR} PATH)
  MESSAGE(STATUS "${PROJECT_NAME}: Using existing PCPP install (${PCPP_LIBPATH})")
ENDIF()

IF(ENABLE_OVERKIT)
  IF(BUILD_OVERKIT)
    MESSAGE(STATUS "${PROJECT_NAME}: Configuring Overkit")
    IF(BUILD_STATIC)
      SET(OVERKIT_SHARED OFF)
    ELSE()
      SET(OVERKIT_SHARED ON)
    ENDIF()
    SET(OVERKIT_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/Overkit")
    SET(OVERKIT_BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}/Overkit")
    INCLUDE(${OVERKIT_SOURCE_DIR}/OverkitSubproject.cmake)
    CONFIGURE_OVERKIT(
      SOURCE_DIR "${OVERKIT_SOURCE_DIR}"
      BINARY_DIR "${OVERKIT_BINARY_DIR}"
      INSTALL_BIN_DIR "${CMAKE_INSTALL_PREFIX}/bin"
      INSTALL_LIB_DIR "${CMAKE_INSTALL_PREFIX}/lib"
      INSTALL_INCLUDE_DIR "${CMAKE_INSTALL_PREFIX}/include"
      OPTIONS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DTESTS=ON -DXPACC=ON
        -DBUILD_SHARED_LIBS=${OVERKIT_SHARED} -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
        -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
      QUIET
    )
    MESSAGE(STATUS "Configured Overkit ${Overkit_VERSION}")
  ELSE()
    MESSAGE(STATUS "Looking for Overkit")
    FIND_PACKAGE(Overkit QUIET CONFIG REQUIRED)
    MESSAGE(STATUS "Found Overkit ${Overkit_VERSION} in ${Overkit_DIR}")
    IF(NOT Overkit_OPTION_XPACC)
      MESSAGE(FATAL_ERROR "Detected Overkit installation was not built with XPACC functionality enabled.")
    ENDIF()
  ENDIF()
  ADD_DEFINITIONS(-DUSE_OVERKIT)
  INCLUDE_DIRECTORIES("${Overkit_INCLUDES}")
ENDIF()

SET(PlasCom2_LibSources  src/Simulation.C src/PlasCom2.C src/Stencil.C src/Boundary.C src/Geometry.C
                         src/NavierStokesBC.C src/Grid.C src/PC2Initialize.C src/PC2IO.C src/PC2Util.C
                         src/PC2Configuration.C src/PC2ReadDomains.C src/PC2WriteDomains.C src/State.C
                         src/EulerUtil.C src/ViscidUtil.C src/RunPlasCom2.C src/WENO.C src/EOS.C
                         src/NavierStokesDictionary.C)

SET(PlasCom2_KernelSources kernels/barebones.f90 kernels/Operators.f90 kernels/MetricOps.f90 
  kernels/RungeKutta.f90 kernels/Grid.f90 kernels/Euler.f90 kernels/Viscid.f90 kernels/SATUtil.f90)

MESSAGE(STATUS "${PROJECT_NAME}: Library Sources: ${PlasCom2_LibSources}")
MESSAGE(STATUS "${PROJECT_NAME}: Kernel Sources: ${PlasCom2_KernelSources}")

#
# Preprocess the sources with ICE
#
if(USE_ICE)
  set(tuningFileIn ${CMAKE_CURRENT_SOURCE_DIR}/ice/tuneHand.yaml)
  if(USE_MOYA)
    set(tuningFileIn ${CMAKE_CURRENT_SOURCE_DIR}/ice/tuneHandMoya.yaml)
  endif()
  set(tuningFile ${CMAKE_CURRENT_BINARY_DIR}/ice/tuningFile.yaml)
  configure_file(${tuningFileIn} ${tuningFile} @ONLY)

  # LibSources
  foreach(fileIn ${PlasCom2_LibSources})
    PreProcessICE(${CMAKE_CURRENT_SOURCE_DIR}/${fileIn} ${tuningFile} 
      ${CMAKE_CURRENT_BINARY_DIR}/ice/src fileOut "ice.C")
    list(APPEND PlasCom2_LibSources-ICE ${fileOut})
  endforeach()
  set(PlasCom2_LibSources ${PlasCom2_LibSources-ICE})

  # KernelSources
  foreach(fileIn ${PlasCom2_KernelSources})
    PreProcessICE(${CMAKE_CURRENT_SOURCE_DIR}/${fileIn} ${tuningFile} 
      ${CMAKE_CURRENT_BINARY_DIR}/ice/kernels fileOut "ice.f90")
    list(APPEND PlasCom2_KernelSources-ICE ${fileOut})
  endforeach()
  set(PlasCom2_KernelSources ${PlasCom2_KernelSources-ICE})
endif()

add_custom_target(
    repoheader
    COMMAND ${CMAKE_SOURCE_DIR}/utils/buildversion.sh ${CMAKE_SOURCE_DIR} ${CMAKE_BINARY_DIR}/include/repositoryinfo.h
)

# Project-specific constructs
include_directories(include ${PCPP_INCPATH} ${IX_INCPATH})
IF(BUILD_STATIC)
  ADD_LIBRARY(PlasCom2 STATIC ${PlasCom2_LibSources})
  set_target_properties(PlasCom2 PROPERTIES LINK_SEARCH_START_STATIC 1)
  set_target_properties(PlasCom2 PROPERTIES LINK_SEARCH_END_STATIC 1)
ELSE()
  ADD_LIBRARY(PlasCom2 SHARED ${PlasCom2_LibSources})
ENDIF()
IF(BUILD_PROVENANCE)
  ADD_DEPENDENCIES(PlasCom2 repoheader)
ELSE()
  FILE(WRITE ${CMAKE_BINARY_DIR}/include/repositoryinfo.h "")
ENDIF()
TARGET_LINK_LIBRARIES(PlasCom2 ${PCPP_LIB} ${IX_LIB} ${PAPI_LIB} ${Overkit_LIBRARIES})

IF(USE_HDF)
  TARGET_LINK_LIBRARIES(PlasCom2 ${HDF5_LIBRARIES})
ENDIF()

INSTALL(TARGETS PlasCom2 LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)

IF(STATIC_KERNELS) 
  ADD_LIBRARY(Kernels STATIC ${PlasCom2_KernelSources})
ELSE()
  ADD_LIBRARY(Kernels SHARED ${PlasCom2_KernelSources})
ENDIF()
INSTALL(TARGETS Kernels LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)


set_source_files_properties(src/PC2Main.C
    PROPERTIES COMPILE_FLAGS "${COMPILE_FLAGS} -O0 ${MPI_CXX_COMPILE_FLAGS} ${MPI_COMPILE_FLAGS}")
ADD_EXECUTABLE(plascom2x src/PC2Main.C)
TARGET_LINK_LIBRARIES(plascom2x PlasCom2 Kernels ${PCPP_LIB} ${IX_LIB}
  ${MPI_CXX_LIBRARIES} ${MPI_LIBRARIES} ${LIBEFENCE})
SET_TARGET_PROPERTIES(plascom2x PROPERTIES LINK_FLAGS "${LINK_FLAGS} ${MPI_CXX_LINK_FLAGS} ${MPI_LINK_FLAGS}")

INSTALL(TARGETS plascom2x RUNTIME DESTINATION bin)

ADD_EXECUTABLE(pc2fileinfo src/pc2fileinfo.C)
TARGET_LINK_LIBRARIES(pc2fileinfo PlasCom2 Kernels ${PCPP_LIB} ${IX_LIB} ${MPI_LIBRARIES} ${LIBEFENCE})
SET_TARGET_PROPERTIES(pc2fileinfo PROPERTIES LINK_FLAGS "${LINK_FLAGS} ${MPI_LINK_FLAGS}")
INSTALL(TARGETS pc2fileinfo RUNTIME DESTINATION bin)

ADD_EXECUTABLE(pc2compare src/pc2compare.C)
TARGET_LINK_LIBRARIES(pc2compare PlasCom2 Kernels ${PCPP_LIB} ${IX_LIB} ${MPI_LIBRARIES} ${LIBEFENCE})
SET_TARGET_PROPERTIES(pc2compare PROPERTIES LINK_FLAGS "${LINK_FLAGS} ${MPI_LINK_FLAGS}")
INSTALL(TARGETS pc2compare RUNTIME DESTINATION bin)

# Add target to build the documentation
IF(BUILD_DOCUMENTATION)
  add_subdirectory(doc) 
endif()

# ========= USE IN EXISTING PROJECT ==============

IF(BUILD_TESTS) 
  ADD_SUBDIRECTORY(Testing)
ENDIF()
